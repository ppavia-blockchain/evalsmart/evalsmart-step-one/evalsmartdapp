# Evalsmart dApp

## Installation

### Create app
```
npx create-react-app evalsmart-dapp
```

### Install dependencies

- #### bootstrap
```
npm install react-bootstrap bootstrap
```

_add this link into the header of index.html_  
    ```
    <link
        rel="stylesheet"
        href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
        crossorigin="anonymous"
    />
    ```

- #### react router
```
npm install -S react-router-dom
```

- #### react bootstrap router
```
npm install -S react-router-bootstrap
```

- #### redux
```
npm install redux  
npm install react-redux  
```

- #### axios
```
npm install axios 
```

### // check form data
### // send validatation form
### // send signature form