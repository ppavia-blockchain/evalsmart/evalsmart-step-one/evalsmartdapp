import EaeFormStructQueryRequestMapping from './EaeFormStructQueryRequestMapping';

/**
 * this is the representation of the response as provided by the service
 */
export default class EaeFormStructListQueriesRequestMapping {
    constructor () {
        this.timestamp          = null;
        this.message            = null;
        this.location           = null;
        this.httpCodeMessage    = null;
        this.queriesRM          = [];
    }

    init = (timestamp,
        message,
        location,
        httpCodeMessage
        ) => {
            this.timestamp          = timestamp;
            this.message            = message;
            this.location           = location;
            this.httpCodeMessage    = httpCodeMessage;
    }

    addQuery = (
        id,
        idQuery,
        idDomain,
        idForm,
        label,
        queryType,
        order
    ) => {
        const query    = new EaeFormStructQueryRequestMapping();
        query.init(
            id,
            idQuery,
            idDomain,
            idForm,
            label,
            queryType,
            order
        );
        this.queriesRM.push(query);
    }
}