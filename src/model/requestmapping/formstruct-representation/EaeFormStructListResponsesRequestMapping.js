import EaeFormStructResponseRequestMapping from './EaeFormStructResponseRequestMapping';

export default class EaeFormStructListResponsesRequestMapping {
    constructor () {
        this.timestamp          = null;
        this.message            = null;
        this.location           = null;
        this.httpCodeMessage    = null;
        this.responsesRM        = [];
    }

    init = (timestamp,
        message,
        location,
        httpCodeMessage
        ) => {
            this.timestamp          = timestamp;
            this.message            = message;
            this.location            = location;
            this.httpCodeMessage    = httpCodeMessage;
    }

    addResponse = (
        idResponse,
        label,
        idQuery,
        idDomain,
        idForm,
        idOwner,
        responseType,
        isCompleted,
        isChecked,
        level,
        content
    ) => {
        const response    = new EaeFormStructResponseRequestMapping();
        response.init(
            idResponse,
            label,
            idQuery,
            idDomain,
            idForm,
            idOwner,
            responseType,
            isCompleted,
            isChecked,
            level,
            content
        );
        this.responsesRM.push(response);
    }
}