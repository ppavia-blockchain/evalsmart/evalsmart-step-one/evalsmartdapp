/**
 * this is the representation of the object as provided by the service
 */
export default class EaeFormStructQueryRequestMapping {
    constructor() {
        this.id             = null;
        this.idQuery		= null;
        this.idDomain		= null;
        this.idForm			= null;
        this.label			= null;
        this.queryType		= null;
        this.order			= null;
    }

    init    = (
        id,
        idQuery,
        idDomain,
        idForm,
        label,
        queryType,
        order
    ) => {
        this.id     		= id;
        this.idQuery		= idQuery;
        this.idDomain		= idDomain;
        this.idForm			= idForm;
        this.label			= label;
        this.queryType		= queryType;
        this.order			= order;
    }
}