/**
 * this is the representation of the object as provided by the service
 */
export default class EaeFormStructInfoRequestMapping {
    constructor () {
        this.id             = null;
        this.idInfo         = null;
        this.idDomain       = null;
        this.idForm         = null;
        this.label          = null;
        this.content        = null;
        this.order          = null;
    }

    init    = (
        id,
        idInfo,
        idDomain,
        idForm,
        label,
        content,
        order
    ) => {
        this.id             = id;
        this.idInfo         = idInfo;
        this.idDomain       = idDomain;
        this.idForm         = idForm;
        this.label          = label;
        this.content        = content;
        this.order          = order;
    }
}