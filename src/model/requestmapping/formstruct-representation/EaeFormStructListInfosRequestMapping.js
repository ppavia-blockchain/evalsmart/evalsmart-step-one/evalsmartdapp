import EaeFormStructInfoRequestMapping from './EaeFormStructInfoRequestMapping';
/**
 * this is the representation of the object as provided by the service
 */
export default class EaeFormStructListInfosRequestMapping {
    constructor () {
        this.timestamp          = null;
        this.message            = null;
        this.location           = null;
        this.httpCodeMessage    = null;
        this.infosRM            = [];
    }

    init = (timestamp,
        message,
        location,
        httpCodeMessage
        ) => {
            this.timestamp          = timestamp;
            this.message            = message;
            this.location           = location;
            this.httpCodeMessage    = httpCodeMessage;
    }

    addInfo = (
        id,
        idInfo,
        idDomain,
        idForm,
        label,
        content,
        order
    ) => {
        const info    = new EaeFormStructInfoRequestMapping();
        info.init(
            id,
            idInfo,
            idDomain,
            idForm,
            label,
            content,
            order
        );
        this.infosRM.push(info);
    }
}