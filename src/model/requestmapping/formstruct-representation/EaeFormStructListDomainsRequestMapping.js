import EaeFormStructDomainRequestMapping from './EaeFormStructDomainRequestMapping';
/**
 * this is the representation of the response as provided by the service
 */
export default class EaeFormStructListDomainsRequestMapping {
    constructor () {
        this.timestamp          = null;
        this.message            = null;
        this.location           = null;
        this.httpCodeMessage    = null;
        this.domainsRM          = [];
    }

    init = (timestamp,
        message,
        location,
        httpCodeMessage
        ) => {
            this.timestamp          = timestamp;
            this.message            = message;
            this.location           = location;
            this.httpCodeMessage    = httpCodeMessage;
    }

    addDomain = (
        id,
        idDomain,
        idForm,
        label,
        order,
        levels
    ) => {
        const domain    = new EaeFormStructDomainRequestMapping();
        domain.init(
            id,
            idDomain,
            idForm,
            label,
            order,
            levels
        );
        this.domainsRM.push(domain);
    }
}