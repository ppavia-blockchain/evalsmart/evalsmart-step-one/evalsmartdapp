/**
 * this is the representation of the object as provided by the service
 */
export default class EaeFormStructResponseRequestMapping {
    constructor() {
        this.id                 = null;
        this.idResponse			= null;
        this.label				= null;
        this.idQuery			= null;
        this.idDomain			= null;
        this.idForm				= null;
        this.responseType		= null;
        this.content			= null;
        this.level				= null;
    }

    init    = (
        id,
        idResponse,
        label,
        idQuery,
        idDomain,
        idForm,
        responseType,
        content,
        level
    ) => {
        this.id                 = id;
        this.idResponse			= idResponse;
        this.label				= label;
        this.idQuery			= idQuery;
        this.idDomain			= idDomain;
        this.idForm				= idForm;
        this.responseType		= responseType;
        this.content			= content;
        this.level				= level;
    }
}