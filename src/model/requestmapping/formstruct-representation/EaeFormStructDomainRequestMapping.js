/**
 * this is the representation of the object as provided by the service
 */
export default class EaeFormStructDomainRequestMapping {

    constructor() {
        this.id         = null;
        this.idDomain	= null;
        this.label		= null;
        this.idForm		= null;
        this.order		= 0;
        this.levels		= [];
    }

    init    = (
        id,
        idDomain,
        idForm,
        label,
        order,
        levels
    ) => {
        this.id     	= id;
        this.idDomain	= idDomain;
        this.idForm		= idForm;
        this.label		= label;
        this.order		= order;
        this.levels		= levels;
    }
}