/**
 * this is the representation of the object as provided by the service
 */
export default class EaeFormStructRequestMapping {
    constructor () {
        this.timestamp          = null;
        this.message            = null;
        this.location           = null;
        this.httpCodeMessage    = null;
        this.id                     = null;
	    this.idForm                 = null;
	    this.label                  = null;
        this.dtCreated              = null;
        this.dtClosed               = null;
        this.dtHistorized           = null;
        this.status                 = null;
    }

    init    = (
        timestamp,
        message,
        location,
        httpCodeMessage,
        id,
	    idForm,
	    label,
        dtCreated,
        dtClosed,
        dtHistorized,
        status
    ) => {
        this.timestamp          = timestamp;
        this.message            = message;
        this.location           = location;
        this.httpCodeMessage    = httpCodeMessage;
        this.id                 = id;
	    this.idForm             = idForm;
	    this.label              = label;
        this.dtCreated          = dtCreated;
        this.dtClosed           = dtClosed;
        this.dtHistorized       = dtHistorized;
        this.status             = status;
    }
}