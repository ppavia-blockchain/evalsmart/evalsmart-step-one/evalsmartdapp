import EaeOneStatusFormRequestMapping from './EaeOneStatusFormRequestMapping';
export default class EaeStatusFormRequestMapping {
    constructor() {
        this.timestamp          = null;
        this.message            = null;
        this.location           = null;
        this.httpCodeMessage    = null;
        this.statusFormRM       = [];
    }

    init = (
        timestamp,
        message,
        location,
        httpCodeMessage
        ) => {
            this.timestamp          = timestamp;
            this.message            = message;
            this.location           = location;
            this.httpCodeMessage    = httpCodeMessage;
    }

    addStatus (
        code,
        label
    ) {
        const status     = new EaeOneStatusFormRequestMapping (
            code,
            label
        );
        this.statusFormRM.push(status);
    }
};