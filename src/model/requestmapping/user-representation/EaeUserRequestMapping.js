export default class EaeUserRequestMapping {
    constructor() {
        this.timestamp          = null;
        this.message            = null;
        this.location           = null;
        this.httpCodeMessage    = null;
        this.idUser     = null;
        this.profile    = null;
        this.firstName  = null;
        this.lastName   = null;
        this.fullName   = null;
        this.userName   = null;
        this.email      = null;
        this.phone      = null;
        this.status     = null;
        this.accounts   = null;
    }

    init = (
        timestamp,
        message,
        location,
        httpCodeMessage,
        idUser,
        profile,
        firstName,
        lastName,
        fullName,
        userName,
        email,
        phone,
        status,
        accounts) => {
            this.timestamp          = timestamp;
            this.message            = message;
            this.location           = location;
            this.httpCodeMessage    = httpCodeMessage;
            this.idUser     = idUser;
            this.profile    = profile;
            this.firstName  = firstName;
            this.lastName   = lastName;
            this.fullName   = fullName;
            this.userName   = userName;
            this.email      = email;
            this.phone      = phone;
            this.status     = status;
            this.accounts   = accounts;
    } 

};