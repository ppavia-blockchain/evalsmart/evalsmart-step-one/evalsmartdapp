/**
 * this is the representation of the object as provided by the service
 */
export default class EaeUserFormRequestMapping {
    constructor() {
        this.timestamp          = null;
        this.message            = null;
        this.location           = null;
        this.httpCodeMessage    = null;
        this.id					= null;
        this.idInterview    	= null;
        this.idForm				= null;
        this.idUser				= null;
        this.userRole			= null;
        this.dtCreated			= null;
        this.dtClosed			= null;
        this.dtHistorized		= null;
        this.idManager			= null;
        this.dtOwnerValidated	= null;
        this.dtManagerValidated	= null;
        this.dtOwnerSigned		= null;
        this.dtManagerSigned	= null;
        this.status				= null;
        this.completion			= null;
    }

    init    = (
        timestamp,
        message,
        location,
        httpCodeMessage,
        id,
        idInterview,
        idForm,
        idUser,
        userRole,
        dtCreated,
        dtClosed,
        dtHistorized,
        idManager,
        dtOwnerValidated,
        dtManagerValidated,
        dtOwnerSigned,
        dtManagerSigned,
        status,
        completion
    ) => {
        this.timestamp          = timestamp;
        this.message            = message;
        this.location           = location;
        this.httpCodeMessage    = httpCodeMessage;
        this.id					= id;
        this.idInterview    	= idInterview;
        this.idForm				= idForm;
        this.idUser				= idUser;
        this.userRole			= userRole;
        this.dtCreated			= dtCreated;
        this.dtClosed			= dtClosed;
        this.dtHistorized		= dtHistorized;
        this.idManager			= idManager;
        this.dtOwnerValidated	= dtOwnerValidated;
        this.dtManagerValidated	= dtManagerValidated;
        this.dtOwnerSigned		= dtOwnerSigned;
        this.dtManagerSigned	= dtManagerSigned;
        this.status				= status;
        this.completion			= completion;
    }
}