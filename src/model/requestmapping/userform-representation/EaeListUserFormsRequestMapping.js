import EaeUserFormRequestMapping from './EaeUserFormRequestMapping';
/**
 * this is the representation of the object as provided by the service
 */
export default class EaeListUserFormsRequestMapping {
    constructor () {
        this.timestamp          = null;
        this.message            = null;
        this.location           = null;
        this.httpCodeMessage    = null;
        this.formsRM            = [];
    }

    init = (
        timestamp,
        message,
        location,
        httpCodeMessage
        ) => {
            this.timestamp          = timestamp;
            this.message            = message;
            this.location           = location;
            this.httpCodeMessage    = httpCodeMessage;
    }

    addForm = (
        id,
        idInterview,
        idForm,
        idUser,
        userRole,
        dtCreated,
        dtClosed,
        dtHistorized,
        idManager,
        dtOwnerValidated,
        dtManagerValidated,
        dtOwnerSigned,
        dtManagerSigned,
        status,
        completion
    ) => {
        const userForm    = new EaeUserFormRequestMapping();
        userForm.init(
            null,
            null,
            null,
            null,
            id,
            idInterview,
            idForm,
            idUser,
            userRole,
            dtCreated,
            dtClosed,
            dtHistorized,
            idManager,
            dtOwnerValidated,
            dtManagerValidated,
            dtOwnerSigned,
            dtManagerSigned,
            status,
            completion
        );
        this.formsRM.push(userForm);
    }
}