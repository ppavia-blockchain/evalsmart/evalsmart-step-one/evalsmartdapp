/**
 * this is the representation of the object as provided by the service
 */
export default class EaeUserResponseRequestMapping {
    constructor () {
        this.id						= null;
        this.idForm					= null;
        this.idUser					= null;
        this.idDomain				= null;
        this.idQuery				= null;
        this.idResponse				= null;
        this.idOwner				= null;
        this.content				= null;
        this.dtCompletion			= null;
    }

    init    = (
        id,
        idForm,
        idUser,
        idDomain,
        idQuery,
        idResponse,
        idOwner,
        content,
        dtCompletion
    ) => {
        this.id						= id;
        this.idForm					= idForm;
        this.idUser					= idUser;
        this.idDomain				= idDomain;
        this.idQuery				= idQuery;
        this.idResponse				= idResponse;
        this.idOwner				= idOwner;
        this.content				= content;
        this.dtCompletion			= dtCompletion;
    }
}