import EaeUserResponseRequestMapping from './EaeUserResponseRequestMapping';
/**
 * this is the representation of the object as provided by the service
 */
export default class EaeListUserResponsesRequestMapping {
    constructor () {
        this.timestamp          = null;
        this.message            = null;
        this.location           = null;
        this.httpCodeMessage    = null;
        this.responsesRM        = [];
    }

    init = (
        timestamp,
        message,
        location,
        httpCodeMessage
        ) => {
            this.timestamp          = timestamp;
            this.message            = message;
            this.location            = location;
            this.httpCodeMessage    = httpCodeMessage;
    }

    addResponse = (
        id,
        idForm,
        idUser,
        idDomain,
        idQuery,
        idResponse,
        idOwner,
        content,
        dtCompletion
    ) => {
        const response    = new EaeUserResponseRequestMapping();
        response.init(
            id,
            idForm,
            idUser,
            idDomain,
            idQuery,
            idResponse,
            idOwner,
            content,
            dtCompletion
        );
        this.responsesRM.push(response);
    }
}