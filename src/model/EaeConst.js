export const ROOT_ENUM_URI                  = "eaeenum";
export const ROOT_USER_URI                  = "users";
export const ROOT_FORM_STRUCT_URI           = "eaeformstruct";
export const ROOT_FORM_STRUCT_DOMAIN_URI    = "eaeformstructdomain";
export const ROOT_FORM_STRUCT_QUERY_URI     = "eaeformstructquery";
export const ROOT_FORM_STRUCT_INFO_URI      = "eaeformstructinfo";
export const ROOT_FORM_STRUCT_RESPONSE_URI  = "eaeformstructresponse";
export const ROOT_USER_FORM_URI             = "eaeuserform";
export const ROOT_USER_FORM_RESPONSE_URI    = "eaeuserformresponse";

export const END_EMAIL_DOMAIN               = "@micropole.com";

export const USER_ROLE_CODE_MANAGER         = "002";
export const USER_ROLE_CODE_COLLABORATOR    = "001";

export const USER_FORM_STATUS_CREATED       = "001";
export const USER_FORM_STATUS_VALIDATED     = "010";
export const USER_FORM_STATUS_SIGNED        = "100";
export const USER_FORM_STATUS_HISTORIZED    = "111";
export const USER_FORM_STATUS_EXPIRED       = "002";
export const USER_FORM_STATUS_CANCELED      = "020";
export const USER_FORM_STATUS_DELETED       = "200";

export const QUERY_TYPE_INPUT               = "001";
export const QUERY_TYPE_INPUTTEXT           = "002";
export const QUERY_TYPE_COMMENT             = "003";
export const QUERY_TYPE_DATE                = "004";


export const UI_TABLE                       = "Table";
export const UI_THEADER                     = "thead";
export const UI_TBODY                       = "tbody";
export const UI_TFOOTER                     = "tfooter";
export const UI_INPUT_AREA                  = "textarea";
export const UI_INPUT_TEXT                  = "text";
export const UI_TEXT                        = "p";
export const UI_CONTAINER                   = "Container";
export const UI_ROW                         = "Row";
export const UI_COL                         = "Col";
export const UI_RADIOBUTTON                 = "radio";
export const UI_BUTTON                      = "Button";
export const UI_DOMAIN                      = "EaeBlocUI";
export const UI_NAV                         = "Nav";
export const UI_FOOTER_DOMAIN               = "EaeFooterDomainComp";

export const UI_COMMENT_LABEL               = "Commentaire";