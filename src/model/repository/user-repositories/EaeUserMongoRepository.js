import axios from 'axios';
import * as EaeConst from '../../EaeConst';
import * as utils from '../../../utils/EvalsmartUtils';
import EaeUserRequestMapping from '../../requestmapping/user-representation/EaeUserRequestMapping';

const fileName = "EaeUserMongoRepository";

export default class EaeUserMongoRepository {
    rootUrlService      = EaeConst.ROOT_USER_URI;
    isDebug             = false;

    constructor (config={log:{isDebug:false, package:[]}}) {
        this.config     = config;
        this.isDebug = utils.isDebug (config.log, fileName);
    }
    
    findById          = (idUser) => {
        const methodName            = "findById";
        const pathService         = `${this.config.eaeServicesPath}/${this.rootUrlService}/${idUser}`;

        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][pathService]`, '\n', pathService);
        }

        let userRequestMapping  = new EaeUserRequestMapping();

        return axios.get(pathService)
        .then(res => {
            userRequestMapping.init(
                res.data.timestamp,
                res.data.message,
                res.data.location,
                res.data.httpCodeMessage,
                res.data.data.idUser,
                res.data.data.profile,
                res.data.data.firstName,
                res.data.data.lastName,
                res.data.data.fullName,
                res.data.data.userName,
                res.data.data.email,
                res.data.data.phone,
                res.data.data.status,
                res.data.data.account
            );
            if ( this.isDebug ) {
                console.log(`[${fileName}][${methodName}]`, '\n', userRequestMapping);
            }
            return userRequestMapping;
        })
        .catch(error => {
            if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                console.log(`[${fileName}][${methodName}][error.response.data]`, '\n', error.response.data);
                console.log(`[${fileName}][${methodName}][error.response.status]`, '\n', error.response.status);
                console.log(`[${fileName}][${methodName}][error.response.headers]`, '\n', error.response.headers);
                throw new Error (`ERROR in [${fileName}][${methodName}] - The status code of the response is not OK : status code [${error.response.status}]`);
            }
            else if (error.request) {
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                // http.ClientRequest in node.js
                console.log(`[${fileName}][${methodName}][error.request]`, '\n', error.request);
                throw new Error (`ERROR from the request in [${fileName}][${methodName}] - no response was recieved`);
            } else {
                // Something happened in setting up the request that triggered an Error
                console.log(`[${fileName}][${methodName}][error.message]`, '\n', error.message);
                console.log(`[${fileName}][${methodName}][error.config]`, '\n', error.config);
                throw new Error (`ERROR from the request in [${fileName}][${methodName}]`);
            }
        });
    }

    findByEmail          = (email) => {
        const methodName            = "findById";
        const pathService         = `${this.config.eaeServicesPath}/${this.rootUrlService}/email/${email}`;

        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][pathService]`, '\n', pathService);
        }

        let userRequestMapping  = new EaeUserRequestMapping();

        return axios.get(pathService)
        .then(res => {
            userRequestMapping.init(
                res.data.timestamp,
                res.data.message,
                res.data.location,
                res.data.httpCodeMessage,
                res.data.data.idUser,
                res.data.data.profile,
                res.data.data.firstName,
                res.data.data.lastName,
                res.data.data.fullName,
                res.data.data.userName,
                res.data.data.email,
                res.data.data.phone,
                res.data.data.status,
                res.data.data.account
            );
            if ( this.isDebug ) {
                console.log(`[${fileName}][${methodName}]`, '\n', userRequestMapping);
            }
            return userRequestMapping;
        })
        .catch(error => {
            if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                console.log(`[${fileName}][${methodName}][error.response.data]`, '\n', error.response.data);
                console.log(`[${fileName}][${methodName}][error.response.status]`, '\n', error.response.status);
                console.log(`[${fileName}][${methodName}][error.response.headers]`, '\n', error.response.headers);
                throw new Error (`ERROR in [${fileName}][${methodName}] - The status code of the response is not OK : status code [${error.response.status}]`);
            }
            else if (error.request) {
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                // http.ClientRequest in node.js
                console.log(`[${fileName}][${methodName}][error.request]`, '\n', error.request);
                throw new Error (`ERROR from the request in [${fileName}][${methodName}] - no response was recieved`);
            } else {
                // Something happened in setting up the request that triggered an Error
                console.log(`[${fileName}][${methodName}][error.message]`, '\n', error.message);
                console.log(`[${fileName}][${methodName}][error.config]`, '\n', error.config);
                throw new Error (`ERROR from the request in [${fileName}][${methodName}]`);
            }
        });
    }

    findByUserName          = (userName) => {
        const methodName            = "findByUserName";
        const pathService         = `${this.config.eaeServicesPath}/${this.rootUrlService}/username/${userName}`;

        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][pathService]`, '\n', pathService);
        }

        let userRequestMapping  = new EaeUserRequestMapping();

        return axios.get(pathService)
        .then(res => {
            userRequestMapping.init(
                res.data.timestamp,
                res.data.message,
                res.data.location,
                res.data.httpCodeMessage,
                res.data.data.idUser,
                res.data.data.profile,
                res.data.data.firstName,
                res.data.data.lastName,
                res.data.data.fullName,
                res.data.data.userName,
                res.data.data.email,
                res.data.data.phone,
                res.data.data.status,
                res.data.data.account
            );
            if ( this.isDebug ) {
                console.log(`[${fileName}][${methodName}]`, '\n', userRequestMapping);
            }
            return userRequestMapping;
        })
        .catch(error => {
            if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                console.error(`[${fileName}][${methodName}][error.response.data]`, '\n', error.response.data);
                console.error(`[${fileName}][${methodName}][error.response.status]`, '\n', error.response.status);
                console.error(`[${fileName}][${methodName}][error.response.headers]`, '\n', error.response.headers);
                throw new Error (`ERROR in [${fileName}][${methodName}] - The status code of the response is not OK : status code [${error.response.status}]`);
            }
            else if (error.request) {
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                // http.ClientRequest in node.js
                console.error(`[${fileName}][${methodName}][error.request]`, '\n', error.request);
                throw new Error (`ERROR from the request in [${fileName}][${methodName}] - no response was recieved`);
            } else {
                // Something happened in setting up the request that triggered an Error
                console.error(`[${fileName}][${methodName}][error.message]`, '\n', error.message);
                console.error(`[${fileName}][${methodName}][error.config]`, '\n', error.config);
                throw new Error (`ERROR from the request in [${fileName}][${methodName}]`);
            }
        });
    }
}
