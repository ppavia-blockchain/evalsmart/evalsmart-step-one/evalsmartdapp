import axios from 'axios';
import * as EaeConst from '../../EaeConst';
import * as utils from '../../../utils/EvalsmartUtils';
import EaeListUserResponsesRequestMapping from '../../requestmapping/userform-representation/EaeListUserResponsesRequestMapping';

const fileName     = "EaeUserFormResponseRepository";

export default class EaeUserFormResponseRepository {
    rootUrlService      = EaeConst.ROOT_USER_FORM_RESPONSE_URI;
    isDebug             = false;

    constructor (config={log:{isDebug:false, package:[]}}) {
        this.config     = config;
        this.isDebug = utils.isDebug (config.log, fileName);
    }

    findUserResponsesByIds    = (idForm, idUser, idResponse, idOwner) => {
        const methodName            = "findUserResponsesByIds";
        const pathService       = `${this.config.eaeServicesPath}/${this.rootUrlService}/responses/${idForm}/${idUser}/${idResponse}/${idOwner}`;
        let userResponsesRM     = new EaeListUserResponsesRequestMapping();

        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}]`, '\n', pathService);
        }
        
        return axios.get(pathService)
        .then(res => {
            if ( utils.isEmpty(res.data) ) {
                throw new Error("response data is empty");
            }
            if ( utils.isEmpty(res.data.data) ) {
                if ( this.isDebug ) {
                    console.log(`[${fileName}][${methodName}]`, '\n', res.data);
                }
                throw new Error("response object  is empty");
            }
            userResponsesRM.init(
                res.data.timestamp,
                res.data.message,
                res.data.location,
                res.data.httpCodeMessage
            );
            res.data.data.forEach(response => {
                userResponsesRM.addResponse (
                    response.id,
                    response.idForm,
                    response.idUser,
                    response.idDomain,
                    response.idQuery,
                    response.idResponse,
                    response.idOwner,
                    response.content,
                    response.dtCompletion
                );
            });
            if ( this.isDebug ) {
                console.log(`[${fileName}][${methodName}]`, '\n', userResponsesRM);
            }
            return userResponsesRM;
        })
        .catch(error => {
            if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                console.log(`[${fileName}][${methodName}][error.response.data]`, '\n', error.response.data);
                console.log(`[${fileName}][${methodName}][error.response.status]`, '\n', error.response.status);
                console.log(`[${fileName}][${methodName}][error.response.headers]`, '\n', error.response.headers);
                throw new Error (`ERROR in [${fileName}][${methodName}] - The status code of the response is not OK : status code [${error.response.status}]`);
            }
            else if (error.request) {
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                // http.ClientRequest in node.js
                console.log(`[${fileName}][${methodName}][error.request]`, '\n', error.request);
                throw new Error (`ERROR from the request in [${fileName}][${methodName}] - no response was recieved`);
            } else {
                // Something happened in setting up the request that triggered an Error
                console.log(`[${fileName}][${methodName}][error.message]`, '\n', error.message);
                console.log(`[${fileName}][${methodName}][error.config]`, '\n', error.config);
                throw new Error (`ERROR from the request in [${fileName}][${methodName}]`);
            }
        });
    }

    findAllUserResponsesByOwner    = (idForm, idUser, idOwner) => {
        const methodName            = "findAllUserResponsesByOwner";
        const pathService       = `${this.config.eaeServicesPath}/${this.rootUrlService}/forms/${idForm}/users/${idUser}/responses/ownedBy/${idOwner}`;
        let userResponsesRM     = new EaeListUserResponsesRequestMapping();

        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}]`, '\n', pathService);
        }
        
        return axios.get(pathService)
        .then(res => {
            if ( utils.isEmpty(res.data) ) {
                throw new Error("response data is empty");
            }
            if ( utils.isEmpty(res.data.data) ) {
                if ( this.isDebug ) {
                    console.log(`[${fileName}][${methodName}]`, '\n', res.data);
                }
                throw new Error("response object  is empty");
            }
            userResponsesRM.init(
                res.data.timestamp,
                res.data.message,
                res.data.location,
                res.data.httpCodeMessage
            );
            res.data.data.forEach(response => {
                userResponsesRM.addResponse (
                    response.id,
                    response.idForm,
                    response.idUser,
                    response.idDomain,
                    response.idQuery,
                    response.idResponse,
                    response.idOwner,
                    response.content,
                    response.dtCompletion
                );
            });
            if ( this.isDebug ) {
                console.log(`[${fileName}][${methodName}]`, '\n', userResponsesRM);
            }
            return userResponsesRM;
        })
        .catch(error => {
            if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                console.log(`[${fileName}][${methodName}][error.response.data]`, '\n', error.response.data);
                console.log(`[${fileName}][${methodName}][error.response.status]`, '\n', error.response.status);
                console.log(`[${fileName}][${methodName}][error.response.headers]`, '\n', error.response.headers);
                throw new Error (`ERROR in [${fileName}][${methodName}] - The status code of the response is not OK : status code [${error.response.status}]`);
            }
            else if (error.request) {
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                // http.ClientRequest in node.js
                console.log(`[${fileName}][${methodName}][error.request]`, '\n', error.request);
                throw new Error (`ERROR from the request in [${fileName}][${methodName}] - no response was recieved`);
            } else {
                // Something happened in setting up the request that triggered an Error
                console.log(`[${fileName}][${methodName}][error.message]`, '\n', error.message);
                console.log(`[${fileName}][${methodName}][error.config]`, '\n', error.config);
                throw new Error (`ERROR from the request in [${fileName}][${methodName}]`);
            }
        });
    }

    findAllUserResponsesByForm    = (idForm, idUser) => {
        const methodName            = "findAllUserResponsesByForm";
        const pathService       = `${this.config.eaeServicesPath}/${this.rootUrlService}/forms/${idForm}/users/${idUser}/responses`;
        let userResponsesRM     = new EaeListUserResponsesRequestMapping();

        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][pathService]`, '\n', pathService);
        }
        
        return axios.get(pathService)
        .then(res => {
            if ( utils.isEmpty(res.data) ) {
                throw new Error("response data is empty");
            }
            if ( utils.isEmptyObject(res.data.data) ) {
                if ( this.isDebug ) {
                    console.log(`[${fileName}][${methodName}]`, '\n', res.data);
                }
                return {};
            }
            userResponsesRM.init(
                res.data.timestamp,
                res.data.message,
                res.data.location,
                res.data.httpCodeMessage
            );
            res.data.data.forEach(response => {
                userResponsesRM.addResponse (
                    response.id,
                    response.idForm,
                    response.idUser,
                    response.idDomain,
                    response.idQuery,
                    response.idResponse,
                    response.idOwner,
                    response.content,
                    response.dtCompletion
                );
            });
            if ( this.isDebug ) {
                console.log(`[${fileName}][${methodName}]`, '\n', userResponsesRM);
            }
            return userResponsesRM;
        })
        .catch(error => {
            if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                console.log(`[${fileName}][${methodName}][error.response.data]`, '\n', error.response.data);
                console.log(`[${fileName}][${methodName}][error.response.status]`, '\n', error.response.status);
                console.log(`[${fileName}][${methodName}][error.response.headers]`, '\n', error.response.headers);
                throw new Error (`ERROR in [${fileName}][${methodName}] - The status code of the response is not OK : status code [${error.response.status}]`);
            }
            else if (error.request) {
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                // http.ClientRequest in node.js
                console.log(`[${fileName}][${methodName}][error.request]`, '\n', error.request);
                throw new Error (`ERROR from the request in [${fileName}][${methodName}] - no response was recieved`);
            } else {
                // Something happened in setting up the request that triggered an Error
                console.log(`[${fileName}][${methodName}][error.message]`, '\n', error.message);
                console.log(`[${fileName}][${methodName}][error.config]`, '\n', error.config);
                throw new Error (`ERROR from the request in [${fileName}][${methodName}]`);
            }
        });
    }

    addNewUserFormResponses  = (jsonEaeUserFormResponse) => {
        const methodName            = "addNewUserFormResponses";
        const pathService           = `${this.config.eaeServicesPath}/${this.rootUrlService}/responses/adduserresponses`;
        const userResponsesRM       = new EaeListUserResponsesRequestMapping();

        return axios.post(
            pathService,
            jsonEaeUserFormResponse)
        .then(res => {
            if ( utils.isEmpty(res.data) ) {
                throw new Error("response data is empty");
            }
            if ( utils.isEmpty(res.data.data) ) { // no user form created
                console.log(`[${fileName}][${methodName}][no user form found]`, '\n', res.data.message);
                if ( this.isDebug ) {
                    console.log(`[${fileName}][${methodName}][res.data]`, '\n', res.data);
                }
                return {};
            }
            userResponsesRM.init(
                res.data.timestamp,
                res.data.message,
                res.data.location,
                res.data.httpCodeMessage
            );
            res.data.data.forEach(response => {
                userResponsesRM.addResponse (
                    response.id,
                    response.idForm,
                    response.idUser,
                    response.idDomain,
                    response.idQuery,
                    response.idResponse,
                    response.idOwner,
                    response.content,
                    response.dtCompletion
                );
            });
            if ( this.isDebug ) {
                console.log(`[${fileName}][${methodName}][userResponsesRM]`, '\n', userResponsesRM);
            }
            return userResponsesRM;
        })
        .catch(error => {
            if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                console.log(`[${fileName}][${methodName}][error.response.data]`, '\n', error.response.data);
                console.log(`[${fileName}][${methodName}][error.response.status]`, '\n', error.response.status);
                console.log(`[${fileName}][${methodName}][error.response.headers]`, '\n', error.response.headers);
                throw new Error (`ERROR in [${fileName}][${methodName}] - The status code of the response is not OK : status code [${error.response.status}]`);
            }
            else if (error.request) {
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                // http.ClientRequest in node.js
                console.log(`[${fileName}][${methodName}][error.request]`, '\n', error.request);
                throw new Error (`ERROR from the request in [${fileName}][${methodName}] - no response was recieved`);
            } else {
                // Something happened in setting up the request that triggered an Error
                console.log(`[${fileName}][${methodName}][error.message]`, '\n', error.message);
                console.log(`[${fileName}][${methodName}][error.config]`, '\n', error.config);
                throw new Error (`ERROR from the request in [${fileName}][${methodName}]`);
            }
        });
    }

    removeResponses      = (jsonEaeUserFormResponse) => {
        const methodName            = "removeResponses";
        const pathService           = `${this.config.eaeServicesPath}/${this.rootUrlService}/responses/removeuserresponses`;

        return axios.post(
            pathService,
            jsonEaeUserFormResponse)
        .then(res => {
            if ( utils.isEmpty(res.data) ) {
                throw new Error("response data is empty");
            }
            if ( this.isDebug ) {
                console.log(`[${fileName}][${methodName}][user responses have been deleted]`);
            }
            return true;
        })
        .catch(error => {
            if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                console.log(`[${fileName}][${methodName}][error.response.data]`, '\n', error.response.data);
                console.log(`[${fileName}][${methodName}][error.response.status]`, '\n', error.response.status);
                console.log(`[${fileName}][${methodName}][error.response.headers]`, '\n', error.response.headers);
                throw new Error (`ERROR in [${fileName}][${methodName}] - The status code of the response is not OK : status code [${error.response.status}]`);
            }
            else if (error.request) {
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                // http.ClientRequest in node.js
                console.log(`[${fileName}][${methodName}][error.request]`, '\n', error.request);
                throw new Error (`ERROR from the request in [${fileName}][${methodName}] - no response was recieved`);
            } else {
                // Something happened in setting up the request that triggered an Error
                console.log(`[${fileName}][${methodName}][error.message]`, '\n', error.message);
                console.log(`[${fileName}][${methodName}][error.config]`, '\n', error.config);
                throw new Error (`ERROR from the request in [${fileName}][${methodName}]`);
            }
        });
    }
}