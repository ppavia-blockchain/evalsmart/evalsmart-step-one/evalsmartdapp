import axios from 'axios';
import * as EaeConst from '../../EaeConst';
import * as utils from '../../../utils/EvalsmartUtils';
import EaeUserFormRequestMapping from '../../requestmapping/userform-representation/EaeUserFormRequestMapping';
import EaeListUserFormsRequestMapping from '../../requestmapping/userform-representation/EaeListUserFormsRequestMapping';

const fileName     = "EaeUserFormRepository";

export default class EaeUserFormRepository {
    rootUrlService      = EaeConst.ROOT_USER_FORM_URI;
    isDebug             = false;

    constructor (config={log:{isDebug:false, package:[]}}) {
        this.config     = config;
        this.isDebug = utils.isDebug (config.log, fileName);
    }

    findById    = (idForm, idUser) => {
        const methodName            = "findById";
        const pathService           = `${this.config.eaeServicesPath}/${this.rootUrlService}/forms/${idForm}/users/${idUser}`;
        let userFormRM              = new EaeUserFormRequestMapping();
        
        return axios.get(pathService)
        .then(res => {
            if ( utils.isEmpty(res.data) ) {
                throw new Error("response data is empty");
            }
            if ( utils.isEmpty(res.data.data) ) { // no user form found
                console.log(`[${fileName}][${methodName}][no user form found]`, '\n', res.data.message);
                if ( this.isDebug ) {
                    console.log(`[${fileName}][${methodName}][res.data]`, '\n', res.data);
                }
                return {};
            }
            userFormRM.init(
                res.data.timestamp,
                res.data.message,
                res.data.location,
                res.data.httpCodeMessage,
                res.data.data.id,
                res.data.data.idInterview,
                res.data.data.idForm,
                res.data.data.idUser,
                res.data.data.userRole,
                res.data.data.dtCreated,
                res.data.data.dtClosed,
                res.data.data.dtHistorized,
                res.data.data.idManager,
                res.data.data.dtOwnerValidated,
                res.data.data.dtManagerValidated,
                res.data.data.dtOwnerSigned,
                res.data.data.dtManagerSigned,
                res.data.data.status,
                res.data.data.completion
            );
            if ( this.isDebug ) {
                console.log(`[${fileName}][${methodName}][userFormRM]`, '\n', userFormRM);
            }
            return userFormRM;
        })
        .catch(error => {
            if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                console.log(`[${fileName}][${methodName}][error.response.data]`, '\n', error.response.data);
                console.log(`[${fileName}][${methodName}][error.response.status]`, '\n', error.response.status);
                console.log(`[${fileName}][${methodName}][error.response.headers]`, '\n', error.response.headers);
                throw new Error (`ERROR in [${fileName}][${methodName}] - The status code of the response is not OK : status code [${error.response.status}]`);
            }
            else if (error.request) {
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                // http.ClientRequest in node.js
                console.log(`[${fileName}][${methodName}][error.request]`, '\n', error.request);
                throw new Error (`ERROR from the request in [${fileName}][${methodName}] - no response was recieved`);
            } else {
                // Something happened in setting up the request that triggered an Error
                console.log(`[${fileName}][${methodName}][error.message]`, '\n', error.message);
                console.log(`[${fileName}][${methodName}][error.config]`, '\n', error.config);
                throw new Error (`ERROR from the request in [${fileName}][${methodName}]`);
            }
        });
    }


    findActiveUserFormByUser    = (idUser) => {
        const methodName            = "findActiveUserFormByUser";
        const pathService           = `${this.config.eaeServicesPath}/${this.rootUrlService}/forms/active/users/${idUser}`;
        const userFormRM            = new EaeUserFormRequestMapping();

        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][pathService]`, '\n', pathService);
        }
        
        return axios.get(pathService)
        .then(res => {
            if ( utils.isEmpty(res.data) ) {
                throw new Error("response data is empty");
            }
            if ( utils.isEmpty(res.data.data) ) { // no user form found
                if ( this.isDebug ) {
                    console.log(`[${fileName}][${methodName}][no user form found]`, '\n', res.data.message);
                    console.log(`[${fileName}][${methodName}][res.data]`, '\n', res.data);
                }
                return {};
            }
            userFormRM.init(
                res.data.timestamp,
                res.data.message,
                res.data.location,
                res.data.httpCodeMessage,
                res.data.data.id,
                res.data.data.idInterview,
                res.data.data.idForm,
                res.data.data.idUser,
                res.data.data.userRole,
                res.data.data.dtCreated,
                res.data.data.dtClosed,
                res.data.data.dtHistorized,
                res.data.data.idManager,
                res.data.data.dtOwnerValidated,
                res.data.data.dtManagerValidated,
                res.data.data.dtOwnerSigned,
                res.data.data.dtManagerSigned,
                res.data.data.status,
                res.data.data.completion
            );
            if ( this.isDebug ) {
                console.log(`[${fileName}][${methodName}][userFormRM]`, '\n', userFormRM);
            }
            return userFormRM;
        })
        .catch(error => {
            if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                console.log(`[${fileName}][${methodName}][error.response.data]`, '\n', error.response.data);
                console.log(`[${fileName}][${methodName}][error.response.status]`, '\n', error.response.status);
                console.log(`[${fileName}][${methodName}][error.response.headers]`, '\n', error.response.headers);
                throw new Error (`ERROR in [${fileName}][${methodName}] - The status code of the response is not OK : status code [${error.response.status}]`);
            }
            else if (error.request) {
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                // http.ClientRequest in node.js
                console.log(`[${fileName}][${methodName}][error.request]`, '\n', error.request);
                throw new Error (`ERROR from the request in [${fileName}][${methodName}] - no response was recieved`);
            } else {
                // Something happened in setting up the request that triggered an Error
                console.log(`[${fileName}][${methodName}][error.message]`, '\n', error.message);
                console.log(`[${fileName}][${methodName}][error.config]`, '\n', error.config);
                throw new Error (`ERROR from the request in [${fileName}][${methodName}]`);
            }
        });
    }

    findByIdManager    = (idManager) => {
        const methodName            = "findByIdManager";
        const pathService           = `${this.config.eaeServicesPath}/${this.rootUrlService}/forms/byManager/${idManager}`;
        let userFormsRM             = new EaeListUserFormsRequestMapping();

        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][pathService]`, '\n', pathService);
        }
        
        return axios.get(pathService)
        .then(res => {
            if ( utils.isEmpty(res.data) ) {
                throw new Error("response data is empty");
            }
            if ( utils.isEmptyObject(res.data.data) ) {
                if ( this.isDebug ) {
                    console.log(`[${fileName}][${methodName}] no user form for this manager`, '\n', idManager);
                }
                return userFormsRM;
            }
            userFormsRM.init(
                res.data.timestamp,
                res.data.message,
                res.data.location,
                res.data.httpCodeMessage
            );
            res.data.data.forEach(userForm => {
                userFormsRM.addForm (
                    userForm.id,
                    userForm.idInterview,
                    userForm.idForm,
                    userForm.idUser,
                    userForm.userRole,
                    userForm.dtCreated,
                    userForm.dtClosed,
                    userForm.dtHistorized,
                    userForm.idManager,
                    userForm.dtOwnerValidated,
                    userForm.dtManagerValidated,
                    userForm.dtOwnerSigned,
                    userForm.dtManagerSigned,
                    userForm.status,
                    userForm.completion
                );
            });
            if ( this.isDebug ) {
                console.log(`[${fileName}][${methodName}][userFormsRM]`, '\n', userFormsRM);
            }
            return userFormsRM;
        })
        .catch(error => {
            if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                console.log(`[${fileName}][${methodName}][error.response.data]`, '\n', error.response.data);
                console.log(`[${fileName}][${methodName}][error.response.status]`, '\n', error.response.status);
                console.log(`[${fileName}][${methodName}][error.response.headers]`, '\n', error.response.headers);
                throw new Error (`ERROR in [${fileName}][${methodName}] - The status code of the response is not OK : status code [${error.response.status}]`);
            }
            else if (error.request) {
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                // http.ClientRequest in node.js
                console.log(`[${fileName}][${methodName}][error.request]`, '\n', error.request);
                throw new Error (`ERROR from the request in [${fileName}][${methodName}] - no response was recieved`);
            } else {
                // Something happened in setting up the request that triggered an Error
                console.log(`[${fileName}][${methodName}][error.message]`, '\n', error.message);
                console.log(`[${fileName}][${methodName}][error.config]`, '\n', error.config);
                throw new Error (`ERROR from the request in [${fileName}][${methodName}]`);
            }
        });
    }

    addNewUserForm  = (jsonEaeUserForm) => {
        const methodName            = "addNewUserForm";
        const pathService           = `${this.config.eaeServicesPath}/${this.rootUrlService}/forms/adduserform`;
        const userFormRM            = new EaeUserFormRequestMapping();

        return axios.post(
            pathService,
            jsonEaeUserForm)
        .then(res => {
            if ( utils.isEmpty(res.data) ) {
                throw new Error("response data is empty");
            }
            if ( utils.isEmpty(res.data.data) ) { // no user form created
                console.log(`[${fileName}][${methodName}][no user form found]`, '\n', res.data.message);
                if ( this.isDebug ) {
                    console.log(`[${fileName}][${methodName}][res.data]`, '\n', res.data);
                }
                return {};
            }
            userFormRM.init(
                res.data.timestamp,
                res.data.message,
                res.data.location,
                res.data.httpCodeMessage,
                res.data.data.id,
                res.data.data.idInterview,
                res.data.data.idForm,
                res.data.data.idUser,
                res.data.data.userRole,
                res.data.data.dtCreated,
                res.data.data.dtClosed,
                res.data.data.dtHistorized,
                res.data.data.idManager,
                res.data.data.dtOwnerValidated,
                res.data.data.dtManagerValidated,
                res.data.data.dtOwnerSigned,
                res.data.data.dtManagerSigned,
                res.data.data.status,
                res.data.data.completion
            );
            if ( this.isDebug ) {
                console.log(`[${fileName}][${methodName}][userFormRM]`, '\n', userFormRM);
            }
            return userFormRM;
        })
        .catch(error => {
            if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                console.log(`[${fileName}][${methodName}][error.response.data]`, '\n', error.response.data);
                console.log(`[${fileName}][${methodName}][error.response.status]`, '\n', error.response.status);
                console.log(`[${fileName}][${methodName}][error.response.headers]`, '\n', error.response.headers);
                throw new Error (`ERROR in [${fileName}][${methodName}] - The status code of the response is not OK : status code [${error.response.status}]`);
            }
            else if (error.request) {
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                // http.ClientRequest in node.js
                console.log(`[${fileName}][${methodName}][error.request]`, '\n', error.request);
                throw new Error (`ERROR from the request in [${fileName}][${methodName}] - no response was recieved`);
            } else {
                // Something happened in setting up the request that triggered an Error
                console.log(`[${fileName}][${methodName}][error.message]`, '\n', error.message);
                console.log(`[${fileName}][${methodName}][error.config]`, '\n', error.config);
                throw new Error (`ERROR from the request in [${fileName}][${methodName}]`);
            }
        });
    }

    updateUserFormDtValidated = (idForm, idUser, dtValidated, userRole) => {
        const methodName            = "updateUserFormDtValidated";
        const pathService           = `${this.config.eaeServicesPath}/${this.rootUrlService}/forms/${idForm}/users/${idUser}/validate`;
        const userFormRM            = new EaeUserFormRequestMapping();
        let jsonData                = {};

        switch (userRole) {
            case EaeConst.USER_ROLE_CODE_COLLABORATOR :
                    jsonData    = {
                        "dtValidated" : dtValidated,
                        "userRole" : EaeConst.USER_ROLE_CODE_COLLABORATOR
                    };
            break;
            case EaeConst.USER_ROLE_CODE_MANAGER :
                    jsonData    = {
                        "dtValidated" : dtValidated,
                        "userRole" : EaeConst.USER_ROLE_CODE_MANAGER
                    };
            break;
            default :
            return null;
        }

        return axios.post(
            pathService,
            jsonData)
        .then(res => {
            if ( utils.isEmpty(res.data) ) {
                throw new Error("response data is empty");
            }
            userFormRM.init(
                res.data.timestamp,
                res.data.message,
                res.data.location,
                res.data.httpCodeMessage,
                res.data.data.id,
                res.data.data.idInterview,
                res.data.data.idForm,
                res.data.data.idUser,
                res.data.data.userRole,
                res.data.data.dtCreated,
                res.data.data.dtClosed,
                res.data.data.dtHistorized,
                res.data.data.idManager,
                res.data.data.dtOwnerValidated,
                res.data.data.dtManagerValidated,
                res.data.data.dtOwnerSigned,
                res.data.data.dtManagerSigned,
                res.data.data.status,
                res.data.data.completion
            );
            if ( this.isDebug ) {
                console.log(`[${fileName}][${methodName}][userFormRM]`, '\n', userFormRM);
            }
            return userFormRM;
        })
        .catch(error => {
            if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                console.log(`[${fileName}][${methodName}][error.response.data]`, '\n', error.response.data);
                console.log(`[${fileName}][${methodName}][error.response.status]`, '\n', error.response.status);
                console.log(`[${fileName}][${methodName}][error.response.headers]`, '\n', error.response.headers);
                throw new Error (`ERROR in [${fileName}][${methodName}] - The status code of the response is not OK : status code [${error.response.status}]`);
            }
            else if (error.request) {
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                // http.ClientRequest in node.js
                console.log(`[${fileName}][${methodName}][error.request]`, '\n', error.request);
                throw new Error (`ERROR from the request in [${fileName}][${methodName}] - no response was recieved`);
            } else {
                // Something happened in setting up the request that triggered an Error
                console.log(`[${fileName}][${methodName}][error.message]`, '\n', error.message);
                console.log(`[${fileName}][${methodName}][error.config]`, '\n', error.config);
                throw new Error (`ERROR from the request in [${fileName}][${methodName}]`);
            }
        });

    }
}