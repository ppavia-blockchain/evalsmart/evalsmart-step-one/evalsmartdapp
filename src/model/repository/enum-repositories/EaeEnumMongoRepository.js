import axios from 'axios';
import * as EaeConst from '../../EaeConst';
import * as utils from '../../../utils/EvalsmartUtils';
import EaeStatusFormRequestMapping from '../../requestmapping/enums-representation/EaeStatusFormRequestMapping';
import EaeOneStatusFormRequestMapping from '../../requestmapping/enums-representation/EaeOneStatusFormRequestMapping';

const fileName   = "EaeEnumMongoRepository";

export default class EaeEnumMongoRepository {
    rootNameService      = EaeConst.ROOT_ENUM_URI;
    isDebug     = false;

    constructor (config={log:{isDebug:false, package:[]}}) {
        this.config     = config;
        this.isDebug = false; //utils.isDebug (config.log, fileName);
    }

    findAllStatusForm          = async () => {
        const methodName            = "findAllStatusForm";
        const pathService         = `${this.config.eaeServicesPath}/${this.rootNameService}/all-statusform`;
        let statusFormRequestMapping  = new EaeStatusFormRequestMapping();

        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][pathService]`, '\n', pathService);
        }

        return axios.get(pathService)
        .then(res => {
            statusFormRequestMapping.init(
                res.data.timestamp,
                res.data.message,
                res.data.location,
                res.data.httpCodeMessage
            );
            res.data.data.forEach(status => {
                statusFormRequestMapping.addStatus (
                    status.code,
                    status.label
                );                
            });
            if ( this.isDebug ) {
                console.log(`[${fileName}][${methodName}][statusFormRequestMapping]`, '\n', statusFormRequestMapping);
            }
            return statusFormRequestMapping;
        })
        .catch(error => {
            if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                console.log(`[${fileName}][${methodName}][error.response.data]`, '\n', error.response.data);
                console.log(`[${fileName}][${methodName}][error.response.status]`, '\n', error.response.status);
                console.log(`[${fileName}][${methodName}][error.response.headers]`, '\n', error.response.headers);
                throw new Error (`ERROR in [${fileName}][${methodName}] - The status code of the response is not OK : status code [${error.response.status}]`);
            }
            else if (error.request) {
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                // http.ClientRequest in node.js
                console.log(`[${fileName}][${methodName}][error.request]`, '\n', error.request);
                throw new Error (`ERROR from the request in [${fileName}][${methodName}] - no response was recieved`);
            } else {
                // Something happened in setting up the request that triggered an Error
                console.log(`[${fileName}][${methodName}][error.message]`, '\n', error.message);
                console.log(`[${fileName}][${methodName}][error.config]`, '\n', error.config);
                throw new Error (`ERROR from the request in [${fileName}][${methodName}]`);
            }
        });
    }
    
    findStatusFormByStatusCode          = (statusCode) => {
        const methodName          = "findStatusFormByStatusCode";
        const pathService         = `${this.config.eaeServicesPath}/${this.rootNameService}/statusform/${statusCode}`;
        let statusFormRequestMapping  = new EaeOneStatusFormRequestMapping();

        return axios.get(pathService)
        .then(res => {
            statusFormRequestMapping.init(
                res.data.timestamp,
                res.data.message,
                res.data.location,
                res.data.httpCodeMessage,
                res.data.data.code,
                res.data.data.label
            );
            if ( this.isDebug ) {
                console.log(`[${fileName}][${methodName}]`, '\n', statusFormRequestMapping);
            }
            return statusFormRequestMapping;
        })
        .catch(error => {
            if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                console.log(`[${fileName}][${methodName}][error.response.data]`, '\n', error.response.data);
                console.log(`[${fileName}][${methodName}][error.response.status]`, '\n', error.response.status);
                console.log(`[${fileName}][${methodName}][error.response.headers]`, '\n', error.response.headers);
                throw new Error (`ERROR in [${fileName}][${methodName}] - The status code of the response is not OK : status code [${error.response.status}]`);
            }
            else if (error.request) {
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                // http.ClientRequest in node.js
                console.log(`[${fileName}][${methodName}][error.request]`, '\n', error.request);
                throw new Error (`ERROR from the request in [${fileName}][${methodName}] - no response was recieved`);
            } else {
                // Something happened in setting up the request that triggered an Error
                console.log(`[${fileName}][${methodName}][error.message]`, '\n', error.message);
                console.log(`[${fileName}][${methodName}][error.config]`, '\n', error.config);
                throw new Error (`ERROR from the request in [${fileName}][${methodName}]`);
            }
        });
    }
}
