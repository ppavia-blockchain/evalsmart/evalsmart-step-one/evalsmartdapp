import axios from 'axios';
import * as EaeConst from '../../EaeConst';
import * as utils from '../../../utils/EvalsmartUtils';
import EaeFormStructListResponsesRequestMapping from '../../requestmapping/formstruct-representation/EaeFormStructListResponsesRequestMapping';
import EaeFormStructResponseRequestMapping from '../../requestmapping/formstruct-representation/EaeFormStructResponseRequestMapping';

const fileName     = "EaeFormStructResponseMongoRepository";

export default class EaeFormStructResponseMongoRepository {
    rootUrlService      = EaeConst.ROOT_FORM_STRUCT_RESPONSE_URI;
    isDebug             = false;

    constructor (config={log:{isDebug:false, package:[]}}) {
        this.config     = config;
        this.isDebug = utils.isDebug (config.log, fileName);
    }
    
    findAllResponsesByForm    = (idForm) => {
        const methodName            = "findAllResponsesByForm";
        const pathService               = `${this.config.eaeServicesPath}/${this.rootUrlService}/forms/${idForm}/responses`;
        let formStructResponsesRM         = new EaeFormStructListResponsesRequestMapping();
        
        return axios.get(pathService)
        .then(res => {
            if ( utils.isEmpty(res.data) ) {
                throw new Error("response data is empty");
            }
            if ( utils.isEmpty(res.data.data) ) {
                console.log(res.data);
                throw new Error("response object  is empty");
            }
            formStructResponsesRM.init(
                res.data.timestamp,
                res.data.message,
                res.data.location,
                res.data.httpCodeMessage
            );
            res.data.data.forEach(response => {
                formStructResponsesRM.addResponse (
                    response.id,
                    response.idResponse,
                    response.label,
                    response.idQuery,
                    response.idDomain,
                    response.idForm,
                    response.responseType,
                    response.content,
                    response.level
                );
            });

            if ( this.isDebug ) {
                console.log(`[${fileName}][${methodName}]`, '\n', formStructResponsesRM);
            }
            return formStructResponsesRM;
        })
        .catch(error => {
            if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                console.log(`[${fileName}][${methodName}][error.response.data]`, '\n', error.response.data);
                console.log(`[${fileName}][${methodName}][error.response.status]`, '\n', error.response.status);
                console.log(`[${fileName}][${methodName}][error.response.headers]`, '\n', error.response.headers);
                throw new Error (`ERROR in [${fileName}][${methodName}] - The status code of the response is not OK : status code [${error.response.status}]`);
            }
            else if (error.request) {
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                // http.ClientRequest in node.js
                console.log(`[${fileName}][${methodName}][error.request]`, '\n', error.request);
                throw new Error (`ERROR from the request in [${fileName}][${methodName}] - no response was recieved`);
            } else {
                // Something happened in setting up the request that triggered an Error
                console.log(`[${fileName}][${methodName}][error.message]`, '\n', error.message);
                console.log(`[${fileName}][${methodName}][error.config]`, '\n', error.config);
                throw new Error (`ERROR from the request in [${fileName}][${methodName}]`);
            }
        });
    }

    findAllResponsesByDomain    = (idDomain) => {
        const methodName            = "findAllResponsesByDomain";
        const pathService               = `${this.config.eaeServicesPath}/${this.rootUrlService}/domains/${idDomain}/responses`;
        let formStructResponsesRM         = new EaeFormStructListResponsesRequestMapping();
        
        return axios.get(pathService)
        .then(res => {
            if ( utils.isEmpty(res.data) ) {
                throw new Error("response data is empty");
            }
            if ( utils.isEmpty(res.data.data) ) {
                console.log(res.data);
                throw new Error("response object  is empty");
            }
            formStructResponsesRM.init(
                res.data.timestamp,
                res.data.message,
                res.data.location,
                res.data.httpCodeMessage
            );
            res.data.data.forEach(response => {
                formStructResponsesRM.addResponse (
                    response.id,
                    response.idResponse,
                    response.label,
                    response.idQuery,
                    response.idDomain,
                    response.idForm,
                    response.responseType,
                    response.content,
                    response.level
                );
            });

            if ( this.isDebug ) {
                console.log(`[${fileName}][${methodName}]`, '\n', formStructResponsesRM);
            }
            return formStructResponsesRM;
        })
        .catch(error => {
            if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                console.log(`[${fileName}][${methodName}][error.response.data]`, '\n', error.response.data);
                console.log(`[${fileName}][${methodName}][error.response.status]`, '\n', error.response.status);
                console.log(`[${fileName}][${methodName}][error.response.headers]`, '\n', error.response.headers);
                throw new Error (`ERROR in [${fileName}][${methodName}] - The status code of the response is not OK : status code [${error.response.status}]`);
            }
            else if (error.request) {
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                // http.ClientRequest in node.js
                console.log(`[${fileName}][${methodName}][error.request]`, '\n', error.request);
                throw new Error (`ERROR from the request in [${fileName}][${methodName}] - no response was recieved`);
            } else {
                // Something happened in setting up the request that triggered an Error
                console.log(`[${fileName}][${methodName}][error.message]`, '\n', error.message);
                console.log(`[${fileName}][${methodName}][error.config]`, '\n', error.config);
                throw new Error (`ERROR from the request in [${fileName}][${methodName}]`);
            }
        });
    }

    findAllResponsesByQuery    = (idQuery) => {
        const methodName            = "findAllResponsesByQuery";
        const pathService               = `${this.config.eaeServicesPath}/${this.rootUrlService}/queries/${idQuery}/responses`;
        let formStructResponsesRM         = new EaeFormStructListResponsesRequestMapping();
        
        return axios.get(pathService)
        .then(res => {
            if ( utils.isEmpty(res.data) ) {
                throw new Error("response data is empty");
            }
            if ( utils.isEmpty(res.data.data) ) {
                console.log(res.data);
                throw new Error("response object  is empty");
            }
            formStructResponsesRM.init(
                res.data.timestamp,
                res.data.message,
                res.data.location,
                res.data.httpCodeMessage
            );
            res.data.data.forEach(response => {
                formStructResponsesRM.addResponse (
                    response.id,
                    response.idResponse,
                    response.label,
                    response.idQuery,
                    response.idDomain,
                    response.idForm,
                    response.responseType,
                    response.content,
                    response.level
                );
            });

            if ( this.isDebug ) {
                console.log(`[${fileName}][${methodName}]`, '\n', formStructResponsesRM);
            }
            return formStructResponsesRM;
        })
        .catch(error => {
            if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                console.log(`[${fileName}][${methodName}][error.response.data]`, '\n', error.response.data);
                console.log(`[${fileName}][${methodName}][error.response.status]`, '\n', error.response.status);
                console.log(`[${fileName}][${methodName}][error.response.headers]`, '\n', error.response.headers);
              } else if (error.request) {
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                // http.ClientRequest in node.js
                console.log(`[${fileName}][${methodName}][error.request]`, '\n', error.request);
              } else {
                // Something happened in setting up the request that triggered an Error
                console.log(`[${fileName}][${methodName}][error.message]`, '\n', error.message);
              }
              console.log(`[${fileName}][${methodName}][error.config]`, '\n', error.config);
        });
    }
}