import axios from 'axios';
import * as EaeConst from '../../EaeConst';
import * as utils from '../../../utils/EvalsmartUtils';
import EaeFormStructListQueriesRequestMapping from '../../requestmapping/formstruct-representation/EaeFormStructListQueriesRequestMapping';
import EaeFormStructQueryRequestMapping from '../../requestmapping/formstruct-representation/EaeFormStructQueryRequestMapping';

const fileName     = "EaeFormStructQueryMongoRepository";

export default class EaeFormStructQueryMongoRepository {
    rootUrlService      = EaeConst.ROOT_FORM_STRUCT_QUERY_URI;
    isDebug             = false;

    constructor (config={log:{isDebug:false, package:[]}}) {
        this.config     = config;
        this.isDebug = utils.isDebug (config.log, fileName);
    }
    
    /**
     * find all queries of a specific form from Eae Service REST
     */
    findAllQueriesByForm    = (idForm) => {
        const methodName            = "findAllQueriesByForm";
        const pathService               = `${this.config.eaeServicesPath}/${this.rootUrlService}/forms/${idForm}/queries`;
        let formStructQueriesRM         = new EaeFormStructListQueriesRequestMapping();
        
        return axios.get(pathService)
        .then(res => {
            if ( utils.isEmpty(res.data) ) {
                throw new Error("response data is empty");
            }
            if ( utils.isEmpty(res.data.data) ) {
                console.log(res.data);
                throw new Error("response object  is empty");
            }
            formStructQueriesRM.init(
                res.data.timestamp,
                res.data.message,
                res.data.location,
                res.data.httpCodeMessage
            );
            res.data.data.forEach(query => {
                formStructQueriesRM.addQuery (
                    query.id,
                    query.idQuery,
                    query.idDomain,
                    query.idForm,
                    query.label,
                    query.queryType,
                    query.order
                );
            });

            if ( this.isDebug ) {
                console.log(`[${fileName}][${methodName}]`, '\n', formStructQueriesRM);
            }
            return formStructQueriesRM;
        })
        .catch(error => {
            if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                console.log(`[${fileName}][${methodName}][error.response.data]`, '\n', error.response.data);
                console.log(`[${fileName}][${methodName}][error.response.status]`, '\n', error.response.status);
                console.log(`[${fileName}][${methodName}][error.response.headers]`, '\n', error.response.headers);
                throw new Error (`ERROR in [${fileName}][${methodName}] - The status code of the response is not OK : status code [${error.response.status}]`);
            }
            else if (error.request) {
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                // http.ClientRequest in node.js
                console.log(`[${fileName}][${methodName}][error.request]`, '\n', error.request);
                throw new Error (`ERROR from the request in [${fileName}][${methodName}] - no response was recieved`);
            } else {
                // Something happened in setting up the request that triggered an Error
                console.log(`[${fileName}][${methodName}][error.message]`, '\n', error.message);
                console.log(`[${fileName}][${methodName}][error.config]`, '\n', error.config);
                throw new Error (`ERROR from the request in [${fileName}][${methodName}]`);
            }
        });
    }

    /**
     * find all queries of a specific form from Eae Service REST
     */
    findAllQueriesByDomain    = (idDomain) => {
        const methodName            = "findAllQueriesByDomain";
        const pathService               = `${this.config.eaeServicesPath}/${this.rootUrlService}/domains/${idDomain}/queries`;
        let formStructQueriesRM         = new EaeFormStructListQueriesRequestMapping();
        
        return axios.get(pathService)
        .then(res => {
            if ( utils.isEmpty(res.data) ) {
                throw new Error("response data is empty");
            }
            if ( utils.isEmpty(res.data.data) ) {
                console.log(res.data);
                throw new Error("response object  is empty");
            }
            formStructQueriesRM.init(
                res.data.timestamp,
                res.data.message,
                res.data.location,
                res.data.httpCodeMessage
            );
            res.data.data.forEach(query => {
                formStructQueriesRM.addQuery (
                    query.id,
                    query.idQuery,
                    query.idDomain,
                    query.idForm,
                    query.label,
                    query.queryType,
                    query.order
                );
            });

            if ( this.isDebug ) {
                console.log(`[${fileName}][${methodName}]`, '\n', formStructQueriesRM);
            }
            return formStructQueriesRM;
        })
        .catch(error => {
            if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                console.log(`[${fileName}][${methodName}][error.response.data]`, '\n', error.response.data);
                console.log(`[${fileName}][${methodName}][error.response.status]`, '\n', error.response.status);
                console.log(`[${fileName}][${methodName}][error.response.headers]`, '\n', error.response.headers);
                throw new Error (`ERROR in [${fileName}][${methodName}] - The status code of the response is not OK : status code [${error.response.status}]`);
            }
            else if (error.request) {
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                // http.ClientRequest in node.js
                console.log(`[${fileName}][${methodName}][error.request]`, '\n', error.request);
                throw new Error (`ERROR from the request in [${fileName}][${methodName}] - no response was recieved`);
            } else {
                // Something happened in setting up the request that triggered an Error
                console.log(`[${fileName}][${methodName}][error.message]`, '\n', error.message);
                console.log(`[${fileName}][${methodName}][error.config]`, '\n', error.config);
                throw new Error (`ERROR from the request in [${fileName}][${methodName}]`);
            }
        });
    }
}