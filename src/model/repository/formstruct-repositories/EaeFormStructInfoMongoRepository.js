import axios from 'axios';
import * as EaeConst from '../../EaeConst';
import * as utils from '../../../utils/EvalsmartUtils';
import EaeFormStructListInfosRequestMapping from '../../requestmapping/formstruct-representation/EaeFormStructListInfosRequestMapping';
import EaeFormStructInfoRequestMapping from '../../requestmapping/formstruct-representation/EaeFormStructInfoRequestMapping';

const fileName     = "EaeFormStructInfoMongoRepository";

export default class EaeFormStructInfoMongoRepository {
    rootUrlService      = EaeConst.ROOT_FORM_STRUCT_INFO_URI;
    isDebug             = false;

    constructor (config={log:{isDebug:false, package:[]}}) {
        this.config     = config;
        this.isDebug = utils.isDebug (config.log, fileName);
    }
    
    /**
     * find all infos of a specific form from Eae Service REST
     */
    findAllInfosByForm    = (idForm) => {
        const methodName            = "findAllInfosByForm";
        const pathService           = `${this.config.eaeServicesPath}/${this.rootUrlService}/forms/${idForm}/infos`;
        let formStructInfosRM       = new EaeFormStructListInfosRequestMapping();
        
        return axios.get(pathService)
        .then(res => {
            if ( utils.isEmpty(res.data) ) {
                throw new Error("response data is empty");
            }
            if ( utils.isEmpty(res.data.data) ) {
                console.log(res.data);
                throw new Error("response object  is empty");
            }
            formStructInfosRM.init(
                res.data.timestamp,
                res.data.message,
                res.data.location,
                res.data.httpCodeMessage
            );
            res.data.data.forEach(info => {
                formStructInfosRM.addInfo (
                    info.id,
                    info.idInfo,
                    info.idDomain,
                    info.idForm,
                    info.label,
                    info.content,
                    info.order
                );
            });

            if ( this.isDebug ) {
                console.log(`[${fileName}][${methodName}]`, '\n', formStructInfosRM);
            }
            return formStructInfosRM;
        })
        .catch(error => {
            if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                console.log(`[${fileName}][${methodName}][error.response.data]`, '\n', error.response.data);
                console.log(`[${fileName}][${methodName}][error.response.status]`, '\n', error.response.status);
                console.log(`[${fileName}][${methodName}][error.response.headers]`, '\n', error.response.headers);
                throw new Error (`ERROR in [${fileName}][${methodName}] - The status code of the response is not OK : status code [${error.response.status}]`);
            }
            else if (error.request) {
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                // http.ClientRequest in node.js
                console.log(`[${fileName}][${methodName}][error.request]`, '\n', error.request);
                throw new Error (`ERROR from the request in [${fileName}][${methodName}] - no response was recieved`);
            } else {
                // Something happened in setting up the request that triggered an Error
                console.log(`[${fileName}][${methodName}][error.message]`, '\n', error.message);
                console.log(`[${fileName}][${methodName}][error.config]`, '\n', error.config);
                throw new Error (`ERROR from the request in [${fileName}][${methodName}]`);
            }
        });
    }

    /**
     * find all infos of a specific form from Eae Service REST
     */
    findAllInfosByDomain    = (idDomain) => {
        const methodName              = "findAllInfosByDomain";
        const pathService             = `${this.config.eaeServicesPath}/${this.rootUrlService}/domains/${idDomain}/infos`;
        let formStructInfosRM         = new EaeFormStructListInfosRequestMapping();
        
        return axios.get(pathService)
        .then(res => {
            if ( utils.isEmpty(res.data) ) {
                throw new Error("response data is empty");
            }
            if ( utils.isEmpty(res.data.data) ) {
                console.log(res.data);
                throw new Error("response object  is empty");
            }
            formStructInfosRM.init(
                res.data.timestamp,
                res.data.message,
                res.data.location,
                res.data.httpCodeMessage
            );
            res.data.data.forEach(info => {
                formStructInfosRM.addInfo (
                    info.id,
                    info.idInfo,
                    info.idDomain,
                    info.idForm,
                    info.label,
                    info.content,
                    info.order
                );
                if ( this.isDebug ) {
                    console.log(`[${fileName}][${methodName}]`, '\n', info);
                }
            });

            if ( this.isDebug ) {
                console.log(`[${fileName}][${methodName}]`, '\n', formStructInfosRM);
            }
            return formStructInfosRM;
        })
        .catch(error => {
            if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                console.log(`[${fileName}][${methodName}][error.response.data]`, '\n', error.response.data);
                console.log(`[${fileName}][${methodName}][error.response.status]`, '\n', error.response.status);
                console.log(`[${fileName}][${methodName}][error.response.headers]`, '\n', error.response.headers);
                throw new Error (`ERROR in [${fileName}][${methodName}] - The status code of the response is not OK : status code [${error.response.status}]`);
            }
            else if (error.request) {
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                // http.ClientRequest in node.js
                console.log(`[${fileName}][${methodName}][error.request]`, '\n', error.request);
                throw new Error (`ERROR from the request in [${fileName}][${methodName}] - no response was recieved`);
            } else {
                // Something happened in setting up the request that triggered an Error
                console.log(`[${fileName}][${methodName}][error.message]`, '\n', error.message);
                console.log(`[${fileName}][${methodName}][error.config]`, '\n', error.config);
                throw new Error (`ERROR from the request in [${fileName}][${methodName}]`);
            }
        });
    }
}