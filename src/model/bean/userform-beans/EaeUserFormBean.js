export default class EaeUserFormBean {
    constructor (
        id,
        idInterview,
        idForm,
        idUser,
        userRole,
        dtCreated,
        dtClosed,
        dtHistorized,
        idManager,
        dtOwnerValidated,
        dtManagerValidated,
        dtOwnerSigned,
        dtManagerSigned,
        status,
        completion,
        userResponses
    ) {
        this.id					= id;
        this.idInterview        = idInterview;
        this.idForm				= idForm;
        this.idUser				= idUser;
        this.userRole			= userRole;
        this.dtCreated			= dtCreated;
        this.dtClosed			= dtClosed;
        this.dtHistorized		= dtHistorized;
        this.idManager			= idManager;
        this.dtOwnerValidated	= dtOwnerValidated;
        this.dtManagerValidated	= dtManagerValidated;
        this.dtOwnerSigned		= dtOwnerSigned;
        this.dtManagerSigned	= dtManagerSigned;
        this.status				= status;
        this.completion			= completion;
        this.userResponses		= userResponses;
    }

    convertIntoJson = () => {
        return {
            "id": this.id,
            "idInterview": this.idInterview,
            "idForm": this.idForm,
            "idUser": this.idUser,
            "userRole": this.userRole,
            "dtCreated": this.dtCreated,
            "dtClosed": this.dtClosed,
            "dtHistorized": this.dtHistorized,
            "idManager": this.idManager,
            "dtOwnerValidated": this.dtOwnerValidated,
            "dtManagerValidated": this.dtManagerValidated,
            "dtOwnerSigned": this.dtOwnerSigned,
            "dtManagerSigned": this.dtManagerSigned,
            "status": this.status,
            "completion": this.completion,
            "eaeUserFormResponses": this.userResponses
        }
    }
}