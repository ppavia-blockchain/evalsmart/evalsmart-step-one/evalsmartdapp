export default class EaeUserFormResponseBean {
    constructor (
        id,
        idForm,
        idUser,
        idDomain,
        idQuery,
        idResponse,
        idOwner,
        content,
        dtCompletion
    ) {
        this.id						= id;
        this.idForm					= idForm;
        this.idUser					= idUser;
        this.idDomain				= idDomain;
        this.idQuery				= idQuery;
        this.idResponse				= idResponse;
        this.idOwner				= idOwner;
        this.content				= content;
        this.dtCompletion			= dtCompletion;
    }
}