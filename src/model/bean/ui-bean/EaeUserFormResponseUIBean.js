class EaeUserFormResponseUIBean {
    constructor (
        idFormStructResponse,
        idUserResponse,
        idForm,
        idUser,
        idDomain,
        idQuery,
        idResponse,
        idOwner,
        content,
        dtCompletion
    )
}
export default EaeUserFormResponseUIBean;