export default class EaeStatusFormBean {
    constructor(
        code,
        label
    ) {
        this.code       = code;
        this.label      = label;
    }
};