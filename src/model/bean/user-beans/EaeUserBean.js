export default class EaeUserBean {
    constructor(
        idUser,
        profile,
        firstName,
        lastName,
        fullName,
        userName,
        email,
        phone,
        status,
        accounts
    ) {
        this.idUser     = idUser;
        this.profile    = profile;
        this.firstName  = firstName;
        this.lastName   = lastName;
        this.fullName   = fullName;
        this.userName   = userName;
        this.email      = email;
        this.phone      = phone;
        this.status     = status;
        this.accounts   = accounts;
    }
};