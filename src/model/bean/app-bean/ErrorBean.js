export default class ErrorBean {
    constructor (
        code,
        label,
        message
    ) {
        this.code           = code;
        this.label          = label;
        this.message        = message;
    }
}