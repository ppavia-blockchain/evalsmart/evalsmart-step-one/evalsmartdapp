/**
 * this is the bean object manipulable in app
 */
export default class EaeFormStructResponseBean {
    constructor (
        id,
        idResponse,
        label,
        idQuery,
        idDomain,
        idForm,
        responseType,
        content,
        level
        ) {
        this.id                 = id;
        this.idResponse			= idResponse;
        this.label				= label;
        this.idQuery			= idQuery;
        this.idDomain			= idDomain;
        this.idForm				= idForm;
        this.responseType		= responseType;
        this.content			= content;
        this.level				= level;
    }
}