/**
 * this is the bean object manipulable in app
 */
export default class EaeFormStructBean {
    constructor (
        id,
	    idForm,
	    label,
        dtCreated,
        dtClosed,
        dtHistorized,
        status,
        domains
        ) {
            this.id                 = id;
            this.idForm             = idForm;
            this.label              = label;
            this.dtCreated          = dtCreated;
            this.dtClosed           = dtClosed;
            this.dtHistorized       = dtHistorized;
            this.status             = status;
            this.domains            = domains;
    }
}