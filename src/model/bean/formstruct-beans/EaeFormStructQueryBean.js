/**
 * this is the bean object manipulable in app
 */
export default class EaeFormStructQueryBean {
    constructor (
        id,
        idQuery,
        idDomain,
        idForm,
        label,
        queryType,
        order,
        responses
        ) {
            this.id     		= id;
            this.idQuery		= idQuery;
            this.idDomain		= idDomain;
            this.idForm			= idForm;
            this.label			= label;
            this.queryType		= queryType;
            this.order			= order;
            this.responses		= responses;
    }
}