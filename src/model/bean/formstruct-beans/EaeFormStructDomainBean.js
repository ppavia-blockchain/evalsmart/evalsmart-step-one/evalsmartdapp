/**
 * this is the bean object manipulable in app
 */
export default class EaeFormStructDomainBean {
    constructor (
        id,
        idDomain,
        label,
        idForm,
        order,
        levels,
        queries,
        ) {
            this.id     	= id;
            this.idDomain	= idDomain;
            this.label		= label;
            this.idForm		= idForm;
            this.order		= order;
            this.levels		= levels;
            this.queries	= queries;
    }
}