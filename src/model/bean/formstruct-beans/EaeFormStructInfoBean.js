/**
 * this is the bean object manipulable in app
 */
export default class EaeFormStructInfoBean {
    constructor (
        id,
        idInfo,
        idDomain,
        idForm,
        label,
        content,
        order
        ) {
            this.id             = id;
            this.idInfo         = idInfo;
            this.idDomain       = idDomain;
            this.idForm         = idForm;
            this.label          = label;
            this.content        = content;
            this.order          = order;
    }
}