import * as EaeConst from '../model/EaeConst';

const fileName      = "EvalsmartUtils";

export const convertTimestampToDateString = (timestamp, isWithoutT) => {
    const date      = new Date(timestamp);
    console.log ("toDateString " + date.toDateString());
    console.log ("getTime " + date.getTime());
    console.log ("getDay " + date.getUTCDay());
    console.log ("getHours " + date.getHours());
    console.log ("getMinutes " + date.getMinutes());
    console.log ("getSeconds " + date.getSeconds());

    const year      = date.getFullYear();
    const month     = ( date.getMonth() < 10 ) ? `0${date.getMonth() + 1}` : date.getMonth() + 1;
    const day       = ( date.getDate() < 10 ) ? `0${date.getDate()}` : date.getDate();
    const hours     = ( date.getHours() === 0 ) ? `0${date.getHours()}` : date.getHours();
    const minutes   = ( date.getMinutes() === 0 ) ? `0${date.getMinutes()}` : date.getMinutes();
    const secondes  = ( date.getSeconds() === 0 ) ? `0${date.getSeconds()}` : date.getSeconds();
    if ( isWithoutT ) {
        return `${year}-${month}-${day}`;
    }
    return `${year}-${month}-${day} ${hours}:${minutes}:${secondes}`;
}

export const convertIntInMonthString = (numDay) => {
    switch (numDay) {
        case 0 :
            return "january";
        case 1 :
            return "february";
        case 2 :
            return "march";
        case 3 :
            return "april";
        case 4 :
            return "may";
        case 5 :
            return "june";
        case 6 :
            return "july";
        case 7:
            return "august";
        case 8 :
            return "september";
        case 9 :
            return "october";
        case 10 :
            return "november";
        case 11 :
            return "december";
        default :
            return null;
    }
}

export const convertIntInDayString = (numDay) => {
    switch (numDay) {
        case 0 :
            return "sunday";
        case 1 :
            return "monday";
        case 2 :
            return "thirday";
        case 3 :
            return "wednesday";
        case 4 :
            return "tuesday";
        case 5 :
            return "friday";
        case 6 :
            return "saturday";
        default :
            return null;
    }
}

export const isUserConnectedAsManager  = (eaeUser, userConnected) => {
    const methodName        = "isUserConnectedAsManager";
    //console.log(`[${fileName}][${methodName}][eaeUser userConnected]`, '\n', eaeUser, '\n', userConnected);
    return (eaeUser.idUser !== userConnected.idUser && 
        userConnected.profile === EaeConst.USER_ROLE_CODE_MANAGER);
}

export const canAccess  = (component) => {
}
export const canEdit    = (component) => {
}

export const canMangagerReadCollabData = (eaeUser, userConnected) => {
    return isUserConnectedAsManager(eaeUser, userConnected);
}

export const isResponseEditable = (userConnected, userForm, response) => {
    return true;
}

export const isEmpty    = (data, isStrict=false) => {
    if (!isStrict) {
        return ( data === null || data === undefined )
    }
    else {
        return ( 
            data === null || 
            data === undefined ||
            data === ""
        )
    }
}

export const isEmptyObject  = (data) => {
    return ( 
        isEmpty(data) || 
        (Object.keys(data).length === 0 && data.constructor === Object) ||
        (data.constructor === Array && data.length === 0)
    );
}

export const isDebug = (logConfig, className) => {
    if ( logConfig.isDebug ) {
        if ( undefined === logConfig.package.find( (elt) => {
            return elt === className;
        }) ) {
            return false;
        }
        return true;
    }
    return false;
}