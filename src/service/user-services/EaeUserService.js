import * as utils from '../../utils/EvalsmartUtils';
import EaeUserMongoRepository from '../../model/repository/user-repositories/EaeUserMongoRepository';
import EaeUserBean from '../../model/bean/user-beans/EaeUserBean';

const fileName   = "EaeUserService";

/**
 * EaeUserService
 */
export default class EaeUserService {
    isDebug     = false;

    constructor (config={log:{isDebug:false, package:[]}}) {
        const methodName        = "constructor";
        this.isDebug            = utils.isDebug (config.log, fileName);
        this.userRepository     = new EaeUserMongoRepository(config);

        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][config]`, '\n', config);
        }
    }

    getUser          = (idUser) => {
        const methodName            = "getUser";
        return this.userRepository.findById(idUser)
        .then( userRM => {
            if ( this.checkUserData(userRM) ) {
                return this.mapUserData(userRM);   
            }
        })
        .catch ((error) => {
            throw new Error (`[${fileName}][${methodName}][error]\n[${error.message}]`);
        });
    }

    getUserByMail          = (email) => {
        const methodName            = "getUserByMail";
        return this.userRepository.findByEmail(email)
        .then( userRM => {
            if ( this.checkUserData(userRM) ) {
                return this.mapUserData(userRM);   
            }
        })
        .catch ((error) => {
            throw new Error (`[${fileName}][${methodName}][error]\n[${error.message}]`);
        });
    }

    getUserByUserName          = (userName) => {
        const methodName            = "getUserByUserName";
        return this.userRepository.findByUserName(userName)
        .then( userRM => {
            if ( this.checkUserData(userRM) ) {
                return this.mapUserData(userRM);   
            }
        })
        .catch ((error) => {
            throw new Error (`[${fileName}][${methodName}][error]\n[${error.message}]`);
        });
    }

    checkUserData   = (data) => {
        return true;
    }

    mapUserData     = (data) => {
        const methodName            = "mapUserData";
        return new EaeUserBean(
            data.idUser,
            data.profile,
            data.firstName,
            data.lastName,
            data.fullName,
            data.userName,
            data.email,
            data.phone,
            data.status,
            data.accounts
        );
    }

    getFullName (idUser) {

    }

    canRead (eaeUser, eaeComponent) {
        return (eaeComponent.idOwner === eaeUser.idUser || eaeUser.role === "001");
    }

    canEdit (eaeUser, eaeComponent, isFormEditable = true) {
        return (eaeComponent.idOwner === eaeUser.idUser && isFormEditable);
    }
}
