/* REACT */
import React from 'react';
import PropTypes from 'prop-types';

/* UTILS */
import * as utils from '../../utils/EvalsmartUtils';
import * as EaeConst from '../../model/EaeConst';

/* SERVICES */
import EaeUserFormResponseService from '../userform-services/EaeUserFormResponseService';
import EaeUserFormService from '../userform-services/EaeUserFormService';
import EaeEnumService from '../enum-services/EaeEnumService';
import EaeUserService from '../user-services/EaeUserService';

/* BOOTTRAP AND CSS */


/* COMPONENTS */
import EaeFormResponseSingleChoiceComp from '../../component/comp-form/comp-form-response/EaeFormResponseSingleChoiceComp';
import EaeFormResponseFreeTextComp from '../../component/comp-form/comp-form-response/EaeFormResponseFreeTextComp';
import EaeFormResponseSimpleTextComp from '../../component/comp-form/comp-form-response/EaeFormResponseSimpleTextComp';
import EaeFormInfoComp from '../../component/comp-form/comp-form-info/EaeFormInfoComp';
import EaeFooterDomainWithStoreResponsesComp from '../../component/comp-form/comp-form-domain/EaeFooterDomainWithStoreResponsesComp';

/* COMPONENTS UI */
import EaeTableUI from '../../component/comp-form/comp-formUI/comp-table-UI/EaeTableUI';
import EaeRowUI from '../../component/comp-form/comp-formUI/comp-rows-UI/EaeRowUI';
import EaeContainerUI from '../../component/comp-form/comp-formUI/comp-commons-UI/EaeContainerUI';
import EaeButtonUI from '../../component/comp-form/comp-formUI/comp-commons-UI/EaeButtonUI';
import EaeLinkUI from '../../component/comp-form/comp-formUI/comp-commons-UI/EaeLinkUI';

/* MODELS */
import * as formUI from '../../component/comp-form/comp-formUI/comp-formUI-bean/oFormUIs';
import EaeUserFormBean from '../../model/bean/userform-beans/EaeUserFormBean';
import EaeUserFormResponseBean from '../../model/bean/userform-beans/EaeUserFormResponseBean';

const fileName   = "EaeQCMServiceComp";

class EaeQCMServiceComp {
    isDebug     = false;

    constructor (config={log:{isDebug:false, package:[]}}) {
        this.isDebug = utils.isDebug (config.log, fileName);
    }
}