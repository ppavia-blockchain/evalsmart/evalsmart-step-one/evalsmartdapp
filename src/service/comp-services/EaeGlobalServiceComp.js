/* REACT */
import React from 'react';
import PropTypes from 'prop-types';

/* UTILS */
import * as utils from '../../utils/EvalsmartUtils';
import * as EaeConst from '../../model/EaeConst';

/* SERVICES */
import * as ruleService from '../EaeRulesService';

/* BOOTTRAP AND CSS */


/* COMPONENTS */
import EaeFormResponseSingleChoiceComp from '../../component/comp-form/comp-form-response/EaeFormResponseSingleChoiceComp';
import EaeFormResponseFreeTextComp from '../../component/comp-form/comp-form-response/EaeFormResponseFreeTextComp';
import EaeFormResponseSimpleTextComp from '../../component/comp-form/comp-form-response/EaeFormResponseSimpleTextComp';
import EaeFormInfoComp from '../../component/comp-form/comp-form-info/EaeFormInfoComp';
import EaeFooterDomainWithStoreResponsesComp from '../../component/comp-form/comp-form-domain/EaeFooterDomainWithStoreResponsesComp';

/* COMPONENTS UI */
import EaeTableUI from '../../component/comp-form/comp-formUI/comp-table-UI/EaeTableUI';
import EaeRowUI from '../../component/comp-form/comp-formUI/comp-rows-UI/EaeRowUI';
import EaeContainerUI from '../../component/comp-form/comp-formUI/comp-commons-UI/EaeContainerUI';
import EaeButtonUI from '../../component/comp-form/comp-formUI/comp-commons-UI/EaeButtonUI';
import EaeLinkUI from '../../component/comp-form/comp-formUI/comp-commons-UI/EaeLinkUI';

/* MODELS */
import * as formUI from '../../component/comp-form/comp-formUI/comp-formUI-bean/oFormUIs';
import EaeUserFormBean from '../../model/bean/userform-beans/EaeUserFormBean';
import EaeUserFormResponseBean from '../../model/bean/userform-beans/EaeUserFormResponseBean';

const fileName   = "EaeGlobalServiceComp";

class EaeGlobalServiceComp {
    isDebug     = false;

    constructor (config={log:{isDebug:false, package:[]}}) {
        this.isDebug = utils.isDebug (config.log, fileName);
    }

    /**
     * 
     */
    getUserResponse     = (
        formStructResponse,
        userResponses,
        idEaeUser,
        idUserConnected,
        isCreateUserResponse=true,
        ) => {

        const methodName            = "_setCommentsWithCommentResponses";

        // get the user response corresponding to the form struct response
        let userResponse                = this.getUserResponseForFormStructResponse(userResponses, formStructResponse);
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][userResponses, formStructResponse]`, '\n', userResponses, '\n', formStructResponse);
            console.log(`[${fileName}][${methodName}][userResponse]`, '\n', userResponse);
        }
        // build the json UI object and pass it to the corresponding UI component
        if ( utils.isEmptyObject(userResponse) && isCreateUserResponse ) {
            userResponse = new EaeUserFormResponseBean(
                null,
                formStructResponse.idForm,
                idEaeUser,
                formStructResponse.idDomain,
                formStructResponse.idQuery,
                formStructResponse.idResponse,
                idUserConnected,
                "",
                new Date()
            );
        }
        return userResponse;
    }

    /**
     * find a user response corresponding to a form struct response
     */
    getUserResponseForFormStructResponse = (userResponses, formStructResponse) => {
        const methodName        = "_getUserResponseForFormStructResponse";
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][userResponses]`, userResponses);
            console.log(`[${fileName}][${methodName}][formStructResponse]`, formStructResponse);
        }
        return userResponses.find( (userResponse) => {
            return ( userResponse.idResponse === formStructResponse.idResponse );
        });
    }

    /**
     * Get all form struct responses with the type passed 
     */
    getFormStructResponsesByType   = (formStructResponses, type) => {
        return formStructResponses.filter ( (formStructResponse) => {
            return (formStructResponse.responseType === type);
        });
    }
}
export default EaeGlobalServiceComp;