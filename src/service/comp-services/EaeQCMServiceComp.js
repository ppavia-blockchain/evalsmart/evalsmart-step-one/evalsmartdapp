/* REACT */
import React from 'react';
import PropTypes from 'prop-types';

/* UTILS */
import * as utils from '../../utils/EvalsmartUtils';
import * as EaeConst from '../../model/EaeConst';

/* SERVICES */
import * as ruleService from '../../service/EaeRulesService';

/* BOOTTRAP AND CSS */


/* COMPONENTS */
import EaeFormResponseSingleChoiceComp from '../../component/comp-form/comp-form-response/EaeFormResponseSingleChoiceComp';
import EaeFormResponseFreeTextComp from '../../component/comp-form/comp-form-response/EaeFormResponseFreeTextComp';
import EaeFormResponseSimpleTextComp from '../../component/comp-form/comp-form-response/EaeFormResponseSimpleTextComp';
import EaeFormInfoComp from '../../component/comp-form/comp-form-info/EaeFormInfoComp';
import EaeFooterDomainWithStoreResponsesComp from '../../component/comp-form/comp-form-domain/EaeFooterDomainWithStoreResponsesComp';

/* COMPONENTS UI */
import EaeTableUI from '../../component/comp-form/comp-formUI/comp-table-UI/EaeTableUI';
import EaeRowUI from '../../component/comp-form/comp-formUI/comp-rows-UI/EaeRowUI';
import EaeColUI from '../../component/comp-form/comp-formUI/comp-rows-UI/EaeColUI';
import EaeContainerUI from '../../component/comp-form/comp-formUI/comp-commons-UI/EaeContainerUI';
import EaeButtonUI from '../../component/comp-form/comp-formUI/comp-commons-UI/EaeButtonUI';
import EaeLinkUI from '../../component/comp-form/comp-formUI/comp-commons-UI/EaeLinkUI';

/* MODELS */
import * as formUI from '../../component/comp-form/comp-formUI/comp-formUI-bean/oFormUIs';
import EaeUserFormBean from '../../model/bean/userform-beans/EaeUserFormBean';
import EaeUserFormResponseBean from '../../model/bean/userform-beans/EaeUserFormResponseBean';

const fileName   = "EaeQCMServiceComp";

class EaeQCMServiceComp {
    isDebug     = false;

    constructor (config={log:{isDebug:false, package:[]}}, eaeGlobalServiceComp) {
        this.isDebug = utils.isDebug (config.log, fileName);
        this.eaeGlobalServiceComp  = eaeGlobalServiceComp;
    }

    /**
     * Return a component UI Table with a json component UI
     */
    _builTableAndCommentUIDomain = (domain, userResponses) => {
        const methodName        = "_builTableAndCommentUIDomain";
        if ( utils.isEmptyObject(domain) ) {
            throw new Error (`[${fileName}][${methodName}][domain] : domain is empty`);
        }

        const oUserResponses            = this._separateUserResponsesByOwner(userResponses);
        let oUserResponsesEditable    = [];
        let oUserResponsesReadable    = [];
        if ( utils.isUserConnectedAsManager(this.eaeUser, this.userConnected) ) {
            oUserResponsesEditable = oUserResponses.oUserResponseManager;
            oUserResponsesReadable = oUserResponses.oUserResponsesCollab;
        }
        else {
            oUserResponsesEditable = oUserResponses.oUserResponsesCollab;
        }
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][oUserResponsesEditable]`, oUserResponsesEditable);
        }

        const oTableUI          = formUI.oTableUI();
        const commentsUI        = [];

        oTableUI.id         = domain.idDomain;
        oTableUI.linkedData = {...domain, queries:[]};
        // thead
        // th of the query (no label)
        const theaderLibelleQueries = formUI.oTheaderUI();
        theaderLibelleQueries.id            = domain.idDomain;
        theaderLibelleQueries.linkedData    = null; 
        theaderLibelleQueries.config = {...theaderLibelleQueries.config, sizeCol: 2 };

        // column Collaborater response (if userConnected is the manager)
        let theaderResultsCollab = [];
        // th of the collaborator results
        if ( utils.canMangagerReadCollabData(this.eaeUser, this.userConnected) ) {
            theaderResultsCollab = formUI.oTheaderUI()
            theaderResultsCollab.id            = domain.idDomain;
            theaderResultsCollab.linkedData    = null; 
            theaderResultsCollab.config = {
                ...theaderResultsCollab.config, 
                sizeCol: 1,
                content:"Collaborateur"
            };
        }
        // th of the level labels
        const theaderLevels  = this._setTheadersWithLevels(domain.levels);
        // concatenation of the theaders
        oTableUI.config.theader = [].concat(theaderLibelleQueries, theaderResultsCollab, theaderLevels);
        
        // tbody
        let oTbodyUIQueryLabel      = [];
        if ( !utils.isEmptyObject(domain.queries) ) {
            domain.queries.forEach(query => { // scan all queries of the domain
                switch ( query.queryType ) {                        
                    case EaeConst.QUERY_TYPE_INPUT :
                        if ( this.isDebug ) {
                            //console.log(`[${fileName}][${methodName}][query]`, '\n', query.idQuery, query.queryType);
                        }
                        // tbody : query label ...
                        oTbodyUIQueryLabel              = formUI.oTbodyUI();
                        oTbodyUIQueryLabel.id           = query.idQuery;
                        oTbodyUIQueryLabel.linkedData   = {...query, responses:[]};
                        oTbodyUIQueryLabel.config       = {...oTbodyUIQueryLabel.config, content:query.label, sizeCol:2};

                        // tbody : user response ...;
                        let oTbodyUIUserResponse = []
                        if ( utils.canMangagerReadCollabData(this.eaeUser, this.userConnected) ) {
                            if ( this.isDebug ) {
                                console.log(`[${fileName}][${methodName}][oUserResponsesReadable]`, oUserResponsesReadable);
                            }
                            // get user response UI object 
                            oTbodyUIUserResponse          = this._setTbodiesUserResponse(query.responses, EaeConst.QUERY_TYPE_INPUT, oUserResponsesReadable);
                        }

                        // ... concat all tbodies : response components and store into oTableUI.config.tbody array
                        oTableUI.config.tbody.push([].concat(
                            // tbodies query labels
                            oTbodyUIQueryLabel,
                            // tbodies user responses readable
                            oTbodyUIUserResponse,
                            // tbodies user responses editable
                            this._setTbodiesWithResponses(query.responses, EaeConst.QUERY_TYPE_INPUT, oUserResponsesEditable)
                        ));
                        break;
                    case EaeConst.QUERY_TYPE_INPUTTEXT :
                        break;
                    case EaeConst.QUERY_TYPE_COMMENT :
                        // comment(s)
                        const oCommentUI        = formUI.oTextAreaUI();
                        oCommentUI.id           = query.idQuery;
                        oCommentUI.linkedData   = {...query, responses:[]};
                        oCommentUI.config       = {
                            ...oCommentUI.config,
                            content: this._setCommentsWithCommentResponses(query.responses, EaeConst.QUERY_TYPE_COMMENT, oUserResponsesEditable, oUserResponsesReadable)
                        };
                        const commentUI        = 
                        <EaeRowUI
                        componentUI={oCommentUI}
                        />;
                        commentsUI.push(commentUI); // in case of multiple comments
                        break;
                        default :
                }
            });
        }
        const componentUI = 
        <EaeTableUI
        componentUI={oTableUI}
        />
        if ( this.isDebug ) {
            // console.log(`[${fileName}][${methodName}][domain]`, '\n', domain.idDomain, domain.label);
            // console.log(`[${fileName}][${methodName}][oTableUI]`, '\n', oTableUI);
        }
        return {oComponentsUI:[componentUI], oCommentsUI:commentsUI};
    }

    _setTheadersWithLevels = (levels) => {
        const theaderLevels  = ( !utils.isEmptyObject(levels) )
            ? levels.map(level => {
                const oTheadUI = formUI.oTheaderUI();
                oTheadUI.id         = `${level.idDomain}-${level.code}`;
                oTheadUI.linkedData = level;
                oTheadUI.config     = { ...oTheadUI.config, content: level.label }
                return oTheadUI;
            })
            : null;
        return theaderLevels;
    }

    _setTbodiesUserResponse = (responses, type, userResponses) => {
        const methodName        = "_setTbodiesUserResponse";
        let userResponse        = null;

        const correspondingResponse = responses.find (response => {
            // find the corresponding user response
            userResponse        = this._getUserResponseForFormStructResponse(userResponses, response);
            // return the corresponding response
            return response.responseType === type && !utils.isEmptyObject(userResponse);
        });
        const oTbodyResponse          = formUI.oTbodyUI();
        const oTextUI                 = formUI.oTextUI();
        if ( !utils.isEmptyObject(userResponse) ) {
            oTextUI.id = oTbodyResponse.id  = userResponse.idResponse;
            oTextUI.linkedData = oTbodyResponse.linkedData = {...userResponse};
            oTextUI.config   = {
                classes: [],
                component:null,
                label: "",
                defaultValue:"",
                content:correspondingResponse.label
            };
            oTbodyResponse.config   = {
                
                ...oTbodyResponse.config, 
                content: <EaeFormInfoComp
                key={userResponse.id}
                infoUI={oTextUI}
                />};
                if ( this.isDebug ) {
                    console.log(`[${fileName}][${methodName}][correspondingResponse]`, correspondingResponse);
                    console.log(`[${fileName}][${methodName}][userResponse]`, userResponse);
                    console.log(`[${fileName}][${methodName}][oTbodyResponse]`, oTbodyResponse);
                }
            return oTbodyResponse;
        }
        return [];
    }

    /**
     * Scan form struct responses to create formUI configuration for each form struct response.
     * map user form response data if exist
     */
    _setTbodiesWithResponses = (responses, type, userResponses) => {
        const methodName        = "_setTbodiesWithResponses";

        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][responses]`, responses);
            console.log(`[${fileName}][${methodName}][userResponses]`, userResponses);
        }

        const responseCols = responses.map (response => {
            if ( response.responseType === type ) {
                // find the corresponding user response
                let userResponse        = this._getUserResponseForFormStructResponse(userResponses, response);
                const isDisabled        = utils.isResponseEditable (this.userConnected, this.userForm, response);
                let isChecked           = false;
                if ( !utils.isEmptyObject(userResponse) ) {
                    if ( this.isDebug ) {
                        // console.log(`[${fileName}][${methodName}][userResponse origin]`, userResponse);
                    }
                    isChecked = true;
                }
                else { // if no user form for this response, create one to store it in the state on click event
                    userResponse = new EaeUserFormResponseBean (
                        null,
                        response.idForm,
                        this.eaeUser.idUser,
                        response.idDomain,
                        response.idQuery,
                        response.idResponse,
                        this.userConnected.idUser,
                        "",
                        new Date()
                    );
                }

                if ( this.isDebug ) {
                    // console.log(`[${fileName}][${methodName}][userResponse after]`, userResponse);
                }

                const oRadioButtonUI    = formUI.oRadioButtonUI();
                const oTbodyResponse    = formUI.oTbodyUI();
                oRadioButtonUI.id = oTbodyResponse.id  = userResponse.idResponse;
                oRadioButtonUI.linkedData = oTbodyResponse.linkedData = {...userResponse};
                oRadioButtonUI.config   = {
                    classes: [],
                    content:null,
                    id:userResponse.idResponse,
                    name:userResponse.idQuery,
                    value:userResponse.idResponse,
                    isChecked:isChecked,
                    isDisabled:isDisabled,
                    isInLine:false,
                    addResponse:this.addResponse
                };
                oTbodyResponse.config   = {

                    ...oTbodyResponse.config, 
                    content: <EaeFormResponseSingleChoiceComp
                    inputUI={oRadioButtonUI}
                    />};
                return oTbodyResponse;
            }
        });
        return responseCols;
    }

    /**
     * Build a Row UI Component with the Col UI comment componenent wich the user needs to access
     */
    buildCollabAndManagerCommentUserResponses = (query, oUserResponsesCollab, oUserResponsesManager, userForm, userConnected,  eaeUser) => {
        const methodName        = "_buildCollabAndManagerCommentUserResponse";

        const sizeCol   = 6;

        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][query, oUserResponsesCollab, oUserResponsesManager, userForm, userConnected,  eaeUser]`, '\n', 
            query,
            '\n',
            oUserResponsesCollab,
            '\n',
            oUserResponsesManager,
            '\n',
            userForm,
            '\n',
            userConnected,
            '\n',
            eaeUser);
        }

        // get the user response comment of the collaborater
        const collabCommentComp     = this._builCommentUI(
            query,
            oUserResponsesCollab,
            userForm,
            userConnected,
            eaeUser,
            sizeCol
        );

        // get the user response comment of the manager
        const managerCommentComp     = this._builCommentUI(
            query,
            oUserResponsesManager,
            userForm,
            userConnected,
            eaeUser,
            sizeCol
        );

        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][collabCommentComp]`, '\n', collabCommentComp);
            console.log(`[${fileName}][${methodName}][managerCommentComp]`, '\n', managerCommentComp);
        }

        if ( utils.isEmptyObject(collabCommentComp) && utils.isEmptyObject(managerCommentComp) ) {
            return null;
        }
        const oRowUI            = formUI.oRowUI();
        oRowUI.id               = query.idQuery;
        oRowUI.linkedData       = {...query, responses:[]};
        oRowUI.config.content   = [collabCommentComp, managerCommentComp];
        
        return <EaeRowUI
        key={query.id}
        componentUI={oRowUI}
        />;
    }

    /**
     * Build comment components from a form struct comment query  and the corresponding user comment responses
     * and return an array of Col UI with comment in content props.
     * Needs userForm, userConnected and eaeUser to check rights on each user response
     */
    _builCommentUI = (query, userResponses, userForm, userConnected,  eaeUser, sizeCol=12) => {
        const methodName        = "_builCommentsUI";
        
        // check if the type of the query is a really comment type
        if ( query.queryType !== EaeConst.QUERY_TYPE_COMMENT ) {
            console.error(`[${fileName}][${methodName}][query]\n${query}`)
            throw new Error (`[${fileName}][${methodName}][error]\nQuery type is not a comment`);
        }

        // get the corresponding comment form struct response from the form struc query
        // (return an array of comment form struct responses because there could be several)
        const commentFormStructResponses = this.eaeGlobalServiceComp.getFormStructResponsesByType (
            query.responses,
            EaeConst.QUERY_TYPE_COMMENT
        );
        if ( utils.isEmptyObject(commentFormStructResponses) ) {
            throw new Error (`[${fileName}][${methodName}][error]\No comment response found for the query with type 'comment'`);
        }

        // for each response of the query, get the corresponding user response
        // and build the component
        return commentFormStructResponses.map ( (commentFormStructResponse) => {

            // get the user response corresponding to the form struct response
            // if does not exist, create an instance of user comment response
            const userResponse = this.eaeGlobalServiceComp.getUserResponse(
                commentFormStructResponse,
                userResponses,
                eaeUser.idUser,
                userConnected.idUser,
                true
            );
            if ( this.isDebug ) {
                console.log(`[${fileName}][${methodName}][userResponse]`, '\n', userResponse);
            }

            // get the rights on this user response
            const userResponseRights = ruleService.getUserResponseRights (
                userResponse,
                userForm,
                userConnected,
                eaeUser
            );
            if ( this.isDebug ) {
                console.log(`[${fileName}][${methodName}][userResponseRights]`, '\n', userResponseRights);
            }
            if ( !userResponseRights.canRead ) {
                return null;
            }
            const addResponseHandler    = () => {};

            // build the Col UI component for this user response comment
            return this._buildUserResponseCommentComp (
                userResponse,
                userResponse.id,
                query.label,
                userResponseRights.canWrite,
                sizeCol,
                addResponseHandler
            );
        });
    }

    /**
     * Build an instane of user comment responses as a EaeColUI comp:  with a json Col UI object as props and a EaeFormResponseFreeTextComp in props content.
     * EaeFormResponseFreeTextComp has a json Textarea UI object in its props content.
     * 
     */
    _buildUserResponseCommentComp       = (
        userResponse,
        key=userResponse.id,
        label,
        isUserCanEdit=true,
        sizeCol=12,
        addResponseHandler
    ) => {
        // Textarea json UI object
        const oTxtAreaUI        = formUI.oTextAreaUI();
        oTxtAreaUI.id           = userResponse.idResponse;
        oTxtAreaUI.linkedData   = {...userResponse};
        oTxtAreaUI.config       = {
            classes: [],
            label: label,
            placeholder:"",
            name:userResponse.idQuery,
            content:null,
            defaultValue:userResponse.content,
            value:userResponse.content,
            readOnly:isUserCanEdit,
            addResponse:addResponseHandler,
            rows:3
        };

        // Col json UI object
        const oColUI        = formUI.oColUI();
        oColUI.id           = null;
        oColUI.linkedData   = null;
        oColUI.typeUI       = EaeConst.UI_COL;
        oColUI.config.sizeCol       = sizeCol;
        oColUI.config.content       = <EaeFormResponseFreeTextComp
        inputUI={oTxtAreaUI}
        />;
        return <EaeColUI
            key={key}
            componentUI={oColUI}
            />;
    }
}

export default EaeQCMServiceComp;