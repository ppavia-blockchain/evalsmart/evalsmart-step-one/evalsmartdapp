/* REACT */
import React from 'react';

/* UTILS */
import * as utils from '../../utils/EvalsmartUtils';
import * as EaeConst from '../../model/EaeConst';

/* SERVICES */

/* BOOTTRAP AND CSS */

/* COMPONENTS */

/* COMPONENTS UI */
import EaeRowUI from '../../component/comp-form/comp-formUI/comp-rows-UI/EaeRowUI';
import EaeButtonUI from '../../component/comp-form/comp-formUI/comp-commons-UI/EaeButtonUI';
import EaeColUI from '../../component/comp-form/comp-formUI/comp-rows-UI/EaeColUI';
import EaeLinkUI from '../../component/comp-form/comp-formUI/comp-commons-UI/EaeLinkUI';

/* MODELS */
import * as formUI from '../../component/comp-form/comp-formUI/comp-formUI-bean/oFormUIs';

const fileName   = "EaeFooterDomainServiceComp";

class EaeFooterDomainServiceComp {
    isDebug     = false;

    constructor (config={log:{isDebug:false, package:[]}}) {
        this.isDebug = utils.isDebug (config.log, fileName);
    }
    /**
     * 
     */
    buildFooterDomain       = (
        currentOrder,
        orderMax,
        classNameAnte,
        classNamePost,
        onClickAnte,
        onClickPost
    ) => {
        const methodName        = "buildFooterDomain";

        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][currentOrder]`, currentOrder);
        }

        const orderAnte = (parseInt(currentOrder, 10) > 1) ? parseInt(currentOrder, 10) - 1 : 1;
        const orderPost = (parseInt(currentOrder, 10) < parseInt(orderMax, 10)) ? parseInt(currentOrder, 10) + 1 : 1;

        const anteButtonComp    = this._buildFooterButtonComp(
            `/form/domains/${orderAnte}`,
            'ante',
            'navAnteID',
            classNameAnte,
            'Précédent',
            6,
            onClickAnte
        );

        const postButtonComp    = this._buildFooterButtonComp(
            `/form/domains/${orderPost}`,
            'post',
            'navPostID',
            classNamePost,
            'Suivant',
            6,
            onClickPost
        );

        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][anteButtonComp]`, anteButtonComp);
        }

        const oRowUI            = formUI.oRowUI();
        oRowUI.id               = 'footerDomainID';
        oRowUI.linkedData       = {};
        oRowUI.config.content   = [anteButtonComp, postButtonComp];

        return <EaeRowUI
        key={oRowUI.id}
        componentUI={oRowUI}
        />;
    }

    /**
     * Build the navigate footer button component
     */
    _buildFooterButtonComp     = (
        urlTo,
        name,
        id,
        className,
        label,
        sizeCol,
        onClickEvent ) => {
        const methodName        = "_buildFooterButtonComp";

        const oColUI        = formUI.oColUI();
        oColUI.id           = id;
        oColUI.linkedData   = null;
        oColUI.typeUI       = EaeConst.UI_COL;
        oColUI.config.sizeCol       = sizeCol;
        oColUI.config.content       = <EaeLinkUI 
                                    to={`${urlTo}`}
                                    content={<EaeButtonUI 
                                        name={`${name}`}
                                        id={`button-${id}`}
                                        className={`${className}`}
                                        click={onClickEvent}
                                        label={`${label}`}
                                        />}
                                    />;
        return <EaeColUI
        key={oColUI.id}
        componentUI={oColUI}
        />;
    }

}
export default EaeFooterDomainServiceComp;