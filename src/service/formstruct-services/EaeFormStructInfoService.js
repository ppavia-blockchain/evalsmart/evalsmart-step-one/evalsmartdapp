import * as utils from '../../utils/EvalsmartUtils';

import EaeFormStructInfoMongoRepository from '../../model/repository/formstruct-repositories/EaeFormStructInfoMongoRepository';
import EaeFormStructInfoBean from '../../model/bean/formstruct-beans/EaeFormStructInfoBean';

const fileName   = "EaeFormStructInfoService";

export default class EaeFormStructInfoService {
    isDebug     = false;

    constructor (config={log:{isDebug:false, package:[]}}) {
        this.isDebug = utils.isDebug (config.log, fileName);
        this.formStructInfoRepository     = new EaeFormStructInfoMongoRepository(config);
    }

    /**
     * Get all infos of a specific form and map to list of query bean objects
     */
    getInfosByForm          = (idForm) => {
        const methodName            = "getInfosByForm";
        return this.formStructInfoRepository.findAllInfosByForm(idForm)
        .then(infosRM => {
            if ( this.isDebug ) {
                console.log(`[${fileName}][${methodName}][infosRM]`, '\n', infosRM);
            }
            // map to EaeFormStructInfoBean
            if ( this.checkFormData(infosRM) ) {
                return this.mapFormData(infosRM);
            }
        })
        .catch ((error) => {
            throw new Error (`[${fileName}][${methodName}][error]\n[${error.message}]`);
        });
    }

    /**
     * Get all infos of a specific domain and map to list of query bean objects
     */
    getInfosByDomain          = (idDomain) => {
        const methodName            = "getInfosByDomain";
        return this.formStructInfoRepository.findAllInfosByDomain(idDomain)
        .then(infosRM => {
            if ( this.isDebug ) {
                console.log(`[${fileName}][${methodName}][infosRM]`, '\n', infosRM);
            }
            // map to EaeFormStructInfoBean
            if ( this.checkFormData(infosRM) ) {
                return this.mapFormData(infosRM);
            }
        })
        .catch ((error) => {
            throw new Error (`[${fileName}][${methodName}][error]\n[${error.message}]`);
        });
    }

    checkFormData   = (data) => {
        return true;
    }

    /**
     * mapping form struct infos to beans object insert into list of infos
     */
    mapFormData     = async (data) => {
        const methodName            = "mapFormData";
        const formStructInfos = data.infosRM.map ( (infoRM) => {
            const formStructInfoBean = new EaeFormStructInfoBean (
                infoRM.id,
                infoRM.idInfo,
                infoRM.idDomain,
                infoRM.idForm,
                infoRM.label,
                infoRM.content,
                infoRM.order,
                []
            );
            return formStructInfoBean;
        });
        return formStructInfos;
    }
}