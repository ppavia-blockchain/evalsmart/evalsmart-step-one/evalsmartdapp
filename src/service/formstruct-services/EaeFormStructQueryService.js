import * as utils from '../../utils/EvalsmartUtils';

import EaeFormStructQueryMongoRepository from '../../model/repository/formstruct-repositories/EaeFormStructQueryMongoRepository';
import EaeFormStructQueryBean from '../../model/bean/formstruct-beans/EaeFormStructQueryBean';

const fileName   = "EaeFormStructQueryService";

export default class EaeFormStructQueryService {
    isDebug     = false;

    constructor (config={log:{isDebug:false, package:[]}}) {
        this.isDebug = utils.isDebug (config.log, fileName);
        this.formStructQueryRepository     = new EaeFormStructQueryMongoRepository(config);
    }

    /**
     * Get all queries of a specific form and map to list of query bean objects
     */
    getQueriesByForm          = (idForm) => {
        const methodName            = "getQueriesByForm";
        return this.formStructQueryRepository.findAllQueriesByForm(idForm)
        .then(queriesRM => {
            if ( this.isDebug ) {
                console.log(`[${fileName}][${methodName}][queriesRM]`, '\n', queriesRM);
            }
            // map to EaeFormStructQueryBean
            if ( this.checkFormData(queriesRM) ) {
                return this.mapFormData(queriesRM);
            }
        })
        .catch ((error) => {
            throw new Error (`[${fileName}][${methodName}][error]\n[${error.message}]`);
        });
    }

    /**
     * Get all queries of a specific domain and map to list of query bean objects
     */
    getQueriesByDomain          = (idDomain) => {
        const methodName            = "getQueriesByDomain";
        return this.formStructQueryRepository.findAllQueriesByDomain(idDomain)
        .then(queriesRM => {
            if ( this.isDebug ) {
                console.log(`[${fileName}][${methodName}][queriesRM]`, '\n', queriesRM);
            }
            // map to EaeFormStructQueryBean
            if ( this.checkFormData(queriesRM) ) {
                return this.mapFormData(queriesRM);
            }
        })
        .catch ((error) => {
            throw new Error (`[${fileName}][${methodName}][error]\n[${error.message}]`);
        });
    }

    checkFormData   = (data) => {
        return true;
    }

    /**
     * mapping form struct queries to beans object insert into list of queries
     */
    mapFormData     = async (data) => {
        const methodName            = "mapFormData";
        const formStructQueries = data.queriesRM.map ( (queryRM) => {
            const formStructQueryBean = new EaeFormStructQueryBean (
                queryRM.id,
                queryRM.idQuery,
                queryRM.idDomain,
                queryRM.idForm,
                queryRM.label,
                queryRM.queryType,
                queryRM.order,
                []
            );
            return formStructQueryBean;
        });
        return formStructQueries;
    }
}