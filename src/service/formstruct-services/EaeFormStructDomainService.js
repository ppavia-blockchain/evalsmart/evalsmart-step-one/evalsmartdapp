import * as utils from '../../utils/EvalsmartUtils';

import EaeFormStructDomainMongoRepository from '../../model/repository/formstruct-repositories/EaeFormStructDomainMongoRepository';
import EaeFormStructDomainBean from '../../model/bean/formstruct-beans/EaeFormStructDomainBean';

const fileName   = "EaeFormStructDomainService";

export default class EaeFormStructDomainService {
    isDebug     = false;

    constructor (config={log:{isDebug:false, package:[]}}) {
        this.isDebug = utils.isDebug (config.log, fileName);
        this.formStructDomainRepository     = new EaeFormStructDomainMongoRepository(config);
    }

    getDomainsByForm          = (idForm) => {
        const methodName            = "getDomainsByForm";
        return this.formStructDomainRepository.findAllDomainsByForm(idForm)
        .then(doaminsRM => {
            if ( this.isDebug ) {
                console.log(`[${fileName}][${methodName}][doaminsRM]`, '\n', doaminsRM);
            }
            // map to EaeFormStructDomainBean
            if ( this.checkFormData(doaminsRM) ) {
                return this.mapFormData(doaminsRM);
            }
        })
        .catch ((error) => {
            throw new Error (`[${fileName}][${methodName}][error]\n[${error.message}]`);
        });
    }

    checkFormData   = (data) => {
        return true;
    }

    /**
     * mapping form struct domains to beans object insert into list of domains
     */
    mapFormData     = async (data) => {
        const formStructDomains = data.domainsRM.map ( (domainRM) => {
            const formStructDomainBean = new EaeFormStructDomainBean (
                domainRM.id,
                domainRM.idDomain,
                domainRM.label,
                domainRM.idForm,
                domainRM.order,
                domainRM.levels,
                []
            );
            return formStructDomainBean;
        });
        return formStructDomains;
    }
}