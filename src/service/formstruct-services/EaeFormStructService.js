import * as utils from '../../utils/EvalsmartUtils';

import EaeFormStructMongoRepository from '../../model/repository/formstruct-repositories/EaeFormStructMongoRepository';
import EaeFormStructBean from '../../model/bean/formstruct-beans/EaeFormStructBean';
import EaeFormStructDomainService from './EaeFormStructDomainService';
import EaeFormStructQueryService from './EaeFormStructQueryService';
import EaeFormStructInfoService from './EaeFormStructInfoService';
import EaeFormStructResponseService from './EaeFormStructResponseService';

const fileName   = "EaeFormStructService";

export default class EaeFormStructService {
    isDebug     = false;

    constructor (config={log:{isDebug:false, package:[]}}) {
        this.isDebug = utils.isDebug (config.log, fileName);
        this.formStructRepository       = new EaeFormStructMongoRepository(config);
        this.formStructDomainService    = new EaeFormStructDomainService(config);
        this.formStructQueryService     = new EaeFormStructQueryService(config);
        this.formStructInfoService      = new EaeFormStructInfoService(config);
        this.formStructResponseService  = new EaeFormStructResponseService(config);
    }

    getForm          = (idForm) => {
        const methodName            = "getForm";
        return this.formStructRepository.findById(idForm)
        .then(formRM => {
            if ( this.isDebug ) {
                console.log(`[${fileName}][${methodName}][formRM]`, '\n', formRM);
            }
            // map to EaeFormStructBean
            if ( this.checkFormData(formRM) ) {
                return this.mapFormData(formRM);
            }
        })
        .catch ((error) => {
            throw new Error (`[${fileName}][${methodName}][error]\n[${error.message}]`);
        });
    }

    getLastActiveForm          = (since) => {
        const methodName            = "getLastActiveForm";
        return this.formStructRepository.findLastActiveFormStruct(since)
        .then(formRM => {
            if ( this.isDebug ) {
                console.log(`[${fileName}][${methodName}][formRM]`, '\n', formRM);
            }
            // map to EaeFormStructBean
            if ( this.checkFormData(formRM) ) {
                return this.mapFormData(formRM);
            }
        })
        .catch ((error) => {
            throw new Error (`[${fileName}][${methodName}][error]\n[${error.message}]`);
        });
    }

    checkFormData   = (data) => {
        return true;
    }

    /**
     * mapping form struct data to bean object, getting domains, queries and responses
     */
    mapFormData     = async (data) => {
        const methodName            = "mapFormData";
        const eaeFormStructBean = new EaeFormStructBean (
            data.id,
            data.idForm,
            data.label,
            data.dtCreated,
            data.dtClosed,
            data.dtHistorized,
            data.status,
            null
        );

        // get domains
        await this.formStructDomainService.getDomainsByForm(data.idForm)
        .then(domains => {
            eaeFormStructBean.domains = domains;
        })
        .catch ((error) => {
            throw new Error (`[${fileName}][${methodName}][error]\n[${error.message}]`);
        });

        // get queries of the form
        let queries = [];
        await this.formStructQueryService.getQueriesByForm(data.idForm)
        .then(queriesF => {
            queries = queriesF;
        })
        .catch ((error) => {
            throw new Error (`[${fileName}][${methodName}][error]\n[${error.message}]`);
        });

         // get infos of the form
         let infos = [];
         await this.formStructInfoService.getInfosByForm(data.idForm)
         .then(infosF => {
            infos = infosF;
         })
         .catch ((error) => {
             throw new Error (`[${fileName}][${methodName}][error]\n[${error.message}]`);
         });
 

        // get responses of the form
        let responses = [];
        await this.formStructResponseService.getResponsesByForm(data.idForm)
        .then(responsesF => {
            responses = responsesF;
        })
        .catch ((error) => {
            throw new Error (`[${fileName}][${methodName}][error]\n[${error.message}]`);
        });

        // map responses, queries, infos for each domain of the form
        eaeFormStructBean.domains.forEach((domain) => {
            domain.queries = queries.filter(query => {
                if ( query.idDomain === domain.idDomain ) {
                    query.responses = responses.filter(response => {
                        return response.idQuery === query.idQuery;
                    });
                    return query;
                }
                return null;
            });
            domain.infos    = infos.filter(info => {
                if ( info.idDomain === domain.idDomain ) {
                    return info;
                }
                return null;
            });
        });
        return eaeFormStructBean;
    }
}