import * as utils from '../../utils/EvalsmartUtils';

import EaeFormStructResponseMongoRepository from '../../model/repository/formstruct-repositories/EaeFormStructResponseMongoRepository';
import EaeFormStructResponseBean from '../../model/bean/formstruct-beans/EaeFormStructResponseBean';

const fileName   = "EaeFormStructResponseService";

export default class EaeFormStructResponseService {
    isDebug     = false;

    constructor (config={log:{isDebug:false, package:[]}}) {
        this.isDebug = utils.isDebug (config.log, fileName);
        this.formStructResponseRepository     = new EaeFormStructResponseMongoRepository(config);
    }

    /**
     * Get all responses of a specific form and map to list of response bean objects
     */
    getResponsesByForm          = (idForm) => {
        const methodName            = "getResponsesByForm";
        return this.formStructResponseRepository.findAllResponsesByForm(idForm)
        .then(responsesRM => {
            if ( this.isDebug ) {
                console.log(`[${fileName}][${methodName}][responsesRM]`, '\n', responsesRM);
            }
            // map to EaeFormStructResponseBean
            if ( this.checkFormData(responsesRM) ) {
                return this.mapFormData(responsesRM);
            }
        })
        .catch ((error) => {
            throw new Error (`[${fileName}][${methodName}][error]\n[${error.message}]`);
        });
    }

    /**
     * Get all responses of a specific domain and map to list of response bean objects
     */
    getResponsesByDomain          = (idDomain) => {
        const methodName            = "getResponsesByDomain";
        return this.formStructResponseRepository.findAllResponsesByDomain(idDomain)
        .then(responsesRM => {
            if ( this.isDebug ) {
                console.log(`[${fileName}][${methodName}][responsesRM]`, '\n', responsesRM);
            }
            // map to EaeFormStructResponseBean
            if ( this.checkFormData(responsesRM) ) {
                return this.mapFormData(responsesRM);
            }
        })
        .catch ((error) => {
            throw new Error (`[${fileName}][${methodName}][error]\n[${error.message}]`);
        });
    }

    /**
     * Get all responses of a specific query and map to list of response bean objects
     */
    getResponsesByQuery          = (idQuery) => {
        const methodName            = "getResponsesByQuery";
        return this.formStructResponseRepository.findAllResponsesByQuery(idQuery)
        .then(responsesRM => {
            if ( this.isDebug ) {
                console.log(`[${fileName}][${methodName}][responsesRM]`, '\n', responsesRM);
            }
            // map to EaeFormStructResponseBean
            if ( this.checkFormData(responsesRM) ) {
                return this.mapFormData(responsesRM);
            }
        })
        .catch ((error) => {
            throw new Error (`[${fileName}][${methodName}][error]\n[${error.message}]`);
        });
    }

    checkFormData   = (data) => {
        return true;
    }

    /**
     * mapping form struct responses to beans object insert into list of responses
     */
    mapFormData     = async (data) => {
        const methodName            = "mapFormData";
        const formStructResponses = data.responsesRM.map ( (responseRM) => {
            const formStructResponseBean = new EaeFormStructResponseBean (
                responseRM.id,
                responseRM.idResponse,
                responseRM.label,
                responseRM.idQuery,
                responseRM.idDomain,
                responseRM.idForm,
                responseRM.responseType,
                responseRM.content,
                responseRM.level
            );
            return formStructResponseBean;
        });
        return formStructResponses;
    }
}