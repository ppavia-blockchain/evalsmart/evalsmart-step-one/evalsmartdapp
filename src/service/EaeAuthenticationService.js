/* SERVICES */
import EaeUserService from '../service/user-services/EaeUserService';

/* UTILS */ 
import * as EaeConst from '../model/EaeConst';
import * as utils from '../utils/EvalsmartUtils';


const fileName   = "EaeAuthenticationService";

export default class EaeAuthenticationService {
    isDebug     = false;

    constructor (config={log:{isDebug:false, package:[]}}) {
        this.isDebug = utils.isDebug (config.log, fileName);
        this.userService        = new EaeUserService(config);
    }

    setUserAuthentication       = (userName, passPhrase, role) => {
        const methodName            = "setUserAuthentication";
        // get user in data base
        return this.userService.getUserByUserName(userName).then(user => {
            if ( this.checkDataEaeUser(user, passPhrase, role) ) {
                if ( this.isDebug ) {
                    console.log(`[${fileName}][${methodName}][user]`, user);
                }
                return user;
            }
            else {
                return null;
            }
        });
    }

    login = (addUserConnected, addEaeUser, addRole, userAuthenticated, role) => {
        addUserConnected(userAuthenticated);
        if ( role === EaeConst.USER_ROLE_CODE_COLLABORATOR ) {
            addEaeUser(userAuthenticated);
            sessionStorage.setItem("session-eae-user", JSON.stringify(userAuthenticated));
        }
        addRole(role);
        sessionStorage.setItem("session-user-connected", JSON.stringify(userAuthenticated));
        sessionStorage.setItem("session-user-role", JSON.stringify(role));
    }

    logout = (removeUserConnected, removeEaeUser, removeRole) => {
        removeUserConnected();
        removeEaeUser();
        removeRole();
        sessionStorage.removeItem("session-eae-user");
        sessionStorage.removeItem("session-user-connected");
        sessionStorage.removeItem("session-user-role");
    }

    /**
     * refresh if necessary
     */
    refreshPropsFromSession = (addUserConnected, addEaeUser, addUserRole, userConnected, eaeUser, eaeUserRole) => {

        if ( sessionStorage.getItem("session-user-connected") && utils.isEmptyObject(userConnected) ) {
            addUserConnected(JSON.parse(sessionStorage.getItem("session-user-connected")));
        }
        if ( sessionStorage.getItem("session-eae-user") && utils.isEmptyObject(eaeUser) ) {
            addEaeUser(JSON.parse(sessionStorage.getItem("session-eae-user")));
        }
        if ( sessionStorage.getItem("session-user-role") && utils.isEmptyObject(eaeUserRole) ) {
            addUserRole(JSON.parse(sessionStorage.getItem("session-user-role")));
        }
    }

    checkDataEaeUser            = (eaeUser, passPhrase, role) => {
        const methodName            = "setUserAuthentication";
        if ( role === EaeConst.USER_ROLE_CODE_MANAGER &&
            eaeUser.profile !== EaeConst.USER_ROLE_CODE_MANAGER) {
                console.warn(`[${fileName}][${methodName}][USER PROFILE IS NOT MANAGER]`, eaeUser.profile);
                return false;
        }
        return true;
    }

    canUserAccessThisComp   = (user) => {
        // TODO
        return true;
    }

}