import * as utils from '../../utils/EvalsmartUtils';
import EaeEnumMongoRepository from '../../model/repository/enum-repositories/EaeEnumMongoRepository';
import EaeStatusFormRequestMapping from '../../model/requestmapping/enums-representation/EaeStatusFormRequestMapping';
import EaeOneStatusFormRequestMapping from '../../model/requestmapping/enums-representation/EaeOneStatusFormRequestMapping';
import EaeStatusFormBean from '../../model/bean/enum-beans/EaeStatusFormBean';

const fileName   = "EaeEnumService";

export default class EaeEnumService {
    isDebug     = false;

    constructor (config={log:{isDebug:false, package:[]}}) {
        this.isDebug = utils.isDebug (config.log, fileName);
        this.enumRepository     = new EaeEnumMongoRepository(config);
    }

    getAllStatusForm          = () => {
        const methodName            = "getAllStatusForm";
        return this.enumRepository.findAllStatusForm()
        .then(allStatusRM => {
            if ( this.checkStatusFormData(allStatusRM) ) {
                return this.mapStatusFormData(allStatusRM);
            }
        })
        .catch ((error) => {
            throw new Error (`[${fileName}][${methodName}][error]\n[${error.message}]`);
        });
    }

    checkStatusFormData   = (data) => {
        return true;
    }

    mapStatusFormData     = (data) => {
        const eaeStatusFormBean  = [];
        data.statusFormRM.map(statusRM => {
            const response = new EaeStatusFormBean(
                statusRM.code,
                statusRM.label
            );
            eaeStatusFormBean.push(statusRM);
        });

        return eaeStatusFormBean;
    }

    getStatusFormLabelByCode = (allStatusForm, statusCode) => {
        return (allStatusForm === undefined || allStatusForm === null ) ? null : allStatusForm.find((status) => {
            return status.code === statusCode;
        });
    }
}