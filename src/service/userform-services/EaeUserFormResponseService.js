import * as utils from '../../utils/EvalsmartUtils';

import EaeUserFormResponseRepository from '../../model/repository/userform-repositories/EaeUserFormResponseRepository';
import EaeUserFormResponseBean from '../../model/bean/userform-beans/EaeUserFormResponseBean';

const fileName   = "EaeUserFormResponseService";

export default class EaeUserFormResponseService {
    isDebug     = false;

    constructor (config={log:{isDebug:false, package:[]}}) {
        this.isDebug = utils.isDebug (config.log, fileName);
        this.userFormResponseRepository     = new EaeUserFormResponseRepository(config);
    }

    /**
     * Get all responses of a specific form and user and map to list of response bean objects
     */
    getUserResponseByIds          = (idForm, idUser, idResponse, idOwner) => {
        const methodName            = "getUserResponseByIds";
        return this.userFormResponseRepository.findUserResponsesByIds(idForm, idUser, idResponse, idOwner)
        .then(responsesRM => {
            if ( this.isDebug ) {
                console.log(`[${fileName}][${methodName}][responsesRM]`, '\n', responsesRM);
            }
            // map to EaeUserFormResponseBean
            if ( this.checkFormData(responsesRM) ) {
                return this.mapFormData(responsesRM);
            }
        })
        .catch ((error) => {
            throw new Error (`[${fileName}][${methodName}][error]\n[${error.message}]`);
        });
    }

    /**
     * Get all responses of a specific form and user and map to list of response bean objects
     */
    getResponsesByUserForm          = (idForm, idUser) => {
        const methodName            = "getResponsesByUserForm";
        return this.userFormResponseRepository.findAllUserResponsesByForm(idForm, idUser)
        .then(responsesRM => {
            if ( this.isDebug ) {
                console.log(`[${fileName}][${methodName}][responsesRM]`, '\n', responsesRM);
            }
            if ( utils.isEmptyObject(responsesRM) ) {
                return {};
            }
            // map to EaeUserFormResponseBean
            if ( this.checkFormData(responsesRM) ) {
                return this.mapFormData(responsesRM);
            }
            return {};
        })
        .catch ((error) => {
            throw new Error (`[${fileName}][${methodName}][error]\n[${error.message}]`);
        });
    }

    /**
     * Get all responses of a specific form and user and map to list of response bean objects
     */
    getResponsesByFormOwnedByUser          = (idForm, idUser, idOwner) => {
        const methodName            = "getResponsesByFormOwnedByUser";
        return this.userFormResponseRepository.findAllUserResponsesByOwner(idForm, idUser, idOwner)
        .then(responsesRM => {
            if ( this.isDebug ) {
                console.log(`[${fileName}][${methodName}][responsesRM]`, '\n', responsesRM);
            }
            // map to EaeUserFormResponseBean
            if ( this.checkFormData(responsesRM) ) {
                return this.mapFormData(responsesRM);
            }
            return null;
        })
        .catch ((error) => {
            throw new Error (`[${fileName}][${methodName}][error]\n[${error.message}]`);
        });
    }

    addUserFormResponses          = (eaeUserFormResponses) => {
        const methodName          = "addUserFormResponses";

        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][eaeUserFormResponses]`, '\n', eaeUserFormResponses);
        }

        return this.userFormResponseRepository.addNewUserFormResponses(this.convertToJson(eaeUserFormResponses))
        .catch ((error) => {
            throw new Error (`[${fileName}][${methodName}][error]\n[${error.message}]`);
        });
    }

    removeResponses  = (deletedResponses) => {
        const methodName          = "removeResponses";

        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][deletedResponses]`, '\n', deletedResponses);
        }

        return this.userFormResponseRepository.removeResponses(this.convertToJson(deletedResponses))
        .then((ret) => {
            if ( ret !== true ) {
                if ( this.isDebug ) {
                    console.log(`[${fileName}][${methodName}][error]`);
                }
                throw new Error (`[${fileName}][${methodName}][error] return is not true`);
            }
            return ret;
        })
        .catch ((error) => {
            throw new Error (`[${fileName}][${methodName}][error]\n[${error.message}]`);
        });
    }

    checkFormData   = (data) => {
        // TODO - check and validate data
        return true;
    }

    /**
     * mapping form struct responses to beans object insert into list of responses
     */
    mapFormData     = (data) => {
        const methodName            = "mapFormData";
        try {
            const userFormResponses = data.responsesRM.map ( (responseRM) => {
                const userFormResponseBean = new EaeUserFormResponseBean (
                    responseRM.id,
                    responseRM.idForm,
                    responseRM.idUser,
                    responseRM.idDomain,
                    responseRM.idQuery,
                    responseRM.idResponse,
                    responseRM.idOwner,
                    responseRM.content,
                    responseRM.dtCompletion
                );
                return userFormResponseBean;
            });
            return userFormResponses;
        }
        catch (error) {
            throw new Error (`[${fileName}][${methodName}][error]\n[${error.message}]`);
        }
    }

    convertToJson = (responsesBean) => {
        return responsesBean.map(responseBean => {
            const oResponse   = {
                "id": responseBean.id,
                "idForm": responseBean.idForm,
                "idUser": responseBean.idUser,
                "idDomain": responseBean.idDomain,
                "idQuery": responseBean.idQuery,
                "idResponse": responseBean.idResponse,
                "idOwner": responseBean.idOwner,
                "content": responseBean.content,
                "dtCompletion": responseBean.dtCompletion
            };
            return oResponse;
        });
    };
}