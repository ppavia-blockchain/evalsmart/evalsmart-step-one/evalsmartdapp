import * as utils from '../../utils/EvalsmartUtils';

import EaeUserFormRepository from '../../model/repository/userform-repositories/EaeUserFormRepository';
import EaeUserFormBean from '../../model/bean/userform-beans/EaeUserFormBean';
import EaeUserFormResponseService from './EaeUserFormResponseService';


const fileName   = "EaeUserFormService";

export default class EaeUserFormService {
    isDebug     = false;

    constructor (config={log:{isDebug:false, package:[]}}) {
        const methodName            = "constructor";
        this.isDebug = utils.isDebug (config.log, fileName);
        this.userFormRepository         = new EaeUserFormRepository(config);
        this.userFormResponseService    = new EaeUserFormResponseService(config);
    }

    getUserForm          = (idForm, idUser, idOwner) => {
        const methodName            = "getUserForm";
        return this.userFormRepository.findById(idForm, idUser)
        .then(formRM => {
            if ( this.isDebug ) {
                console.log(`[${fileName}][${methodName}][formRM]`, '\n', formRM);
            }
            if ( utils.isEmptyObject(formRM) ) { // no user form found
                return {};
            }
            // map to EaeUserFormBean
            if ( this.checkFormData(formRM) ) {
                return this.mapFormData(formRM, idOwner);
            }
        })
        .catch ((error) => {
            throw new Error (`[${fileName}][${methodName}][error]\n[${error.message}]`);
        });
    }

    _builUserFormList               = async (formsRM) => {
        const methodName            = "_builUserFormList";
        return await Promise.all(formsRM.map((formRM) => {
            const userForm = this.mapFormData(formRM);
            if ( this.isDebug ) {
                console.log(`[${fileName}][${methodName}][formRM]`, '\n', formRM);
                console.log(`[${fileName}][${methodName}][userForm]`, '\n', userForm);
            }
            return userForm;
        }));
    }

    getUserFormsByManager          = (idManager) => {
        const methodName            = "getUserFormsByManager";
        return this.userFormRepository.findByIdManager(idManager)
        .then(userFormsRM => {
            // if ( this.isDebug ) {
            //     console.log(`[${fileName}][${methodName}][userFormsRM]`, '\n', userFormsRM);
            // }
            if ( utils.isEmptyObject(userFormsRM.formsRM) ) { // no user form found
                return {};
            }
            // map to EaeUserFormBean
            if ( this.checkFormData(userFormsRM.formsRM) ) {
                
                return this._builUserFormList(userFormsRM.formsRM)
                .then( (userFormsList) => {
                    if ( this.isDebug ) {
                        console.log(`[${fileName}][${methodName}][userFormsList]`, '\n', userFormsList);
                    }
                    return userFormsList;
                });
            }
        })
        .catch ((error) => {
            throw new Error (`[${fileName}][${methodName}][error]\n[${error.message}]`);
        });
    }

    getActiveUserForm          = (idUser, idOwner) => {
        const methodName            = "getActiveUserForm";
        return this.userFormRepository.findActiveUserFormByUser(idUser)
        .then(formRM => {
            if ( this.isDebug ) {
                console.log(`[${fileName}][${methodName}][formRM]`, '\n', formRM);
            }
            if ( utils.isEmptyObject(formRM) ) { // no user form found
                return {};
            }
            // map to EaeUserFormBean
            if ( this.checkFormData(formRM) ) {
                return this.mapFormData(formRM, idOwner);
            }
        })
        .catch ((error) => {
            throw new Error (`[${fileName}][${methodName}][error]\n[${error.message}]`);
        });
    }

    addUserForm          = (eaeUserForm) => {
        const methodName            = "addUserForm";

        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][eaeUserForm]`, '\n', eaeUserForm);
        }

        return this.userFormRepository.addNewUserForm(eaeUserForm.convertIntoJson())
        .then(formRM => {
            if ( this.isDebug ) {
                console.log(`[${fileName}][${methodName}][formRM]`, '\n', formRM);
            }
            if ( utils.isEmptyObject(formRM) ) { // no user form created
                throw new Error (`[${fileName}][${methodName}][error]\n[no userForm created]`);
            }
            // map to EaeUserFormBean
            if ( this.checkFormData(formRM) ) {
                return this.mapFormData(formRM);
            }
        })
        .catch ((error) => {
            throw new Error (`[${fileName}][${methodName}][error]\n[${error.message}]`);
        });
    }

    checkFormData   = (data) => {
        return true;
    }

    // validate user form by user
    validateUserFormByOwner = (idForm, idUser, dtValidated) => {
    }
    validateUserFormByManager = (idForm, idUser, dtValidated) => {
    }

    // signe user form by user
    signeUserFormByOwner = (idForm, idUser, dtSigned) => {
    }
    signeUserFormByManager = (idForm, idUser, dtSigned) => {
    }

    // close user form

    // historize user form



    /**
     * mapping form struct data to bean object, getting domains, queries and responses
     */
    mapFormData     = async (data, idOwner) => {
        const methodName            = "mapFormData";
        const eaeUserFormBean = new EaeUserFormBean (
            data.id,
            data.idInterview,
            data.idForm,
            data.idUser,
            data.userRole,
            data.dtCreated,
            data.dtClosed,
            data.dtHistorized,
            data.idManager,
            data.dtOwnerValidated,
            data.dtManagerValidated,
            data.dtOwnerSigned,
            data.dtManagerSigned,
            data.status,
            data.completion,
            []
        );

        if ( idOwner ) {
            // attach user responses to the user form
            await this.userFormResponseService.getResponsesByFormOwnedByUser(data.idForm, data.idUser, idOwner)
            .then(responsesF => {
                if ( !utils.isEmptyObject(responsesF) ) {
                    eaeUserFormBean.userResponses = responsesF;
                }
            })
            .catch ((error) => {
                throw new Error (`[${fileName}][${methodName}][error]\n[${error.message}]`);
            });
        }
        else {
            if ( this.isDebug ) {
                console.log(`[${fileName}][${methodName}][no owner defined - eaeUserFormBean]`, '\n', eaeUserFormBean);
            }
            // attach user responses to the user form
            await this.userFormResponseService.getResponsesByUserForm(data.idForm, data.idUser)
            .then(responsesF => {
                if ( !utils.isEmptyObject(responsesF) ) {
                    eaeUserFormBean.userResponses = responsesF;
                }
            })
            .catch ((error) => {
                throw new Error (`[${fileName}][${methodName}][error]\n[${error.message}]`);
            });
        }
        
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][eaeUserFormBean]`, '\n', eaeUserFormBean);
        }

        return eaeUserFormBean;
    }
}