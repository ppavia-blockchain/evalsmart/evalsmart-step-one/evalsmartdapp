/* REACT */
import React from 'react';
import PropTypes from 'prop-types';

/* UTILS */
import * as utils from '../utils/EvalsmartUtils';
import * as EaeConst from '../model/EaeConst';

/* SERVICES */
import EaeUserFormResponseService from './userform-services/EaeUserFormResponseService';
import EaeUserFormService from './userform-services/EaeUserFormService';
import EaeEnumService from './enum-services/EaeEnumService';
import EaeUserService from './user-services/EaeUserService';
import EaeQCMServiceComp from './comp-services/EaeQCMServiceComp';
import EaeGlobalServiceComp from './comp-services/EaeGlobalServiceComp';

/* BOOTTRAP AND CSS */


/* COMPONENTS */
import EaeFormResponseSingleChoiceComp from '../component/comp-form/comp-form-response/EaeFormResponseSingleChoiceComp';
import EaeFormResponseFreeTextComp from '../component/comp-form/comp-form-response/EaeFormResponseFreeTextComp';
import EaeFormResponseSimpleTextComp from '../component/comp-form/comp-form-response/EaeFormResponseSimpleTextComp';
import EaeFormInfoComp from '../component/comp-form/comp-form-info/EaeFormInfoComp';
import EaeFooterDomainWithStoreResponsesComp from '../component/comp-form/comp-form-domain/EaeFooterDomainWithStoreResponsesComp';
import EaeBusinessDomain01 from '../component/comp-form/comp-form-domain/comp-business-domains/EaeBusinessDomain01';
import EaeBusinessDomain02 from '../component/comp-form/comp-form-domain/comp-business-domains/EaeBusinessDomain02';
import EaeBusinessDomain03 from '../component/comp-form/comp-form-domain/comp-business-domains/EaeBusinessDomain03';
import EaeBusinessDomain04 from '../component/comp-form/comp-form-domain/comp-business-domains/EaeBusinessDomain04';
import EaeBusinessDomain05 from '../component/comp-form/comp-form-domain/comp-business-domains/EaeBusinessDomain05';
import EaeBusinessDomain06 from '../component/comp-form/comp-form-domain/comp-business-domains/EaeBusinessDomain06';
import EaeBusinessDomain07 from '../component/comp-form/comp-form-domain/comp-business-domains/EaeBusinessDomain07';
import EaeBusinessDomain08 from '../component/comp-form/comp-form-domain/comp-business-domains/EaeBusinessDomain07';
import EaeBusinessDomain09 from '../component/comp-form/comp-form-domain/comp-business-domains/EaeBusinessDomain08';
import EaeBusinessDomain10 from '../component/comp-form/comp-form-domain/comp-business-domains/EaeBusinessDomain10';
import EaeBusinessDomain11 from '../component/comp-form/comp-form-domain/comp-business-domains/EaeBusinessDomain11';
import EaeBusinessDomain12 from '../component/comp-form/comp-form-domain/comp-business-domains/EaeBusinessDomain12';

/* COMPONENTS UI */
import EaeTableUI from '../component/comp-form/comp-formUI/comp-table-UI/EaeTableUI';
import EaeRowUI from '../component/comp-form/comp-formUI/comp-rows-UI/EaeRowUI';
import EaeContainerUI from '../component/comp-form/comp-formUI/comp-commons-UI/EaeContainerUI';
import EaeButtonUI from '../component/comp-form/comp-formUI/comp-commons-UI/EaeButtonUI';
import EaeLinkUI from '../component/comp-form/comp-formUI/comp-commons-UI/EaeLinkUI';

/* MODELS */
import * as formUI from '../component/comp-form/comp-formUI/comp-formUI-bean/oFormUIs';
import EaeUserFormBean from '../model/bean/userform-beans/EaeUserFormBean';
import EaeUserFormResponseBean from '../model/bean/userform-beans/EaeUserFormResponseBean';

const fileName   = "EaeServiceComp";

class EaeServiceComp {
    isDebug     = false;

    constructor (config={log:{isDebug:false, package:[]}}) {
        this.isDebug = utils.isDebug (config.log, fileName);
        // model services
        this.eaeUserService             = new EaeUserService(config);
        this.eaeUserFormService         = new EaeUserFormService(config);
        this.eaeUserFormResponseService = new EaeUserFormResponseService(config);
        this.eaeEnumService             = new EaeEnumService(config);
        // comp services
        this.eaeGlobalServiceComp      = new EaeGlobalServiceComp(config);
        this.eaeQCMServiceComp          = new EaeQCMServiceComp(config, this.eaeGlobalServiceComp);

        this.deletedResponses           = [];   // must be injected by react component
        this.eaeUser                    = null; // must be injected by react component
        this.userConnected              = null; // must be injected by react component
        this.userForm                   = null; // must be injected by react component
        this.userResponses              = null; // must be injected by react component
        this.addResponse                            = () => {}; // must be injected by react component
        this.addResponses                           = () => {}; // must be injected by react component
        this.initDeletedResponsesStateArray         = () => {}; // must be injected by react component
        this.initUserResponsesStateArray            = () => {}; // must be injected by react component
    }

    refreshUserResponsesRecovery = async (idForm, idUser, idOwner) => {
        const methodName        = "refreshUserResponsesRecovery";
        const responsesBean = await this.eaeUserFormResponseService.getResponsesByFormOwnedByUser(idForm, idUser, idOwner);
        return responsesBean;
    }

    /**
     * Process user responses storage into db and refresh global state user-responses property
     */
    storeResponsesDomain = async (responses, deletedResponses=[]) => {
        // TODO - finalize deleting user responses
        const methodName        = "storeResponsesDomain";
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][check params responses, deletedResponses]`, '\n', responses, deletedResponses);
        }
        // add responses into db
        const userResponses = await this.eaeUserFormResponseService.addUserFormResponses(responses)
        .then((responsesBean) => { // remove user responses, if necessary
            if ( !utils.isEmptyObject(deletedResponses) ) {
                if ( this.isDebug ) {
                    console.log(`[${fileName}][${methodName}][removeResponses]`, '\n', deletedResponses);
                }
                this.eaeUserFormResponseService.removeResponses (deletedResponses);
            }
            return responsesBean;
        })
        .then((responsesBean) => { 
            this.initDeletedResponsesStateArray(); // init deleted responses array
            if ( this.isDebug ) {
                console.log(`[${fileName}][${methodName}][initDeletedResponsesStateArray]`, responsesBean);
            }
            return responsesBean;
        })
        .then( (responsesBean) => {
            this.initUserResponsesStateArray();
            if ( this.isDebug ) {
                console.log(`[${fileName}][${methodName}][initUserResponsesStateArray]`, responsesBean);
            }
            return responsesBean;
        })
        .then( () => {
            const userResponses = this.eaeUserFormResponseService.getResponsesByUserForm(this.userForm.idForm, this.eaeUser.idUser)
            .then((userResponses) => {
                if ( this.isDebug ) {
                    console.log(`[${fileName}][${methodName}][getResponsesByUserForm]`, '\n', userResponses);
                }
                return userResponses;
            }); // retrieve new user responses from db
            return userResponses;
        })        
        .then( (userResponses) => { // then refresh user responses state array
            this.addResponses(userResponses);
            if ( this.isDebug ) {
                console.log(`[${fileName}][${methodName}][addResponses]`, '\n', userResponses);
            }
            return userResponses;
        })
        .catch (error => {
            console.log(`[${fileName}][${methodName}][error]`, '\n', error.message); 
        });
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][check state after store]`, '\n', userResponses, deletedResponses);
        }
    }

    /**
     * find a user response corresponding to a form struct response
     */
    _getUserResponseForFormStructResponse = (userResponses, formStructResponse) => {
        const methodName        = "_getUserResponseForFormStructResponse";
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][userResponses]`, userResponses);
            console.log(`[${fileName}][${methodName}][formStructResponse]`, formStructResponse);
        }
        return userResponses.find( (userResponse) => {
            return ( userResponse.idResponse === formStructResponse.idResponse );
        });
    }

    _separateUserResponsesByOwner = (userResponses) => {
        const methodName        = "separateUserResponsesByOwner";
        let userResponsesCollab     = [];
        let userResponseManager     = [];
        let oUserResponses            = {};
        userResponses.forEach ((userResponse) => {
            if ( userResponse.idOwner === userResponse.idUser ) {
                userResponsesCollab.push(userResponse);
            }
            else {
                userResponseManager.push(userResponse);
            }
        });
        oUserResponses = {
            oUserResponsesCollab:userResponsesCollab,
            oUserResponseManager:userResponseManager
        };
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][oUserResponses]`, oUserResponses);
        }
        return oUserResponses;
    }

    // MANAGE FORM

    createUserForm = (idForm, idUser, idManager) => {
        const methodName        = "createUserForm";
        return this.userFormService.addUserForm(new EaeUserFormBean(
            null,
            idForm,
            idUser,
            "001",
            Date.now(),
            null,
            null,
            idManager,
            null,
            null,
            null,
            null,
            "001",
            0,
            []
        ))
        .then(userFormBean => {
            return userFormBean;
        })
        .catch( (error) => {
            if ( this.isDebug ) {
                console.error(`[${fileName}][${methodName}][error]`, '\n', error.message);
            }
            this.errors     = [...this.errors, error];
            return null;
        });
    }

    // MANAGE DOMAIN

    buildManagerListEaeView         = async (user, statusForm, actionHandler) => {
        const methodName        = "buildManagerListEaeView";
        const oTableUI          = formUI.oTableUI();
        oTableUI.id             = user.idUser;
        oTableUI.linkedData     = {...user};

        // theaders
        oTableUI.config.theader     = this._setManagerTheadersListEaeView();

        // tbodies
        oTableUI.config.tbody = await this._setManagerTbodiesListEaeView(user.idUser, statusForm, actionHandler);

        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][oTableUI.config.tbody]`, oTableUI.config.tbody);
        }

        return [<EaeTableUI
        componentUI={oTableUI}
        />];

    }

    _setManagerTheadersListEaeView      = () => {
        const theaderFirstname     = formUI.oTheaderUI();
        theaderFirstname.id              = 1;
        theaderFirstname.linkedData      = null; 
        theaderFirstname.config          = {...theaderFirstname.config, sizeCol: 1 };
        theaderFirstname.config.content  = "Nom";
        const theaderLastname           = formUI.oTheaderUI();
        theaderLastname.id              = 2;
        theaderLastname.linkedData      = null; 
        theaderLastname.config          = {...theaderLastname.config, sizeCol: 1 };
        theaderLastname.config.content  = "Prénom";
        const theaderDateEae            = formUI.oTheaderUI();
        theaderDateEae.id               = 3;
        theaderDateEae.linkedData       = null; 
        theaderDateEae.config           = {...theaderDateEae.config, sizeCol: 1 };
        theaderDateEae.config.content   = "Date Eae";
        const theaderStatusEae              = formUI.oTheaderUI();
        theaderStatusEae.id                 = 4;
        theaderStatusEae.linkedData         = null; 
        theaderStatusEae.config             = {...theaderStatusEae.config, sizeCol: 1 };
        theaderStatusEae.config.content     = "Status Eae";
        const theaderAction                 = formUI.oTheaderUI();
        theaderAction.id                    = 5;
        theaderAction.linkedData             = null; 
        theaderAction.config                = {...theaderAction.config, sizeCol: 1 };
        theaderAction.config.content        = "";
        return [
            theaderFirstname,
            theaderLastname,
            theaderDateEae,
            theaderStatusEae,
            theaderAction
        ]
    }

    _getUserOfAForm             = async (idUser) => {
        return await this.eaeUserService.getUser(idUser);
    }


    // TODO - interviews must be passed instead of userForms
    _setManagerTbodiesListEaeView    = async (idUser, statusForm, actionHandler) => {
        const methodName        = "_setManagerTbodiesListEaeView";

        const userFormList      = await this.eaeUserFormService.getUserFormsByManager(idUser);
        if ( utils.isEmptyObject(userFormList) ) {
            return [];
        }
        
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][userFormList]`, userFormList);
        }

        return await Promise.all(
            userFormList.map((userForm) => {
                // retrieve user infos from userForm
                return this._getUserOfAForm(userForm.idUser)
                .then ( (eaeUser) => {

                    if ( this.isDebug ) {
                        console.log(`[${fileName}][${methodName}][eaeUser]`, '\n', eaeUser);
                    }
                    const rowTbody  = [];
                    const tFirstName     = formUI.oTbodyUI();
                    tFirstName.id                    = userForm.id;
                    tFirstName.linkedData            = {...userForm, userFormResponses:[]};
                    tFirstName.config.content        = <p>{eaeUser.firstName}</p>;

                    const tLastName     = formUI.oTbodyUI();
                    tLastName.id                    = userForm.id;
                    tLastName.linkedData            = {...userForm, userFormResponses:[]};
                    tLastName.config.content        = <p>{eaeUser.lastName}</p>;

                    const tEaeDate     = formUI.oTbodyUI();
                    tEaeDate.id                    = userForm.id;
                    tEaeDate.linkedData            = {...userForm, userFormResponses:[]};
                    tEaeDate.config.content        = <p>2019-10-13</p>;

                    const tEaeStatus     = formUI.oTbodyUI();
                    tEaeStatus.id                    = userForm.id;
                    tEaeStatus.linkedData            = {...userForm, userFormResponses:[]};
                    tEaeStatus.config.content        = <p>{this.eaeEnumService.getStatusFormLabelByCode(statusForm, userForm.status).label}</p>;

                    const tEaeButton     = formUI.oTbodyUI();
                    tEaeButton.id                    = userForm.id;
                    tEaeButton.linkedData            = {...userForm, userFormResponses:[]};
                    tEaeButton.config.content        = <EaeLinkUI 
                                                        to={`/form`}
                                                        content={ <EaeButtonUI
                                                            name="gotoItem" 
                                                            id={userForm.id}
                                                            value={JSON.stringify(eaeUser)}
                                                            className=""
                                                            click={actionHandler}
                                                            label={`Suivant`}
                                                            />}
                                                        />
                    
                    rowTbody.push(tFirstName);
                    rowTbody.push(tLastName);
                    rowTbody.push(tEaeDate);
                    rowTbody.push(tEaeStatus);
                    rowTbody.push(tEaeButton);

                    if ( this.isDebug ) {
                        console.log(`[${fileName}][${methodName}][rowTbody]`, rowTbody);
                    }

                    return rowTbody;
                });            
            })
        );
    }

    buildTableWithInfosDomain       = (domain) => {
        const methodName        = "buildTableWithInfosDomain";
        if ( utils.isEmptyObject(domain) ) {
            throw new Error (`[${fileName}][${methodName}][domain] : domain is empty`);
        }
        const oTableUI          = formUI.oTableUI();

        oTableUI.id         = domain.idDomain;
        oTableUI.linkedData = {...domain, infos:[]};
        // No thead
        // tbody
        if ( !utils.isEmptyObject(domain.infos) ) {
            domain.infos.forEach(info => { // scan all infos of the domain
                if ( this.isDebug ) {
                    // console.log(`[${fileName}][${methodName}][info]`, '\n', info);
                }
                oTableUI.config.tbody.push(this._setTbodiesWithInfos(info));
            });
        }
        if ( this.isDebug ) {
            // console.log(`[${fileName}][${methodName}][domain]`, '\n', domain);
            // console.log(`[${fileName}][${methodName}][oTableUI]`, '\n', oTableUI);
        }
        const componentUI = 
        <EaeTableUI
        componentUI={oTableUI}
        />
        return [componentUI];
    }

    buildContainerInputsUIDomain    = (domain, userResponses) => {
        const methodName        = "buildContainerInputsUIDomain";
        if ( utils.isEmptyObject(domain) ) {
            throw new Error (`[${fileName}][${methodName}][domain] : domain is empty`);
        }
        const oContainerRowsUI      = [];
        let oRowUI                  = null;

        if ( !utils.isEmptyObject(domain.queries) ) {
            
            domain.queries.forEach( (query, index) => { // scan all queries of the domain
                // if ( this.isDebug ) {
                //     console.log(`[${fileName}][${methodName}][index]`, index);
                // }
                if ( index % 2 === 0 ) {
                    oRowUI  = formUI.oRowUI();
                    oRowUI.config.content  = []; // config cols
                    oRowUI.id               = query.idQuery;
                    oRowUI.linkedData       = {...query};
                    oRowUI.config.content.push({
                        sizeCol:6,
                        content: this._setInputTextResponse(query.responses, EaeConst.QUERY_TYPE_INPUTTEXT, userResponses)
                    });
                    if ( index === domain.queries.length-1 ) {
                        oContainerRowsUI.push(
                            <EaeRowUI
                                key={oRowUI.id}
                                componentUI={oRowUI}
                            />);
                    }
                }
                else {
                    oRowUI.config.content.push({
                        sizeCol:6,
                        content: this._setInputTextResponse(query.responses, EaeConst.QUERY_TYPE_INPUTTEXT, userResponses)
                    });
                    oContainerRowsUI.push(
                        <EaeRowUI
                            key={oRowUI.id}
                            componentUI={oRowUI}
                        />);
                }
                if ( this.isDebug ) {
                    // console.log(`[${fileName}][${methodName}][oRowUI.config.content]`, oRowUI.config.content);
                }
            });
        }
        if ( this.isDebug ) {
            // console.log(`[${fileName}][${methodName}][oContainerRowsUI]`, `${domain.label}\n`, oContainerRowsUI);
        }
        const componentUI   = formUI.oContainerRowsUI();
        componentUI.id          = domain.id;
        componentUI.linkedData  = {...domain, queries:[]};
        componentUI.typeUI      = EaeConst.UI_CONTAINER;
        componentUI.config      = {
            ...componentUI.config,
            content:oContainerRowsUI
        }
        const componentComp = 
        <EaeContainerUI
        key={domain.id}
        componentUI={componentUI}
        />
        return [componentComp];
    }

    _builTableAndCommentUIDomain = (domain, userResponses) => {
        const methodName        = "_builTableAndCommentUIDomain";
        if ( utils.isEmptyObject(domain) ) {
            throw new Error (`[${fileName}][${methodName}][domain] : domain is empty`);
        }

        const oUserResponses            = this._separateUserResponsesByOwner(userResponses);
        let oUserResponsesEditable    = [];
        let oUserResponsesReadable    = [];
        if ( utils.isUserConnectedAsManager(this.eaeUser, this.userConnected) ) {
            oUserResponsesEditable = oUserResponses.oUserResponseManager;
            oUserResponsesReadable = oUserResponses.oUserResponsesCollab;
        }
        else {
            oUserResponsesEditable = oUserResponses.oUserResponsesCollab;
        }
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][oUserResponsesEditable]`, oUserResponsesEditable);
        }

        const oTableUI          = formUI.oTableUI();
        const commentsUI        = [];

        oTableUI.id         = domain.idDomain;
        oTableUI.linkedData = {...domain, queries:[]};
        // thead
        // th of the query (no label)
        const theaderLibelleQueries = formUI.oTheaderUI();
        theaderLibelleQueries.id            = domain.idDomain;
        theaderLibelleQueries.linkedData    = null; 
        theaderLibelleQueries.config = {...theaderLibelleQueries.config, sizeCol: 2 };

        // column Collaborater response (if userConnected is the manager)
        let theaderResultsCollab = [];
        // th of the collaborator results
        if ( utils.canMangagerReadCollabData(this.eaeUser, this.userConnected) ) {
            theaderResultsCollab = formUI.oTheaderUI()
            theaderResultsCollab.id            = domain.idDomain;
            theaderResultsCollab.linkedData    = null; 
            theaderResultsCollab.config = {
                ...theaderResultsCollab.config, 
                sizeCol: 1,
                content:"Collaborateur"
            };
        }
        // th of the level labels
        const theaderLevels  = this._setTheadersWithLevels(domain.levels);
        // concatenation of the theaders
        oTableUI.config.theader = [].concat(theaderLibelleQueries, theaderResultsCollab, theaderLevels);
        
        // tbody
        let oTbodyUIQueryLabel      = [];
        if ( !utils.isEmptyObject(domain.queries) ) {
            domain.queries.forEach(query => { // scan all queries of the domain
                switch ( query.queryType ) {
                    case EaeConst.QUERY_TYPE_INPUT :
                        if ( this.isDebug ) {
                            //console.log(`[${fileName}][${methodName}][query]`, '\n', query.idQuery, query.queryType);
                        }
                        // tbody : query label ...
                        oTbodyUIQueryLabel              = formUI.oTbodyUI();
                        oTbodyUIQueryLabel.id           = query.idQuery;
                        oTbodyUIQueryLabel.linkedData   = {...query, responses:[]};
                        oTbodyUIQueryLabel.config       = {...oTbodyUIQueryLabel.config, content:query.label, sizeCol:2};

                        // tbody : user response ...;
                        let oTbodyUIUserResponse = []
                        if ( utils.canMangagerReadCollabData(this.eaeUser, this.userConnected) ) {
                            if ( this.isDebug ) {
                                console.log(`[${fileName}][${methodName}][oUserResponsesReadable]`, oUserResponsesReadable);
                            }
                            // get user response UI object 
                            oTbodyUIUserResponse          = this._setTbodiesUserResponse(query.responses, EaeConst.QUERY_TYPE_INPUT, oUserResponsesReadable);
                        }

                        // ... concat all tbodies : response components and store into oTableUI.config.tbody array
                        oTableUI.config.tbody.push([].concat(
                            // tbodies query labels
                            oTbodyUIQueryLabel,
                            // tbodies user responses readable
                            oTbodyUIUserResponse,
                            // tbodies user responses editable
                            this._setTbodiesWithResponses(query.responses, EaeConst.QUERY_TYPE_INPUT, oUserResponsesEditable)
                        ));
                        break;
                    case EaeConst.QUERY_TYPE_INPUTTEXT :
                        break;
                    case EaeConst.QUERY_TYPE_COMMENT :
                        // comment(s)
                        const oCommentUI        = formUI.oTextAreaUI();
                        oCommentUI.id           = query.idQuery;
                        oCommentUI.linkedData   = {...query, responses:[]};
                        oCommentUI.config       = {
                            ...oCommentUI.config, 
                            content: this._setCommentsWithCommentResponses(query.responses, EaeConst.QUERY_TYPE_COMMENT, oUserResponsesEditable, oUserResponsesReadable)
                        };
                        const commentUI        = this.eaeQCMServiceComp.buildCollabAndManagerCommentUserResponses(
                            query,
                            oUserResponses.oUserResponsesCollab,
                            oUserResponses.oUserResponseManager,
                            this.userForm,
                            this.userConnected,
                            this.eaeUser
                        );
                        // <EaeRowUI
                        // componentUI={oCommentUI}
                        // />;
                        commentsUI.push(commentUI); // in case of multiple comments
                        break;
                        default :                        
                }
            });
        }
        const componentUI = 
        <EaeTableUI
        componentUI={oTableUI}
        />
        if ( this.isDebug ) {
            // console.log(`[${fileName}][${methodName}][domain]`, '\n', domain.idDomain, domain.label);
            // console.log(`[${fileName}][${methodName}][oTableUI]`, '\n', oTableUI);
        }
        return {oComponentsUI:[componentUI], oCommentsUI:commentsUI};
    }

    _setTheadersWithLevels = (levels) => {
        const theaderLevels  = ( !utils.isEmptyObject(levels) )
            ? levels.map(level => {
                const oTheadUI = formUI.oTheaderUI();
                oTheadUI.id         = `${level.idDomain}-${level.code}`;
                oTheadUI.linkedData = level;
                oTheadUI.config     = { ...oTheadUI.config, content: level.label }
                return oTheadUI;
            })
            : null;
        return theaderLevels;
    }

    _setTbodiesUserResponse = (responses, type, userResponses) => {
        const methodName        = "_setTbodiesUserResponse";
        let userResponse        = null;

        const correspondingResponse = responses.find (response => {
            // find the corresponding user response
            userResponse        = this._getUserResponseForFormStructResponse(userResponses, response);
            // return the corresponding response
            return response.responseType === type && !utils.isEmptyObject(userResponse);
        });
        const oTbodyResponse          = formUI.oTbodyUI();
        const oTextUI                 = formUI.oTextUI();
        if ( !utils.isEmptyObject(userResponse) ) {
            oTextUI.id = oTbodyResponse.id  = userResponse.idResponse;
            oTextUI.linkedData = oTbodyResponse.linkedData = {...userResponse};
            oTextUI.config   = {
                classes: [],
                component:null,
                label: "",
                defaultValue:"",
                content:correspondingResponse.label
            };
            oTbodyResponse.config   = {
                
                ...oTbodyResponse.config, 
                content: <EaeFormInfoComp
                key={userResponse.id}
                infoUI={oTextUI}
                />};
                if ( this.isDebug ) {
                    console.log(`[${fileName}][${methodName}][correspondingResponse]`, correspondingResponse);
                    console.log(`[${fileName}][${methodName}][userResponse]`, userResponse);
                    console.log(`[${fileName}][${methodName}][oTbodyResponse]`, oTbodyResponse);
                }
            return oTbodyResponse;
        }
        return [];
    }

    /**
     * Scan form struct responses to create formUI configuration for each form struct response.
     * map user form response data if exist
     */
    _setTbodiesWithResponses = (responses, type, userResponses) => {
        const methodName        = "_setTbodiesWithResponses";

        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][responses]`, responses);
            console.log(`[${fileName}][${methodName}][userResponses]`, userResponses);
        }

        const responseCols = responses.map (response => {
            if ( response.responseType === type ) {
                // find the corresponding user response
                let userResponse        = this._getUserResponseForFormStructResponse(userResponses, response);
                const isDisabled        = utils.isResponseEditable (this.userConnected, this.userForm, response);
                let isChecked           = false;
                if ( !utils.isEmptyObject(userResponse) ) {
                    if ( this.isDebug ) {
                        // console.log(`[${fileName}][${methodName}][userResponse origin]`, userResponse);
                    }
                    isChecked = true;
                }
                else { // if no user form for this response, create one to store it in the state on click event
                    userResponse = new EaeUserFormResponseBean (
                        null,
                        response.idForm,
                        this.eaeUser.idUser,
                        response.idDomain,
                        response.idQuery,
                        response.idResponse,
                        this.userConnected.idUser,
                        "",
                        new Date()
                    );
                }

                if ( this.isDebug ) {
                    // console.log(`[${fileName}][${methodName}][userResponse after]`, userResponse);
                }

                const oRadioButtonUI    = formUI.oRadioButtonUI();
                const oTbodyResponse    = formUI.oTbodyUI();
                oRadioButtonUI.id = oTbodyResponse.id  = userResponse.idResponse;
                oRadioButtonUI.linkedData = oTbodyResponse.linkedData = {...userResponse};
                oRadioButtonUI.config   = {
                    classes: [],
                    content:null,
                    id:userResponse.idResponse,
                    name:userResponse.idQuery,
                    value:userResponse.idResponse,
                    isChecked:isChecked,
                    isDisabled:isDisabled,
                    isInLine:false,
                    addResponse:this.addResponse
                };
                oTbodyResponse.config   = {

                    ...oTbodyResponse.config, 
                    content: <EaeFormResponseSingleChoiceComp
                    inputUI={oRadioButtonUI}
                    />};
                return oTbodyResponse;
            }
        });
        return responseCols;
    }

    _setTbodiesWithInfos         = (info) => {
        const methodName        = "_setTbodiesWithInfos";
        const tableLines        = [];
        let sizeCol             = 1;
        if( !utils.isEmpty(info.label, true) ) {
            let oTbodyUI            = formUI.oTbodyUI();
            oTbodyUI.id             = info.idInfo;
            oTbodyUI.linkedData     = {...info};
            oTbodyUI.config   = {
                ...oTbodyUI.config
            }
            const oInfoLabelUI      = formUI.oInfoUI();
            oInfoLabelUI.id                 = info.idInfo;
            oInfoLabelUI.linkedData         = {...info};
            oInfoLabelUI.config             = {
                ...oInfoLabelUI.config,
                label:info.label,
                content:info.label
            }
            oTbodyUI.config.content = <EaeFormInfoComp
                key={info.id}
                infoUI={oInfoLabelUI}
                />;
                tableLines.push(oTbodyUI);
        }
        else {
            sizeCol             = 2;
        }
        let oTbodyUI            = formUI.oTbodyUI();
        oTbodyUI.id             = info.idInfo;
        oTbodyUI.linkedData     = {...info};
        oTbodyUI.config.sizeCol = sizeCol;
        oTbodyUI.config   = {
            ...oTbodyUI.config
        }
        const oInfoContentUI    = formUI.oInfoUI();
        oInfoContentUI.id                 = info.idInfo;
        oInfoContentUI.linkedData         = {...info};
        oInfoContentUI.config             = {
            ...oInfoContentUI.config,
            label:info.label,
            content:info.content
        }
        oTbodyUI.config.content = <EaeFormInfoComp
        key={info.id}
        infoUI={oInfoContentUI}
        />;
        tableLines.push(oTbodyUI);
        if ( this.isDebug ) {
            // console.log(`[${fileName}][${methodName}][tableLines]`, tableLines);
        }

        return tableLines;
    }

    _setInputTextResponse        = (responses, type, userResponses) => {
        const methodName        = "_setInputTextResponse";
        const inputs = responses.map (response => { // potentially contains only one response

            if ( response.responseType === type ) {
                // find the corresponding user response
                let userResponse                = this._getUserResponseForFormStructResponse(userResponses, response);
                const oInputTextUI              = formUI.oInputTextUI();
                oInputTextUI.id                 = response.idResponse;
                oInputTextUI.linkedData         = {...response};
                oInputTextUI.config.content     = null;
                oInputTextUI.config.label       = response.label;
                return <EaeFormResponseSimpleTextComp
                    key={response.id}
                    inputUI={oInputTextUI}
                    />;
            }
        });
        // if ( this.isDebug ) {
        //     console.log(`[${fileName}][${methodName}][inputs]`, inputs);
        // }

        return inputs;
    }

    _setCommentsWithCommentResponsesNew = (oUserResponsesEditable, oUserResponsesReadable, userConnected,  eaeUser) => {

        // check if a user response of the user connected is available
        // if not, create it from form struct response to pass it to the onchange handler

        // if user connected is a manager
        // he can read the user response of the collaborater

        // if user conncected is the eae user
        // and status of the form is signed by all parties
        // he can read user response of the manager 
        // if not, he can not

    }

    _getUserResponseCommentUI       = (
        userResponse,
        label,
        isUserCanEdit=true,
        sizeCol=12,
        addResponseHandler
    ) => {
        const oTxtAreaUI        = formUI.oTextAreaUI();
        oTxtAreaUI.id           = userResponse.idResponse;
        oTxtAreaUI.linkedData   = {...userResponse};
        oTxtAreaUI.config       = {
            classes: [],
            label: label,
            placeholder:"",
            name:userResponse.idQuery,
            content:null,
            defaultValue:userResponse.content,
            value:userResponse.content,
            readOnly:isUserCanEdit,
            addResponse:addResponseHandler,
            rows:3
        };
        return {
            sizeCol:sizeCol,
            content:<EaeFormResponseFreeTextComp
            key={userResponse.id}
            inputUI={oTxtAreaUI}
            />
        };
    }

    /**
     * 
     */
    _setCommentsWithCommentResponses     = (queryResponses, type, oUserResponsesEditable, oUserResponsesReadable, isUserCanRead) => {
        const methodName            = "_setCommentsWithCommentResponses";
        let userResponseComments    = [];
        let sizeCol                 = 12;

        queryResponses.forEach (response => {
            if ( response.responseType === type ) {
                let userResponseComment                = this._getUserResponseForFormStructResponse(oUserResponsesReadable, response);
                if ( this.isDebug ) {
                    console.log(`[${fileName}][${methodName}][oUserResponsesReadable, response]`, '\n', oUserResponsesReadable, '\n', response);
                    console.log(`[${fileName}][${methodName}][userResponseComment]`, '\n', userResponseComment);
                } 
                if ( !utils.isEmptyObject(userResponseComment) ) {
                    sizeCol                 = 6;
                    const oTxtAreaUI        = formUI.oTextAreaUI();
                    oTxtAreaUI.id           = response.idResponse;
                    oTxtAreaUI.linkedData   = {...userResponseComment};
                    oTxtAreaUI.config       = {
                        classes: [],
                        label: response.label,
                        placeholder:"",
                        name:response.idQuery,
                        content:null,
                        defaultValue:userResponseComment.content,
                        value:userResponseComment.content,
                        readOnly:true,
                        addResponse:this.addResponse,
                        rows:3
                    };
                    userResponseComments.push({
                        sizeCol:sizeCol,
                        content:<EaeFormResponseFreeTextComp
                        key={response.id}
                        inputUI={oTxtAreaUI}
                        />
                    })
                }


                userResponseComment                = this._getUserResponseForFormStructResponse(oUserResponsesEditable, response);
                if ( this.isDebug ) {
                    console.log(`[${fileName}][${methodName}][oUserResponsesEditable, response]`, '\n', oUserResponsesEditable, '\n', response);
                    console.log(`[${fileName}][${methodName}][userResponseComment]`, '\n', userResponseComment);
                } 
                if ( !utils.isEmptyObject(userResponseComment) ) {
                    const oTxtAreaUI        = formUI.oTextAreaUI();
                    oTxtAreaUI.id           = response.idResponse;
                    oTxtAreaUI.linkedData   = {...userResponseComment};
                    oTxtAreaUI.config       = {
                        classes: [],
                        label: response.label,
                        placeholder:"",
                        name:response.idQuery,
                        content:null,
                        defaultValue:userResponseComment.content,
                        value:userResponseComment.content,
                        readOnly:false,
                        addResponse:this.addResponse,
                        rows:3
                    };
                    userResponseComments.push({
                        sizeCol:sizeCol,
                        content:<EaeFormResponseFreeTextComp
                        key={response.id}
                        inputUI={oTxtAreaUI}
                        />
                    })
                }

                
            }
        });
        return userResponseComments;
    }

    getManagerListItwComp = (user, userFormList) => {
        const methodName        = "getManagerListItwComp";

        const managerListItwUI  = this.buildManagerListEaeView(user, userFormList);

        return managerListItwUI;
    }

    /**
     * build the business domain (ui representation of a domain) following the domain form param 
     */
    getBusinessDomainComp = (domain, orderMax, userResponses, deletedResponses) => {
        const methodName        = "getBusinessDomainComp";
        const numDomain     = (domain.order < 10) ? `0${domain.order}` : domain.order;
        const businessDomainUI = formUI.oBusinessDomainUI();
        businessDomainUI.id        = domain.idDomain;
        businessDomainUI.linkedData    = {...domain, queries:[]};
        businessDomainUI.config        = {...businessDomainUI.config,
            label: domain.label          
        };

        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][init - userResponses]`, userResponses, deletedResponses);
        }
        const footerDomainUI     = formUI.oFooterDomainUI();
        footerDomainUI.id        = domain.idDomain;
        footerDomainUI.linkedData    = {...domain, queries:[]};
        footerDomainUI.config        = {...footerDomainUI.config,
            label: domain.label,
            content: <EaeFooterDomainWithStoreResponsesComp
                    currentOrder={domain.order}
                    orderMax={orderMax}
                    eaeServiceComp={this}
                    />
        };
        if ( this.isDebug ) {
            // console.log(`[${fileName}][${methodName}][footerDomainUI]`, footerDomainUI);
        }
        let domainUI = null;
        switch (domain.order) {
            case 1 :
                // get configuration json object of the domain
                domainUI          = this.buildContainerInputsUIDomain(domain, userResponses);

                businessDomainUI.config.content = {domainUI:domainUI, footerUI:footerDomainUI};
                return <EaeBusinessDomain01
                businessDomainUI={businessDomainUI}
                />;
            case 2 :
                 // get configuration json object of the domain
                 domainUI          = this.buildTableWithInfosDomain(domain);
                if ( this.isDebug ) {
                    // console.log(`[${fileName}][${methodName}][domainUI]`, domainUI);
                }
                 businessDomainUI.config.content = {domainUI:domainUI, footerUI:footerDomainUI};
                 return <EaeBusinessDomain02
                 businessDomainUI={businessDomainUI}
                 />;
            case 3 :
                // get configuration json object of the domain
                domainUI          = this.buildTableWithInfosDomain(domain);
                if ( this.isDebug ) {
                    // console.log(`[${fileName}][${methodName}][domainUI]`, domainUI);
                }
                 businessDomainUI.config.content = {domainUI:domainUI, footerUI:footerDomainUI};
                 return <EaeBusinessDomain03
                 businessDomainUI={businessDomainUI}
                 />;
            break;
            case 4 :
                // get configuration json object of the domain
                domainUI          = this._builTableAndCommentUIDomain(domain, userResponses);
                if ( this.isDebug ) {
                    // console.log(`[${fileName}][${methodName}][userResponses]`, userResponses);
                }
                 businessDomainUI.config.content = {domainUI:domainUI, footerUI:footerDomainUI};
                 return <EaeBusinessDomain04
                 businessDomainUI={businessDomainUI}
                 />;
            break;
            case 5 :
                // get configuration json object of the domain
                domainUI          = this._builTableAndCommentUIDomain(domain, userResponses);
                if ( this.isDebug ) {
                    // console.log(`[${fileName}][${methodName}][userResponses]`, userResponses);
                }
                 businessDomainUI.config.content = {domainUI:domainUI, footerUI:footerDomainUI};
                 return <EaeBusinessDomain05
                 businessDomainUI={businessDomainUI}
                 />;
            break;
            case 6 :
                // get configuration json object of the domain
                domainUI          = this._builTableAndCommentUIDomain(domain, userResponses);
                if ( this.isDebug ) {
                    // console.log(`[${fileName}][${methodName}][userResponses]`, userResponses);
                }
                 businessDomainUI.config.content = {domainUI:domainUI, footerUI:footerDomainUI};
                 return <EaeBusinessDomain06
                 businessDomainUI={businessDomainUI}
                 />;
            case 7 :
                // get configuration json object of the domain
                domainUI          = this._builTableAndCommentUIDomain(domain, userResponses);
                if ( this.isDebug ) {
                    // console.log(`[${fileName}][${methodName}][userResponses]`, userResponses);
                }
                 businessDomainUI.config.content = {domainUI:domainUI, footerUI:footerDomainUI};
                 return <EaeBusinessDomain07
                 businessDomainUI={businessDomainUI}
                 />;
            case 8 :
                // get configuration json object of the domain
                domainUI          = this._builTableAndCommentUIDomain(domain, userResponses);
                if ( this.isDebug ) {
                    // console.log(`[${fileName}][${methodName}][domainUI]`, domainUI);
                }
                 businessDomainUI.config.content = {domainUI:domainUI, footerUI:footerDomainUI};
                 return <EaeBusinessDomain08
                 businessDomainUI={businessDomainUI}
                 />;
            case 9 :
                // get configuration json object of the domain
                domainUI          = this._builTableAndCommentUIDomain(domain, userResponses);
                if ( this.isDebug ) {
                    // console.log(`[${fileName}][${methodName}][domainUI]`, domainUI);
                }
                 businessDomainUI.config.content = {domainUI:domainUI, footerUI:footerDomainUI};
                 return <EaeBusinessDomain09
                 businessDomainUI={businessDomainUI}
                 />;
            case 10 :
                // get configuration json object of the domain
                domainUI          = this._builTableAndCommentUIDomain(domain, userResponses);
                if ( this.isDebug ) {
                    // console.log(`[${fileName}][${methodName}][domainUI]`, domainUI);
                }
                 businessDomainUI.config.content = {domainUI:domainUI, footerUI:footerDomainUI};
                 return <EaeBusinessDomain10
                 businessDomainUI={businessDomainUI}
                 />;
            case 11 :
                // get configuration json object of the domain
                domainUI          = this._builTableAndCommentUIDomain(domain, userResponses);
                if ( this.isDebug ) {
                    // console.log(`[${fileName}][${methodName}][domainUI]`, domainUI);
                }
                 businessDomainUI.config.content = {domainUI:domainUI, footerUI:footerDomainUI};
                 return <EaeBusinessDomain11
                 businessDomainUI={businessDomainUI}
                 />;
            case 12 :
                // get configuration json object of the domain
                domainUI          = this._builTableAndCommentUIDomain(domain, userResponses);
                if ( this.isDebug ) {
                    // console.log(`[${fileName}][${methodName}][domainUI]`, domainUI);
                }
                 businessDomainUI.config.content = {domainUI:domainUI, footerUI:footerDomainUI};
                 return <EaeBusinessDomain12
                 businessDomainUI={businessDomainUI}
                 />;
            case 13 :
                // get configuration json object of the domain
                domainUI          = this._builTableAndCommentUIDomain(domain, userResponses);
                if ( this.isDebug ) {
                    // console.log(`[${fileName}][${methodName}][domainUI]`, domainUI);
                }
                businessDomainUI.config.content = {domainUI:domainUI, footerUI:footerDomainUI};
                return <EaeBusinessDomain12
                businessDomainUI={businessDomainUI}
                />;
            case 14 :
                // get configuration json object of the domain
                domainUI          = this._builTableAndCommentUIDomain(domain, userResponses);
                if ( this.isDebug ) {
                    // console.log(`[${fileName}][${methodName}][domainUI]`, domainUI);
                }
                businessDomainUI.config.content = {domainUI:domainUI, footerUI:footerDomainUI};
                return <EaeBusinessDomain12
                businessDomainUI={businessDomainUI}
                />;
            default :
        }
    }
}
EaeServiceComp.propTypes = {
    idUser: PropTypes.string
};

export default EaeServiceComp;