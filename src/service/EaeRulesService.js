
/* BEANS */
import EaeUserBean from '../model/bean/user-beans/EaeUserBean';
import EaeUserFormBean from '../model/bean/userform-beans/EaeUserFormBean';

/* UTILS */ 
import * as EaeConst from '../model/EaeConst';
import * as utils from '../utils/EvalsmartUtils';

export const isFormReadableByUser   = (eaeUser=new EaeUserBean(), eaeUserForm=new EaeUserFormBean()) => {
    // the user is the owner of the form
    if ( eaeUser.idUser === eaeUserForm.idUser ) {
        return true;
    }

    // the user is the manager of the form
    if ( eaeUser.profile === EaeConst.USER_ROLE_CODE_MANAGER && eaeUser.idUser === eaeUserForm.idManager ) {
        return true;
    }
}

export const isFormEditableByUser = (eaeUser=new EaeUserBean(), eaeUserForm=new EaeUserFormBean()) => {
    // user is the owner or manager and the status of the form is [001:created]
    
    // the user is the owner and he has not validated it yet
    if ( eaeUser.idUser === eaeUserForm.idUser &&
         eaeUserForm.status === EaeConst.USER_FORM_STATUS_CREATED &&
         utils.isEmptyObject(eaeUserForm.dtOwnerValidated) ) {
        return true;
    }

    // the user is the manager and he has not validated it yet
    if ( eaeUser.idUser === eaeUserForm.idManager &&
        eaeUserForm.status === EaeConst.USER_FORM_STATUS_CREATED &&
        utils.isEmptyObject(eaeUserForm.dtManagerValidated) ) {
       return true;
    }
    return false;
}

const _getUserRightsOnFormStatusCreated = (
    userForm,
    userConnected,
    eaeUser
) => {
    const userRights    =  {
        canRead: false,
        canWrite: false
    };
    // user connected is the collab and the owner of the user form
    if ( userConnected.idUser === eaeUser.idUser && 
        userConnected.idUser === userForm.idUser && 
        userConnected.profile === EaeConst.USER_ROLE_CODE_COLLABORATOR
        ) {
            userRights.canRead      = true;
            userRights.canWrite     = true;
    }
    // user connected is the collab and is not the owner of the user form (error case)
    if ( userConnected.idUser !== eaeUser.idUser &&
        userConnected.idUser !== userForm.idUser && 
        userConnected.profile === EaeConst.USER_ROLE_CODE_COLLABORATOR
        ) {
            userRights.canRead      = false;
            userRights.canWrite     = false;
    }

    // user connected is a manager and the manager of the user form
    if ( userConnected.idUser !== eaeUser.idUser &&
        userConnected.idUser === userForm.idManager &&
        userConnected.profile === EaeConst.USER_ROLE_CODE_MANAGER
        ) {
            userRights.canRead      = true;
            userRights.canWrite     = true;
    }

    // user connected is the manager and he is not the manager of the user response (error case)
    if ( userConnected.idUser !== eaeUser.idUser &&
        userConnected.idUser !== userForm.idManager &&
        userConnected.profile === EaeConst.USER_ROLE_CODE_MANAGER
        ) {
            userRights.canRead      = false;
            userRights.canWrite     = false;
    }

    return userRights;
}

export const getUserResponseRights = (
    userResponse,
    userForm,
    userConnected,
    eaeUser
    ) => {
    const userResponseRights    =  {
        canRead: false,
        canWrite: false
    };

    if ( userForm.status === EaeConst.USER_FORM_STATUS_CREATED ) {
        return _getUserResponseRightsOnFormStatusCreated (
            userResponse,
            userConnected,
            eaeUser
        );
    }

    if ( userForm.status === EaeConst.USER_FORM_STATUS_VALIDATED &&
        !utils.isEmptyObject(userForm.dtOwnerValidated) && 
        utils.isEmptyObject(userForm.dtManagerValidated)
        ) {
        return _getUserResponseRightsOnFormStatusValidatedByCollab (
            userResponse,
            userConnected,
            eaeUser
        );
    }

    if ( userForm.status === EaeConst.USER_FORM_STATUS_VALIDATED &&
        !utils.isEmptyObject(userForm.dtOwnerValidated) && 
        !utils.isEmptyObject(userForm.dtManagerValidated)
        ) {
        return _getUserResponseRightsOnFormStatusValidatedByManager (
            userResponse,
            userConnected,
            eaeUser
        );
    }

    if ( userForm.status === EaeConst.USER_FORM_STATUS_SIGNED &&
        !utils.isEmptyObject(userForm.dtOwnedSigned) && 
        utils.isEmptyObject(userForm.dtManagerSigned)
        ) {
        return _getUserResponseRightsOnFormStatusSignedByCollab (
            userResponse,
            userConnected,
            eaeUser
        );
    }

    if ( userForm.status === EaeConst.USER_FORM_STATUS_SIGNED &&
        !utils.isEmptyObject(userForm.dtOwnedSigned) && 
        !utils.isEmptyObject(userForm.dtManagerSigned)
        ) {
        return _getUserResponseRightsOnFormStatusSignedByManager (
            userResponse,
            userConnected,
            eaeUser
        );
    }

    return userResponseRights;
}

const _getUserResponseRightsOnFormStatusCreated = (
    userResponse,
    userConnected,
    eaeUser
) => {
    const userResponseRights    =  {
        canRead: false,
        canWrite: false
    };
    // user connected is the collab and the owner of the user response
    if ( userConnected.idUser === eaeUser.idUser && 
        userConnected.profile === EaeConst.USER_ROLE_CODE_COLLABORATOR &&
        userResponse.idOwner === userConnected.idUser ) {
        userResponseRights.canRead      = true;
        userResponseRights.canWrite     = true;
    }
    // user connected is the collab and he is not the owner of the user response
    if ( userConnected.idUser === eaeUser.idUser && 
        userConnected.profile === EaeConst.USER_ROLE_CODE_COLLABORATOR &&
        userResponse.idOwner !== userConnected.idUser ) {
        userResponseRights.canRead      = false;
        userResponseRights.canWrite     = false;
    }

    // user connected is the manager and the owner of the user response
    if ( userConnected.idUser !== eaeUser.idUser &&
        userConnected.profile === EaeConst.USER_ROLE_CODE_MANAGER &&
        userResponse.idOwner === userConnected.idUser ) {
        userResponseRights.canRead      = true;
        userResponseRights.canWrite     = true;
    }

    // user connected is the manager and he is not the owner of the user response
    if ( userConnected.idUser !== eaeUser.idUser &&
        userConnected.profile === EaeConst.USER_ROLE_CODE_MANAGER &&
        userResponse.idOwner !== userConnected.idUser ) {
        userResponseRights.canRead      = true;
        userResponseRights.canWrite     = false;
    }

    return userResponseRights;
}

const _getUserResponseRightsOnFormStatusValidatedByCollab = (
    userResponse,
    userConnected,
    eaeUser
) => {
    const userResponseRights    =  {
        canRead: false,
        canWrite: false
    };
    // user connected is the collab and the owner of the user response
    if ( userConnected.idUser === eaeUser.idUser && 
        userConnected.profile === EaeConst.USER_ROLE_CODE_COLLABORATOR &&
        userResponse.idOwner === userConnected.idUser ) {
        userResponseRights.canRead      = true;
        userResponseRights.canWrite     = false;
    }
    // user connected is the collab and he is not the owner of the user response
    if ( userConnected.idUser === eaeUser.idUser && 
        userConnected.profile === EaeConst.USER_ROLE_CODE_COLLABORATOR &&
        userResponse.idOwner !== userConnected.idUser ) {
        userResponseRights.canRead      = false;
        userResponseRights.canWrite     = false;
    }

    // user connected is the manager and the owner of the user response
    if ( userConnected.idUser !== eaeUser.idUser &&
        userConnected.profile === EaeConst.USER_ROLE_CODE_MANAGER &&
        userResponse.idOwner === userConnected.idUser ) {
        userResponseRights.canRead      = true;
        userResponseRights.canWrite     = true;
    }

    // user connected is the manager and he is not the owner of the user response
    if ( userConnected.idUser !== eaeUser.idUser &&
        userConnected.profile === EaeConst.USER_ROLE_CODE_MANAGER &&
        userResponse.idOwner !== userConnected.idUser ) {
        userResponseRights.canRead      = true;
        userResponseRights.canWrite     = false;
    }

    return userResponseRights;
}

const _getUserResponseRightsOnFormStatusValidatedByManager = (
    userResponse,
    userConnected,
    eaeUser
) => {
    const userResponseRights    =  {
        canRead: false,
        canWrite: false
    };
    // user connected is the collab and the owner of the user response
    if ( userConnected.idUser === eaeUser.idUser && 
        userConnected.profile === EaeConst.USER_ROLE_CODE_COLLABORATOR &&
        userResponse.idOwner === userConnected.idUser ) {
        userResponseRights.canRead      = true;
        userResponseRights.canWrite     = false;
    }
    // user connected is the collab and he is not the owner of the user response
    if ( userConnected.idUser === eaeUser.idUser && 
        userConnected.profile === EaeConst.USER_ROLE_CODE_COLLABORATOR &&
        userResponse.idOwner !== userConnected.idUser ) {
        userResponseRights.canRead      = false;
        userResponseRights.canWrite     = false;
    }

    // user connected is the manager and the owner of the user response
    if ( userConnected.idUser !== eaeUser.idUser &&
        userConnected.profile === EaeConst.USER_ROLE_CODE_MANAGER &&
        userResponse.idOwner === userConnected.idUser ) {
        userResponseRights.canRead      = true;
        userResponseRights.canWrite     = false;
    }

    // user connected is the manager and he is not the owner of the user response
    if ( userConnected.idUser !== eaeUser.idUser &&
        userConnected.profile === EaeConst.USER_ROLE_CODE_MANAGER &&
        userResponse.idOwner !== userConnected.idUser ) {
        userResponseRights.canRead      = true;
        userResponseRights.canWrite     = false;
    }

    return userResponseRights;
}

const _getUserResponseRightsOnFormStatusSignedByCollab = (
    userResponse,
    userConnected,
    eaeUser
) => {
    const userResponseRights    =  {
        canRead: false,
        canWrite: false
    };
    // user connected is the collab and the owner of the user response
    if ( userConnected.idUser === eaeUser.idUser && 
        userConnected.profile === EaeConst.USER_ROLE_CODE_COLLABORATOR &&
        userResponse.idOwner === userConnected.idUser ) {
        userResponseRights.canRead      = true;
        userResponseRights.canWrite     = false;
    }
    // user connected is the collab and he is not the owner of the user response
    if ( userConnected.idUser === eaeUser.idUser && 
        userConnected.profile === EaeConst.USER_ROLE_CODE_COLLABORATOR &&
        userResponse.idOwner !== userConnected.idUser ) {
        userResponseRights.canRead      = true;
        userResponseRights.canWrite     = false;
    }

    // user connected is the manager and the owner of the user response
    if ( userConnected.idUser !== eaeUser.idUser &&
        userConnected.profile === EaeConst.USER_ROLE_CODE_MANAGER &&
        userResponse.idOwner === userConnected.idUser ) {
        userResponseRights.canRead      = true;
        userResponseRights.canWrite     = false;
    }

    // user connected is the manager and he is not the owner of the user response
    if ( userConnected.idUser !== eaeUser.idUser &&
        userConnected.profile === EaeConst.USER_ROLE_CODE_MANAGER &&
        userResponse.idOwner !== userConnected.idUser ) {
        userResponseRights.canRead      = true;
        userResponseRights.canWrite     = false;
    }

    return userResponseRights;
}

const _getUserResponseRightsOnFormStatusSignedByManager = (
    userResponse,
    userConnected,
    eaeUser
) => {
    const userResponseRights    =  {
        canRead: false,
        canWrite: false
    };
    // user connected is the collab and the owner of the user response
    if ( userConnected.idUser === eaeUser.idUser && 
        userConnected.profile === EaeConst.USER_ROLE_CODE_COLLABORATOR &&
        userResponse.idOwner === userConnected.idUser ) {
        userResponseRights.canRead      = true;
        userResponseRights.canWrite     = false;
    }
    // user connected is the collab and he is not the owner of the user response
    if ( userConnected.idUser === eaeUser.idUser && 
        userConnected.profile === EaeConst.USER_ROLE_CODE_COLLABORATOR &&
        userResponse.idOwner !== userConnected.idUser ) {
        userResponseRights.canRead      = true;
        userResponseRights.canWrite     = false;
    }

    // user connected is the manager and the owner of the user response
    if ( userConnected.idUser !== eaeUser.idUser &&
        userConnected.profile === EaeConst.USER_ROLE_CODE_MANAGER &&
        userResponse.idOwner === userConnected.idUser ) {
        userResponseRights.canRead      = true;
        userResponseRights.canWrite     = false;
    }

    // user connected is the manager and he is not the owner of the user response
    if ( userConnected.idUser !== eaeUser.idUser &&
        userConnected.profile === EaeConst.USER_ROLE_CODE_MANAGER &&
        userResponse.idOwner !== userConnected.idUser ) {
        userResponseRights.canRead      = true;
        userResponseRights.canWrite     = false;
    }

    return userResponseRights;
}