export const config     = {
    "prod" :
        {
            "env": "prod",
            "eaeServicesPath": "https://192.168.43.148:8090/evalsmart",
            "log": {
                "isDebug":false,
                "package":[]
            }
        },
    "dev" :
        {
            "env": "dev",
            "eaeServicesPath": "https://192.168.126.1:8090/evalsmart",
            "log": {
                "isDebug":true,
                "package":[
                    // "EaeRouter",

                    /* REDUX */
                    // "userReducer",
                    // "formStructReducer",
                    // "userFormReducer",
                    // "responseReducer",

                    /* REPOSITORIES */
                    // "EaeFormStructInfoMongoRepository",
                    // "EaeUserFormRepository",
                    // "EaeUserFormResponseRepository",
                    // "EaeEnumMongoRepository",

                    /* SERVICES */
                    // "EaeAuthenticationService",
                    "EaeServiceComp",
                    // "EaeUserFormService",
                    // "EaeUserFormResponseService",
                    "EaeFooterDomainServiceComp",
                    // "EaeQCMServiceComp",

                    /* COMPONENTS MANAGEMENT */
                    // "EaeManagerDashboardComp",

                    /* COMPONENTS CONTAINERS */
                    // "App",
                    // "EaeFormLoginComp",
                    // "EaeFormWrapperComp",

                    /* COMPONENTS FORM STRUCT*/
                    // "EaeFormComp",
                    // "EaeFormDomainComp",
                    // "EaeFooterDomainComp",
                    
                    /* COMPONENTS FONCTIONNAL UI*/
                    // "EaeFormResponseSingleChoiceComp",
                    // "EaeFormResponseFreeTextComp",
                    // "EaeFooterDomainWithStoreResponsesComp",
                    // "EaeFormInfoComp",

                    /* COMPONENTS BUSINESS DOMAINS */
                    // "EaeBusinessDomain01",
                    // "EaeBusinessDomain02",
                    // "EaeBusinessDomain03",
                    // "EaeBusinessDomain04",
                    // "EaeBusinessDomain05",
                    // "EaeBusinessDomain06",
                    // "EaeBusinessDomain07",

                    /* COMPONENTS UI*/
                    // "EaeNavUI",
                    // "EaeBlockUI",
                    // "EaeHeaderBlockUI",
                    // "EaeBodyBlockUI",
                    // "EaeFooterBlockUI",
                    // "EaeTableUI",
                    // "EaeHeadersTableUI",
                    // "EaeHeaderTableUI",
                    // "EaeRowTableUI",
                    // "EaeBodiesTableUI",
                    // "EaeBodyTableUI",
                    // "EaeCellTableUI",
                    "EaeRowUI",
                    "EaeColUI",
                    // "EaeRadioButtonUI",
                    // "EaeTextAreaUI",
                    // "EaeInputTextUI",
                ]
            }
        }
};