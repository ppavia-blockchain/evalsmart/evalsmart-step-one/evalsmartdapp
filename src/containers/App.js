/* REACT */
import React from 'react';

/* REDUX */
import { connect } from 'react-redux';
import * as actionConfig from '../redux/actions/action-config';
import * as actionEnum from '../redux/actions/action-enum';

/* UTILS */
import * as utils from '../utils/EvalsmartUtils';
import * as config from '../config/eaeConfig';
import * as EaeConst from '../model/EaeConst';

/* BOOTTRAP AND CSS */
import './App.css';

/* REACT ROUTER */
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

/* COMPONENTS */
import EaeFormLoginComp from '../component/comp-authent/EaeFormLoginComp';
import EaeUserComp from '../component/comp-user/EaeUserComp';
import EaeFormWrapperComp from '../component/comp-form/EaeFormWrapperComp';
import EaeNavUI from '../component/comp-form/comp-formUI/comp-nav-UI/EaeNavUI';
import EaeManagerDashboardComp from '../component/comp-manager/EaeManagerDashboardComp';

/* SERVICES */
import EaeEnumService from '../service/enum-services/EaeEnumService';

const fileName   = "App";

class App extends React.Component {
    constructor (props) {
        super(props);
        const methodName            = "constructor";
        this.props.setConfig("dev");
        this.state = {
            init: true
        }
    }

    getAllStatusForm = () => {
        this.enumService.getAllStatusForm()
        .then( (allStatus) => {
            this.props.addStatusForm(allStatus);
        })
        .catch( (error) => {
            throw new Error(error.message);
        });
    }

    componentDidMount () {
        const methodName        = "componentDidMount";
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][DID MOUNT]`, null);
        }
        
        this.setState({
            init:false
        });
    }

    shouldComponentUpdate(nextProps, nextState) {
        const methodName        = "shouldComponentUpdate";
        return true;
    }
    
    render () {
        const methodName            = "render";
        if ( utils.isEmptyObject(this.props.config) ) {
            if ( this.isDebug ) {
                console.log(`[${fileName}][${methodName}][config]`, '\n', this.props.config);
            }
            return null;
        }
        this.isDebug                = utils.isDebug (this.props.config.log, fileName);
        if ( utils.isEmptyObject(this.props.statusForm) ) {
            this.enumService            = new EaeEnumService(this.props.config);
            this.getAllStatusForm();
        }
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][this.prop]`, this.props);
        }

        const loginFormComp         = <EaeFormLoginComp config={this.props.config} />;

        const topBarComp            = <EaeNavUI />;

        const managerDashboardCom   = ( true ) 
        ? <Route path="/manager/list-userforms" 
        render={(props) => <EaeManagerDashboardComp {...props} // this is the only way to pass parameters to child component of the route
        config={this.props.config}                        
        />}
        />
        : null;

        const formWrapperComp       = ( !utils.isEmptyObject(this.props.eaeUser) ) 
        ? <Route path="/form" 
        render={(props) => <EaeFormWrapperComp {...props} // this is the only way to pass parameters to child component of the route
        idUser={this.props.eaeUser.idUser}
        idOwner={this.props.userConnected.idUser}
        idManager={
            ( this.props.eaeUser.idUser !== this.props.userConnected.idUser ) 
            ? this.props.userConnected.idUser 
            : "user_4819_1562334165648"
        }
        config={this.props.config}
        />}
        />
        : null;
 
        return (
            ( utils.isEmptyObject(this.props.userConnected) )
                ? 
                <Router>
                    <section>
                        {loginFormComp}
                    </section>
                </Router>
                :
                <Router>
                    <section>
                        {topBarComp}
                    </section>                  
                    <section className="content">
                        <Switch>
                            {managerDashboardCom}

                            {formWrapperComp}
                        </Switch>
                    </section>
                </Router>
        );
    }
}
const mapStateToProps = (state) => {

    return {
        config:state.configReducer.config,
        userConnected: state.userReducer.userConnected,
        eaeUser:state.userReducer.eaeUser,
        eaeUserRole:state.userReducer.eaeUserRole,
        statusForm:state.enumReducer.statusForm,
    };
};

const mapDispatchToProps    = (dispatch) => {
    return {
        setConfig: (env) => dispatch({type:actionConfig.SET_CONFIG, env:env}),
        addStatusForm: (statusFormList) => dispatch({type:actionEnum.ADD_ALL_STATUS_FORM, statusForm:statusFormList})
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(App);