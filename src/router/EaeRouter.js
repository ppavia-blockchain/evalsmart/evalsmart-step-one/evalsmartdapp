import * as utils from '../utils/EvalsmartUtils';
import * as EaeConst from '../model/EaeConst';

const fileName   = "EaeRouter";

export class EaeRouter {
    isDebug     = false;
    constructor (config={log:{isDebug:false, package:[]}}) {
        const methodName            = "constructor";
        this.isDebug = utils.isDebug (config.log, fileName);
        this.currentDomain  = null;
    }

    getDomain = (formStruct, orderId) => {
        const methodName            = "getDomain";
        this.currentDomain = formStruct.domains.find ( (domain) => {
            return (parseInt(orderId, 10) === parseInt(domain.order, 10));
        });
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][this.currentDomain]`, '\n', this.currentDomain);
        }
        return this.currentDomain;
    }
}