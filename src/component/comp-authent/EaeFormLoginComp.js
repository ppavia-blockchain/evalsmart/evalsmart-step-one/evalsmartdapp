/* REACT */
import React from 'react';
import PropTypes from 'prop-types';

/* REDUX */
import { connect } from 'react-redux';
import * as actionUserType from '../../redux/actions/action-user';

/* SERVICES */
import EaeAuthenticationService from '../../service/EaeAuthenticationService';

/* UTILS */ 
import * as EaeConst from '../../model/EaeConst';
import * as utils from '../../utils/EvalsmartUtils';

/* BOOTSTRAP - CSS */
import {Row, Col} from 'react-bootstrap';
import {Form, InputGroup, FormControl, Button} from 'react-bootstrap';
import classes from './EaeFormLoginComp.css';

/* REACT ROUTER */
import { BrowserRouter as Router } from "react-router-dom";

/* COMPONENTS */
import EaeLinkUI from '../../component/comp-form/comp-formUI/comp-commons-UI/EaeLinkUI';


const fileName = "EaeFormLoginComp";

/**
 * Manage authentification -> statefull, global storage
 */
class EaeFormLoginComp extends React.Component {
    state = {};
    constructor(props) {
        super(props);
        const methodName        = "constructor";
        this.isDebug            = utils.isDebug (props.config.log, fileName);
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][props]`, '\n', props);
        }
        this.eaeAuthenticationService        = new EaeAuthenticationService(props.config);

        /**
         * refresh props from session data only if necessary (on refresh navigator)
         */
        this.eaeAuthenticationService.refreshPropsFromSession(
            this.props.addUserConnected,
            this.props.addEaeUser,
            this.props.addUserRole,
            this.props.userConnected,
            this.props.eaeUser,
            this.props.eaeUserRole
        );

        this.state  = {
            goto:"/form",
            role:EaeConst.USER_ROLE_CODE_COLLABORATOR
        }

        this.onChangeInputHandler       = this.onChangeInputHandler.bind(this);
        this.submitAuthentFormHandler   = this.submitAuthentFormHandler.bind(this);
    }
    componentDidMount() {
    }

    componentDidUpdate() {
    }

    shouldComponentUpdate ( nextProps, nextState ) {
        const methodName        = "shouldComponentUpdate";
        if ( this.props.userConnected !== nextProps.userConnected ||
            this.props.eaeUser !== nextProps.eaeUser ||
            this.state.userName !== nextState.userName || 
            this.state.pathphrase !== nextState.pathphrase ||
            this.state.goto !== nextState.goto) {
            return true;
        }
        return false;
    }
    
    setUserFromAuthentication   = async (userName, pathphrase, role) => {
        const methodName            = "setUserFromAuthentication";
        if ( utils.isEmptyObject(this.props.userConnected) ) {
            let userAuthenticated = null;
            
            userAuthenticated = await this.eaeAuthenticationService.setUserAuthentication(userName, pathphrase, role);
            if ( this.isDebug ) {
                console.log(`[${fileName}][${methodName}][userAuthenticated]`, '\n', userAuthenticated);
            }
            if ( !utils.isEmptyObject(userAuthenticated) ) {
                this.eaeAuthenticationService.login(
                    this.props.addUserConnected,
                    this.props.addEaeUser,
                    this.props.addUserRole,
                    userAuthenticated,
                    role
                );
            }
        }
    }

    _getGoto    = (role) => {
        switch (role) {
            case EaeConst.USER_ROLE_CODE_COLLABORATOR :                
                return '/form';
            case EaeConst.USER_ROLE_CODE_MANAGER :
                return '/manager/list-userforms';
            default :
                alert(role);

        }
    }

    onChangeInputHandler = (event) => {
        const methodName            = "onChangeInputHandler";
        switch (event.target.name) {
            case "UserName" :                
                this.setState ({
                    userName:event.target.value
                });
                break;
            case "Passphrase" :                
                this.setState ({
                    pathPrase:event.target.value
                });
                break;
            case "Role" :
                this.setState ({
                    role:event.target.value,
                    goto:this._getGoto(event.target.value)
                });
                break;
            default :
                alert(event.target.value);

        }
    }

    submitAuthentFormHandler = (event) => {
        switch (event.target.name) {
            case "submitAuthentForm" :
                this.setUserFromAuthentication( this.state.userName, this.state.pathPrase, this.state.role );
                break;
            default :
                alert(event.target.value);
        }
    }

    render () {
        const methodName            = "render";
        if ( utils.isEmpty( this.props.userConnected ) ) {
            return null;
        }
        else {
            if ( this.isDebug ) {
                console.log(`[${fileName}][${methodName}][props]`, '\n', this.props);
            }
            return (
                <Form className="EaeAuthentForm">
                    <Form.Group as={Row} controlId="eaeUserName">
                        <Form.Label column sm="2">
                            Enter your username :
                        </Form.Label>
                        <Col sm="10">
                            <InputGroup className="mb-3">
                                <FormControl
                                name="UserName"
                                placeholder="Recipient's username"
                                aria-label="Recipient's username"
                                aria-describedby="UserName"
                                onChange={this.onChangeInputHandler}
                                />
                                <InputGroup.Append>
                                    <InputGroup.Text id="eaeemail">{EaeConst.END_EMAIL_DOMAIN}</InputGroup.Text>
                                </InputGroup.Append>
                            </InputGroup>
                        </Col>
                    </Form.Group>
                    <Form.Group as={Row} controlId="exampleForm.ControlSelect1">
                        <Form.Label column sm="2">
                            Enter your passphrase :
                        </Form.Label>
                        <Col sm="10">
                            <Form.Control 
                            as="input"
                            name="Passphrase"
                            placeholder="Passphrase"
                            onChange={this.onChangeInputHandler}
                            >
                            </Form.Control>
                        </Col>
                    </Form.Group>
                    <Form.Group as={Row} controlId="exampleForm.ControlSelect1">
                        <Form.Label column sm="2">
                            Select your profile :
                        </Form.Label>
                        <Col sm="10">
                            <Form.Control
                            as="select"
                            name="Role"
                            onChange={this.onChangeInputHandler}>
                                <option value={EaeConst.USER_ROLE_CODE_COLLABORATOR}>Collaborator</option>
                                <option value={EaeConst.USER_ROLE_CODE_MANAGER}>Manager</option>
                            </Form.Control>
                        </Col>
                    </Form.Group>
                    <EaeLinkUI 
                        to={this.state.goto}
                        content={ 
                            <Button 
                            name="submitAuthentForm"
                            variant="primary" 
                            type="button"
                            onClick={this.submitAuthentFormHandler}>
                                Login
                            </Button>
                        }
                    />
                </Form>
            );
        }
    }
}

EaeFormLoginComp.propTypes = {
    config: PropTypes.object
};


const mapStateToProps = (state) => {
    return {
        eaeUser:state.userReducer.eaeUser,
        userConnected: state.userReducer.userConnected,
    };
};

const mapDispatchToProps    = (dispatch) => {
    return {
        addUserConnected: (userConnected) => dispatch({type:actionUserType.ADD_USER_CONNECTED, userConnected:userConnected}),
        addEaeUser: (eaeUser) => dispatch({type:actionUserType.ADD_EAE_USER, eaeUser:eaeUser}),
        addUserRole: (userRole) => dispatch({type:actionUserType.ADD_ROLE_USER, userRole:userRole})
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(EaeFormLoginComp);