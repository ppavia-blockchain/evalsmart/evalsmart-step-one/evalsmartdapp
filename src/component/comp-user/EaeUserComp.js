import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as utils from '../../utils/EvalsmartUtils';

const fileName = "EaeUserComp";

class EaeUserComp extends React.Component {
    state = {};
    constructor(props) {
        super(props);
        const methodName            = "constructor";
        this.isDebug            = utils.isDebug (props.config.log, fileName);
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][props]`, '\n', props);
        }
    }
    componentDidMount() {
    }

    componentDidUpdate() {
    }

    idUserHandler = (event) => {
        this.props.addIdUser("001");
        const idUserUI = document.querySelector("#displayIdUser");
        if ( event.target.name === 'addIdUser') {
            idUserUI.innerHTML = `User ID = ${this.props.idUserUI}`
        }
    }

    render () {
        if ( utils.isEmpty(this.props.userConnected) ) {
            return null;
        }
        else {
            const userUI        =  <span>{this.props.userConnected.fullName}</span>;
            return (
                <div className="user-owner">
                    <span>
                        <h3>{userUI} [account = {this.props.userConnected.accounts[0]}]</h3>
                    </span>
                </div>
            );
        }
    }
}

EaeUserComp.propTypes = {
    config: PropTypes.object,
    idUser: PropTypes.string
};


const mapStateToProps = (state) => {
    return {
        eaeUser:state.userReducer.eaeUser,
        userConnected:state.userReducer.userConnected,
    };
};

export default connect(mapStateToProps, null)(EaeUserComp);