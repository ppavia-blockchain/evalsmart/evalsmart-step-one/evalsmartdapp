import React from 'react';
import * as utils from '../../utils/EvalsmartUtils';
import { connect } from 'react-redux';

import './comp-form-style/EaeForm.css';

import {Row, Col} from 'react-bootstrap';


const fileName = "EaeHeaderFormComp";

class EaeHeaderFormComp extends React.Component {
    constructor(props) {
        super(props);
        const methodName       = "constructor";
        this.isDebug           = utils.isDebug (props.config.log, fileName);
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][props]`, '\n', props);
        }
        this.formStruct       = props.formStruct;
        this.userForm         = props.userForm;
        this.statusLabel      = props.statusLabel;
    }

    componentDidMount() {
    }

    render () {
        if ( utils.isEmpty(this.formStruct) ) {
            return null;
        }
        else {
            const createdOn     = new Date(this.userForm.dtCreated).toDateString();            
            return (
                <Row className="infos" >
                    <Col md={6}><h2>{this.formStruct.label}</h2></Col>
                    <Col md={3}><p>created on {createdOn}</p></Col>
                    <Col md={3}><p>On status {this.statusLabel}</p></Col>
                </Row>
            );
        }
    }
}
const mapStateToProps = (state) => {
    return {
        userConnected:state.userReducer.userConnected,
        eaeUser:state.userReducer.eaeUser,
    };
};
export default connect(mapStateToProps)(EaeHeaderFormComp);