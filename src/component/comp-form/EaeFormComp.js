import React from 'react';
import * as utils from '../../utils/EvalsmartUtils';
import * as EaeConst from '../../model/EaeConst';
import { connect } from 'react-redux';

import './comp-form-style/EaeForm.css';

import {Container} from 'react-bootstrap';
import { BrowserRouter as Router, Route } from "react-router-dom";

import EaeFormDomainComp from './comp-form-domain/EaeFormDomainComp';

const fileName = "EaeFormComp";

class EaeFormComp extends React.Component {
    constructor(props) {
        super(props);
        const methodName       = "constructor";
        this.isDebug           = utils.isDebug (this.props.config.log, fileName);
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][props]`, '\n', props);
        }
        this.formStruct       = props.formStruct;
        this.userForm         = props.userForm;
        this.statusLabel      = props.statusLabel;
    }

    componentDidMount() {
    }

    render () {
        if ( utils.isEmpty(this.formStruct) ) {
            return null;
        }
        else {
            return (
                <Container fluid>
                    <Router>
                        <Route path="/form/domains/:orderId" 
                        render={(props) => <EaeFormDomainComp {...props} // this is the only way to pass parameters to child component of the route
                        eaeServiceComp={this.props.eaeServiceComp}
                        addResponse={this.props.addResponse}
                        addResponses={this.props.addResponses}
                        initDeletedResponsesStateArray={this.props.initDeletedResponsesStateArray}
                        initUserResponsesStateArray={this.props.initUserResponsesStateArray}                        
                        />}
                        >    
                        </Route>
                    </Router>
                </Container>
            );
        }
    }
}
const mapStateToProps = (state) => {
    return {
        config:state.configReducer.config,
        userConnected:state.userReducer.userConnected,
        eaeUser:state.userReducer.eaeUser,
        userForm: state.userFormReducer.userForm,
        formStruct: state.formStructReducer.formStruct
    };
};
export default connect(mapStateToProps)(EaeFormComp);