import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as utils from '../../../../utils/EvalsmartUtils';

const fileName = "EaeTextUI";

class EaeTextUI extends React.Component {
    constructor(props) {
        super(props);
        const methodName    = "constructor";
        this.isDebug    = utils.isDebug (this.props.config.log, fileName);
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][this.props]`, '\n', this.props);
        }
    }

    render () {
        return (
            <p>{this.props.content}</p>
        );
    }
}

EaeTextUI.propTypes = {
}

const mapStateToProps = (state) => {
    return {
        config: state.configReducer.config
    };
};
const mapDispatchToProps    = (dispatch) => {
    return {};
};
export default connect(mapStateToProps, mapDispatchToProps)(EaeTextUI);