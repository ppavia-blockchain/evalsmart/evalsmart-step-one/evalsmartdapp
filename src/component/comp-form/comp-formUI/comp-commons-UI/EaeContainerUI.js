/* REACT */
import React from 'react';
import PropTypes from 'prop-types';

/* REDUX */
import { connect } from 'react-redux';

/* BOOTTRAP AND CSS */
import {Container} from 'react-bootstrap';

/* UTILS */
import * as utils from '../../../../utils/EvalsmartUtils';

/* COMPONENTS */


const fileName = "EaeContainerUI";

class EaeContainerUI extends React.Component {
    constructor(props) {
        super(props);
        const methodName       = "constructor";
        this.isDebug    = utils.isDebug (this.props.config.log, fileName);
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][this.props]`, '\n', this.props);
        }
    }

    render () {
        return (
            <Container>
                {this.props.componentUI.config.content}
            </Container>
        );
    }
}

EaeContainerUI.propTypes = {
}

const mapStateToProps = (state) => {
    return {
        config: state.configReducer.config
    };
};
const mapDispatchToProps    = (dispatch) => {
    return {};
};
export default connect(mapStateToProps, mapDispatchToProps)(EaeContainerUI);