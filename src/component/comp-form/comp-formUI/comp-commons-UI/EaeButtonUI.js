/* REACT */
import React from 'react';
import PropTypes from 'prop-types';

/* REDUX */
import { connect } from 'react-redux';

/* BOOTTRAP AND CSS */
import {Button} from 'react-bootstrap';

/* UTILS */
import * as utils from '../../../../utils/EvalsmartUtils';

/* COMPONENTS */


const fileName = "EaeButtonUI";

class EaeButtonUI extends React.Component {
    constructor(props) {
        super(props);
        const methodName       = "constructor";
        this.isDebug    = utils.isDebug (this.props.config.log, fileName);
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][this.props]`, '\n', this.props);
        }
    }

    render () {
        return (
            <Button 
            name={this.props.name}
            id={this.props.id}
            className={this.props.className}
            onClick={this.props.click}
            value={this.props.value}
            >
                {this.props.label}
            </Button>
        );
    }
}

EaeButtonUI.propTypes = {
}

const mapStateToProps = (state) => {
    return {
        config: state.configReducer.config
    };
};
const mapDispatchToProps    = (dispatch) => {
    return {};
};
export default connect(mapStateToProps, mapDispatchToProps)(EaeButtonUI);