/* REACT */
import React from 'react';
import PropTypes from 'prop-types';

/* REDUX */
import { connect } from 'react-redux';

/* BOOTTRAP AND CSS */
import {Link} from 'react-router-dom';

/* UTILS */
import * as utils from '../../../../utils/EvalsmartUtils';

/* COMPONENTS */

const fileName = "EaeLinkUI";

class EaeLinkUI extends React.Component {
    constructor(props) {
        super(props);
        const methodName       = "constructor";
        this.isDebug    = utils.isDebug (this.props.config.log, fileName);
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][this.props]`, '\n', this.props);
        }
    }

    render () {
        return (
            <Link className={this.props.classNames} to={this.props.to}>
                {this.props.content}
            </Link>
        );
    }
}

EaeLinkUI.propTypes = {
}

const mapStateToProps = (state) => {
    return {
        config: state.configReducer.config
    };
};
const mapDispatchToProps    = (dispatch) => {
    return {};
};
export default connect(mapStateToProps, mapDispatchToProps)(EaeLinkUI);