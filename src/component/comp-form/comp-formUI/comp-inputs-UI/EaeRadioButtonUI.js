import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as utils from '../../../../utils/EvalsmartUtils';
import classes from './EaeRadioButtonUI.module.css';

import {Form} from 'react-bootstrap';

const fileName = "EaeRadioButtonUI";

class EaeRadioButtonUI extends React.Component {
    constructor(props) {
        super(props);
        const methodName        = "constructor";
        this.isDisabled         = props.isDisabled;
        this.id                 = props.id;
        this.name               = props.name;
        this.value              = props.value;
        this.isInLine           = props.isInLine;

        this.isDebug    = utils.isDebug (this.props.config.log, fileName);
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][this.props]`, '\n', this.props);
        }
    }

    render () {
        const rbDisabled        = 
        <Form.Check
        className={classes.Disabled}
        id={this.id}
        type="radio"
        name={this.name}
        value={this.value}
        onChange={this.props.click}
        checked={this.props.isChecked}
        disabled
        />;

        const rb                =
        <Form.Check
        className={classes.Default}
        id={this.id}
        type="radio"
        name={this.name}
        value={this.value}
        onChange={this.props.click}
        checked={this.props.isChecked}
        disabled={this.props.isDisabled}
        />;

        const rbDisabledInLine        = 
        <Form.Check
        className={classes.DisabledInLine}
        inline 
        id={this.id}
        type="radio"
        name={this.name}
        value={this.value}
        onClick={this.props.click}
        checked={this.props.isChecked}
        disabled={this.props.isDisabled}
        />;

        const rbInLine                =
        <Form.Check
        className={classes.InLine}
        inline
        id={this.id}
        type="radio"
        name={this.name}
        value={this.value}
        onClick={this.props.click}
        checked={this.props.isChecked}
        disabled={this.props.isDisabled}
        />;

        const radioButtonComp   = 
        ( this.isDisabled && this.isInLine )
        ? rbDisabledInLine
        : ( this.isDisabled && !this.isInLine )
        ? rbDisabled
        : ( !this.isDisabled && this.isInLine )
        ? rbInLine
        : ( !this.isDisabled && !this.isInLine )
        ? rb
        : null;

        return (
            <label className={classes.WrapperRadioButton}>
                {radioButtonComp}
            </label>
        );
    }
}
EaeRadioButtonUI.propTypes = {
    id: PropTypes.string,
    name: PropTypes.string,
    value: PropTypes.string,
    isDisabled: PropTypes.bool,
    isInLine: PropTypes.bool,
}

const mapStateToProps = (state) => {
    return {
        config: state.configReducer.config
    };
};
const mapDispatchToProps    = (dispatch) => {
    return {};
};
export default connect(mapStateToProps, mapDispatchToProps)(EaeRadioButtonUI);