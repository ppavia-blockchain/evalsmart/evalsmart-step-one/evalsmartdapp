import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as utils from '../../../../utils/EvalsmartUtils';
import classes from './EaeInputTextUI.module.css';

import {Form} from 'react-bootstrap';

const fileName = "EaeInputTextUI";

class EaeInputTextUI extends React.Component {
    constructor(props) {
        super(props);
        const methodName        = "constructor";

        this.isDebug    = utils.isDebug (this.props.config.log, fileName);
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][this.props]`, '\n', this.props);
        }
    }

    render () {
        const methodName        = "render";
        const label        = ( !utils.isEmpty(this.props.label, true) )
        ? <Form.Label>{this.props.label}</Form.Label>
        : null;

        const control       = 
        <Form.Control 
        type="text"
        defaultValue={this.props.defaultValue}
        placeholder={this.props.placeholder}
        onChange={this.props.change}
        />
        
        return (
            <Form.Group>
                {label}
                {control}
            </Form.Group>
        );
    }
}
EaeInputTextUI.propTypes = {
}

const mapStateToProps = (state) => {
    return {
        config: state.configReducer.config
    };
};
const mapDispatchToProps    = (dispatch) => {
    return {};
};
export default connect(mapStateToProps, mapDispatchToProps)(EaeInputTextUI);