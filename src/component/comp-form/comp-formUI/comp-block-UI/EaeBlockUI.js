import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Container } from 'react-bootstrap';
import * as utils from '../../../../utils/EvalsmartUtils';
import * as EaeConst from '../../../../model/EaeConst';
import EaeHeaderBlockUI from './EaeHeaderBlockUI';
import EaeBodyBlockUI from './EaeBodyBlockUI';
import EaeFooterBlockUI from './EaeFooterBlockUI';

const fileName = "EaeBlockUI";

class EaeBlockUI extends React.Component {
    constructor(props) {
        super(props);
        const methodName        = "constructor";
        this.isDebug            = utils.isDebug (this.props.config.log, fileName);
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][props]`, '\n', props);
        }
    }
    componentDidMount () {
        const methodName        = "componentDidMount";

    }
    
    render() {
        const methodName        = "render";        
        // block headers
        this.eaeHeaderBlockUI = ( !utils.isEmpty(this.props.headersUI ) )
        ?
        this.props.headersUI.map (headerUI => {
            return ( !utils.isEmptyObject(headerUI) )
            ? <EaeHeaderBlockUI
            key={headerUI.id}
            headerUI={headerUI} // hight level UI component
            />
            : null;
        })
        : null;
    
        // block bodies
        this.eaeBodyBlockUI = ( !utils.isEmptyObject(this.props.bodiesUI ) )
        ?
        this.props.bodiesUI.map (bodyUI => {
            return ( !utils.isEmptyObject(bodyUI) )
            ? <EaeBodyBlockUI
            key={bodyUI.props.componentUI.id}
            bodyUI={bodyUI} // hight level UI component
            />
            : null;
        })
        : null;
    
        // block footers
        this.eaeFooterBlockUI = ( !utils.isEmpty(this.props.footersUI ) ) 
        ?
        this.props.footersUI.map (footerUI => {
            return ( !utils.isEmptyObject(footerUI) )
            ? <EaeFooterBlockUI
            key={footerUI.id}
            footerUI={footerUI} // hight level UI component
            />
            : null;
        })
        : null;
    
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][this.eaeBodyBlockUI]`, '\n', this.eaeBodyBlockUI);
        }

        return (
            <Container>
                {this.eaeHeaderBlockUI}
                {this.eaeBodyBlockUI}
                {this.eaeFooterBlockUI}
            </Container>
        );
    }

}

EaeBlockUI.propTypes = {

};

const mapStateToProps = (state) => {
    return {
        config: state.configReducer.config
    };
};
const mapDispatchToProps    = (dispatch) => {
    return {};
};
export default connect(mapStateToProps, mapDispatchToProps)(EaeBlockUI);