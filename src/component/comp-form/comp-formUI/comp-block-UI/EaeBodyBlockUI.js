import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as utils from '../../../../utils/EvalsmartUtils';
import * as EaeConst from '../../../../model/EaeConst';
import EaeTableUI from '../comp-table-UI/EaeTableUI';
import EaeRowUI from '../comp-rows-UI/EaeRowUI';
import {Container} from 'react-bootstrap';

const fileName = "EaeBodyBlockUI";

class EaeBodyBlockUI extends React.Component {
    constructor(props) {
        super(props);
        const methodName        = "constructor";
        this.isDebug            = utils.isDebug (this.props.config.log, fileName);
        if ( this.isDebug ) {
            //console.log(`[${fileName}][${methodName}][props]`, '\n', this.props);
        }
    }
    

    render() {
        const methodName        = "render";

        if ( utils.isEmptyObject(this.props.bodyUI) ) {
            if ( this.isDebug ) {
                console.log(`[${fileName}][${methodName}][this.props.bodyUI empty]`, '\n', this.props);
            }
            return null;
        }
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][this.props.bodyUI]`, '\n', this.props.bodyUI);
        }

        return this.props.bodyUI;
    }

}

EaeBodyBlockUI.propTypes = {

};

const mapStateToProps = (state) => {
    return {
        config: state.configReducer.config
    };
};
const mapDispatchToProps    = (dispatch) => {
    return {};
};
export default connect(mapStateToProps, mapDispatchToProps)(EaeBodyBlockUI);