import React from 'react';
import {Container, Row, Col} from 'react-bootstrap';
import classes from './EaeHeaderBlockUI.module.css';
import * as utils from '../../../../utils/EvalsmartUtils';
import { connect } from 'react-redux';

const fileName = "EaeHeaderBlockUI";

class EaeHeaderBlockUI extends React.Component {
    constructor(props) {
        super(props);
        this.header    = props.headerUI;
        const methodName        = "constructor";
        this.isDebug            = utils.isDebug (this.props.config.log, fileName);
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][props]`, '\n', this.props);
        }
    }

    render () {
        const headerBlockLib    =
            <Row className="justify-content-md-center">
                <h2>{this.header.title}</h2>
            </Row>;

        const headerBlockHr     =
            <Row className="justify-content-md-center">
                <Col md="3" ><hr/></Col>
            </Row>

        return (
            <Container className={classes.Header}>
                {headerBlockLib}
                {headerBlockHr}
            </Container>
        );
    }
}
EaeHeaderBlockUI.propTypes = {

};

const mapStateToProps = (state) => {
    return {
        config: state.configReducer.config
    };
};
const mapDispatchToProps    = (dispatch) => {
    return {};
};
export default connect(mapStateToProps, mapDispatchToProps)(EaeHeaderBlockUI);