import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Col } from 'react-bootstrap';
import * as utils from '../../../../utils/EvalsmartUtils';
import * as EaeConst from '../../../../model/EaeConst';

const fileName = "EaeColUI";

class EaeColUI extends React.Component {
    constructor(props) {
        super(props);
        const methodName        = "constructor";
        this.isDebug            = utils.isDebug (this.props.config.log, fileName);
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][props]`, '\n', props);
        }
    }

    render() {
        if ( utils.isEmptyObject(this.props.componentUI) ) {
            return null;
        }
        
        const contentCol = ( utils.isEmptyObject(this.props.componentUI.config.content) ) ? null : this.props.componentUI.config.content;
        return (
            <Col
            className={ ( utils.isEmptyObject(this.props.componentUI.config.classes) ) ? this.props.componentUI.config.classes.join(' ') : ""}
            md={this.props.sizeCol}
            >
                {contentCol}
            </Col>
        );
    }

}

EaeColUI.propTypes = {

};

const mapStateToProps = (state) => {
    return {
        config: state.configReducer.config
    };
};
const mapDispatchToProps    = (dispatch) => {
    return {};
};
export default connect(mapStateToProps, mapDispatchToProps)(EaeColUI);