import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Row } from 'react-bootstrap';
import * as utils from '../../../../utils/EvalsmartUtils';
import * as EaeConst from '../../../../model/EaeConst';
import EaeColUI from './EaeColUI';

const fileName = "EaeRowUI";

class EaeRowUI extends React.Component {
    constructor(props) {
        super(props);
        const methodName        = "constructor";
        this.isDebug            = utils.isDebug (this.props.config.log, fileName);
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][props]`, '\n', this.props);
        }
    }

    render() {
        const methodName        = "render";
        const contentRow = ( !utils.isEmptyObject(this.props.componentUI) && !utils.isEmptyObject(this.props.componentUI.config.content) ) 
        ? this.props.componentUI.config.content.filter ( (oCol) => {
            if ( this.isDebug ) {
                console.log(`[${fileName}][${methodName}][oCol]`, '\n', oCol);
            }
            return ( !oCol.sizeCol );
        })
        : [];
        return (
            <Row
            className={(this.props.componentUI.config.classes) ? this.props.componentUI.config.classes : ""}
            >
                {contentRow}
            </Row>
        );
    }

}

EaeRowUI.propTypes = {

};

const mapStateToProps = (state) => {
    return {
        config: state.configReducer.config
    };
};
const mapDispatchToProps    = (dispatch) => {
    return {};
};
export default connect(mapStateToProps, mapDispatchToProps)(EaeRowUI);