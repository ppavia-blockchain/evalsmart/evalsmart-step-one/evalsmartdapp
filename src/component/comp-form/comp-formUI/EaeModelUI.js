/* REACT */
import React from 'react';
import PropTypes from 'prop-types';

/* REDUX */
import { connect } from 'react-redux';

/* BOOTTRAP AND CSS */
import {Button} from 'react-bootstrap';
import { Link } from "react-router-dom";
import classes from './EaeFooterDomainComp.module.css';

/* UTILS */
import * as utils from '../../../utils/EvalsmartUtils';
import * as EaeConst from '../../../model/EaeConst';
import { EaeRouter } from '../../../router/EaeRouter';

/* COMPONENTS */
import * as formUI from '../comp-formUI/comp-formUI-bean/oFormUIs';
import EaeRowUI from '../comp-formUI/comp-rows-UI/EaeRowUI';
import EaeButtonUI from '../comp-formUI/comp-commons-UI/EaeButtonUI';
import EaeLinkUI from '../comp-formUI/comp-commons-UI/EaeLinkUI';

const fileName = "EaeModelUI";

class EaeModelUI extends React.Component {
    constructor(props) {
        super(props);
        const methodName        = "constructor";
        this.isDebug            = utils.isDebug (this.props.config.log, fileName);
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][props]`, '\n', this.props);
        }
    }

    render() {
        return (
            null
        );
    }

}

EaeModelUI.propTypes = {

};

const mapStateToProps = (state) => {
    return {
        config: state.configReducer.config
    };
};
const mapDispatchToProps    = (dispatch) => {
    return {};
};
export default connect(mapStateToProps, mapDispatchToProps)(EaeModelUI);