import * as EaeConst from '../../../../model/EaeConst';

export const oTableUI        = () => {
    return {
        id:null,
        linkedData:null,
        typeUI: EaeConst.UI_TABLE,
        config: {
            classes: [],
            component:null,
            theader: [],
            tbody: [],
            tfooter: []
        }
    };
};

export const oTheaderUI        = () => {
    return {
        id:null,
        linkedData:null,
        typeUI: EaeConst.UI_THEADER,
        config: {
            classes: [],
            content: null,
            defaultContent:null,
            sizeCol:1,
            component:null
        }
    };
};

export const oTbodyUI        = () => {
    return {
        id:null,
        linkedData:null,
        typeUI: EaeConst.UI_TBODY,
        config: {
            classes: [],
            component:null,
            content: null,
            defaultContent:null,
            sizeCol:1
        }
    };
};

export const oTfooterUI        = () => {
    return {
        id:null,
        linkedData:null,
        typeUI: EaeConst.UI_TFOOTER,
        config: {
            classes: [],
            component:null,
            content: null,
            defaultContent:null,
            sizeCol:1
        }
    };
};

export const oContainerRowsUI = () => {
    return {
        id:null,
        linkedData:null,
        typeUI: EaeConst.UI_ROW,
        config: {
            classes: [],
            component:null,
            content:null,
            width:1,
            hight:1
        }
    };
};

export const oRowUI        = () => {
    return {
        id:null,
        linkedData:null,
        typeUI: EaeConst.UI_ROW,
        config: {
            classes: [],
            component:null,
            content:null,
            width:1,
            hight:1
        }
    };
};

export const oColUI        = () => {
    return {
        id:null,
        linkedData:null,
        typeUI: EaeConst.UI_COL,
        config: {
            classes: [],
            component:null,
            content:null,
            width:1,
            hight:1,
            sizeCol:1
        }
    };
};

export const oRadioButtonUI = () => {
    return {
        id:null,
        linkedData:null,
        typeUI: EaeConst.UI_RADIOBUTTON,
        config: {
            classes: [],
            content:null,
            component:null,
            id:null,
            name:null,
            value:null,
            onClick:null,
            isChecked:false,
            isDisabled:false,
            isInLine:false
        }
    };
};

export const oTextAreaUI        = () => {
    return {
        id:null,
        linkedData:null,
        typeUI: EaeConst.UI_INPUT_AREA,
        config: {
            classes: [],
            component:null,
            name:"",
            label: "",
            placeholder:"",
            defaultValue:"",
            value:null,
            readOnly:false,
            onChange:null,
            content:null,
            rows:3
        }
    };
};


export const oInputTextUI        = () => {
    return {
        id:null,
        linkedData:null,
        typeUI: EaeConst.UI_INPUT_TEXT,
        config: {
            classes: [],
            component:null,
            label: "",
            placeholder:"",
            defaultValue:"",
            value:null,
            readOnly:false,
            onChange:null,
            content:null
        }
    };
};

export const oTextUI        = () => {
    return {
        id:null,
        linkedData:null,
        typeUI: EaeConst.UI_TEXT,
        config: {
            classes: [],
            component:null,
            label: "",
            defaultValue:"",
            content:null
        }
    };
};

export const oButtonUI        = () => {
    return {
        id:null,
        linkedData:null,
        typeUI: EaeConst.UI_BUTTON,
        config: {
            classes: [],
            component:null,
            label: "",
            placeholder:"",
            defaultValue:"",
            value:null,
            readOnly:false,
            onClick:null,
            content:null
        }
    };
};

export const oNavUI        = () => {
    return {
        id:null,
        linkedData:null,
        typeUI: EaeConst.UI_NAV,
        config: {
            classes: [],
            component:null,
            label: "",
            defaultValue:"",
            content:null
        }
    };
};


export const oInfoUI        = () => {
    return {
        id:null,
        linkedData:null,
        typeUI: EaeConst.UI_TEXT,
        config: {
            classes: [],
            component:null,
            label: "",
            defaultValue:"",
            content:null
        }
    };
};

export const oBusinessDomainUI        = () => {
    return {
        id:null,
        linkedData:null,
        typeUI: EaeConst.UI_DOMAIN,
        config: {
            classes: [],
            component:null,
            label: "",
            readOnly:false,
            onClick:null,
            content:null
        }
    };
};

export const oFooterDomainUI        = () => {
    return {
        id:null,
        linkedData:null,
        typeUI: EaeConst.UI_FOOTER_DOMAIN,
        config: {
            classes: [],
            component:null,
            label: "",
            currentOrder:null,
            orderMax:null,
            readOnly:false,
            onClick:null,
            content:null
        }
    };
};
