/* REACT */
import React from 'react';
import PropTypes from 'prop-types';

/* BOOTTRAP AND CSS */
import {Nav, Navbar, Form, FormControl, Button} from 'react-bootstrap';

/* REDUX */
import { connect } from 'react-redux';
import * as actionUserType from '../../../../redux/actions/action-user';

/* UTILS */
import * as utils from '../../../../utils/EvalsmartUtils';
import * as EaeConst from '../../../../model/EaeConst';

/* MODELS */
import * as formUI from '../../comp-formUI/comp-formUI-bean/oFormUIs';

/* COMPONENTS */
import EaeLinkUI from '../comp-commons-UI/EaeLinkUI';

/* SERVICES */
import EaeAuthenticationService from '../../../../service/EaeAuthenticationService';

const fileName = "EaeNavUI";

class EaeNavUI extends React.Component {
    constructor(props) {
        super(props);
        const methodName        = "constructor";
        this.isDebug            = utils.isDebug (this.props.config.log, fileName);
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][props]`, props);
        }
        this.eaeAuthenticationService        = new EaeAuthenticationService(props.config);
        this.onClickHandler       = this.onClickHandler.bind(this);

        this.stateLabelLog = "Logout";
        // TODO - this is a mock
        this.navUI      = formUI.oNavUI();
        this.navUI.config.content = {
            
        }
    }

    componentDidMount () {
    }

    onClickHandler = (event) => {
        const methodName            = "onClickHandler";
        switch (event.target.name) {
            case "logout" :
                    this.eaeAuthenticationService.logout(
                        this.props.removeUserConnected, 
                        this.props.removeEaeUser, 
                        this.props.removeRole);
                if ( this.isDebug ) {
                    console.log(`[${fileName}][${methodName}][ this.props.userConnected]`,  this.props.userConnected);
                }                
                break;
            default :
                alert(event.target.value);
        }
    }

    render () {
        if ( !this.props.userConnected ) {
            return null;
        }

        return (
            <Navbar fixed="top" bg="dark" variant="dark">
                <Navbar.Brand href="#home">
                    <img
                    src="/assets/img/logo.svg"
                    width="30"
                    height="30"
                    className="d-inline-block align-top"
                    alt=""
                    />{' '}Evalsmart
                </Navbar.Brand>
                <Nav className="mr-auto">
                    <Nav.Link href="/">Home</Nav.Link>
                    <Nav.Link href="/form/domains/1">Your Form</Nav.Link>
                </Nav>
                <Form inline>
                    <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                    <EaeLinkUI 
                        to='/log'
                        content={ 
                            <Button 
                            name="logout" 
                            variant="outline-info"
                            onClick={this.onClickHandler}
                            >{this.stateLabelLog}</Button>
                        }
                    />
                </Form>
            </Navbar>
        );        
    }
}
EaeNavUI.propTypes = {

};

const mapStateToProps = (state) => {
    return {
        config:state.configReducer.config,
        userConnected:state.userReducer.userConnected,
        eaeUser:state.userReducer.eaeUser,
        userForm: state.userFormReducer.userForm,
        formStruct: state.formStructReducer.formStruct,
        eaeUserResponses: state.responseReducer.eaeUserResponses
    };
};
const mapDispatchToProps    = (dispatch) => {
    return {
        removeUserConnected: () => dispatch({type:actionUserType.REMOVE_USER_CONNECTED}),
        removeEaeUser: () => dispatch({type:actionUserType.REMOVE_EAE_USER}),
        removeRole: () => dispatch({type:actionUserType.REMOVE_ROLE_USER})
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(EaeNavUI);