import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as utils from '../../../../utils/EvalsmartUtils';
import * as EaeConst from '../../../../model/EaeConst';
import EaeCellTableUI from './EaeCellTableUI';

const fileName = "EaeRowTableUI";

class EaeRowTableUI extends React.Component {
    constructor(props) {
        super(props);
        const methodName        = "constructor";
        this.isDebug            = utils.isDebug (this.props.config.log, fileName);
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][props]`, '\n', props);
        }
    }

    componentDidMount () {

    }
    
    render() {
        const methodName        = "render";
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][this.props.trUI]`, '\n', this.props.trUI);
        }
        this.TRsComp   = ( !utils.isEmptyObject(this.props.trUI) ) // contains all tr of a tr
        ? this.props.trUI.map ( (oTr, index) => {
            
            return (
                <tr key={index}>
                    {
                        oTr.map ( (cell, lIndex) => { // contains all td of a tr
                            if ( this.isDebug ) {
                                console.log(`[${fileName}][${methodName}][cell]`, '\n', cell);
                            }
                            if ( !utils.isEmptyObject(cell) ) {
                                if ( cell.config.sizeCol > 0 ) {
                                    return (
                                        <EaeCellTableUI
                                        key={lIndex}
                                        cell={cell}
                                        />
                                    );
                                }
                            }
                        })
                    }
                </tr>
            );
        })
        : null;

        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][this.TRsComp]`, '\n', this.TRsComp);
        }

        return (
            <tbody>
                {this.TRsComp}
            </tbody>
        );
    }
}

EaeRowTableUI.propTypes = {
    trUI: PropTypes.array
};

const mapStateToProps = (state) => {
    return {
        config: state.configReducer.config
    };
};
const mapDispatchToProps    = (dispatch) => {
    return {};
};
export default connect(mapStateToProps, mapDispatchToProps)(EaeRowTableUI);