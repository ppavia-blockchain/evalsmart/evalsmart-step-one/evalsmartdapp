import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as utils from '../../../../utils/EvalsmartUtils';
import * as EaeConst from '../../../../model/EaeConst';
import EaeHeaderTableUI from './EaeHeaderTableUI';

const fileName = "EaeHeadersTableUI";

class EaeHeadersTableUI extends React.Component {
    constructor(props) {
        super(props);
        const methodName        = "constructor";
        this.isDebug            = utils.isDebug (this.props.config.log, fileName);
        if ( this.isDebug ) {
            //console.log(`[${fileName}][${methodName}][props]`, '\n', props);
        }
    }

    componentDidMount () {

    }
    
    render() {
        const methodName        = "render";
        this.theadersComp   = ( !utils.isEmptyObject(this.props.theaders) )
        ? this.props.theaders.map ( (theader, index) => {
            if ( !utils.isEmptyObject(theader) ) {
                if ( theader.config.sizeCol > 0 ) {
                    if ( this.isDebug ) {
                        console.log(`[${fileName}][${methodName}][theader]`, '\n', theader);
                    }
                    return (
                    <EaeHeaderTableUI
                    key={index}
                    theader={theader}
                    />
                    );
                }
            }
        })
        : null;

        return (
            <thead>
                <tr>
                    {this.theadersComp}
                </tr>
            </thead>
        );
    }
}

EaeHeadersTableUI.propTypes = {
    tableUI: PropTypes.object
};

const mapStateToProps = (state) => {
    return {
        config: state.configReducer.config
    };
};
const mapDispatchToProps    = (dispatch) => {
    return {};
};
export default connect(mapStateToProps, mapDispatchToProps)(EaeHeadersTableUI);