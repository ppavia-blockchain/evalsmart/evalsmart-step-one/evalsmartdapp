import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as utils from '../../../../utils/EvalsmartUtils';
import * as EaeConst from '../../../../model/EaeConst';

const fileName = "EaeCellTableUI";

class EaeCellTableUI extends React.Component {
    constructor(props) {
        super(props);
        const methodName        = "constructor";
        this.isDebug            = utils.isDebug (this.props.config.log, fileName);
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][props]`, '\n', props);
        }
    }

    render() {

        const theaderBody    = ( !utils.isEmptyObject(this.props.cell.config.content) )
        ?
        this.props.cell.config.content
        : "";

        return (
            <td colSpan={this.props.cell.config.sizeCol} >
                {theaderBody}
            </td>
        );
    }
}

EaeCellTableUI.propTypes = {
    tableUI: PropTypes.object
};

const mapStateToProps = (state) => {
    return {
        config: state.configReducer.config
    };
};
const mapDispatchToProps    = (dispatch) => {
    return {};
};
export default connect(mapStateToProps, mapDispatchToProps)(EaeCellTableUI);