import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Table } from 'react-bootstrap';
import * as utils from '../../../../utils/EvalsmartUtils';
import * as EaeConst from '../../../../model/EaeConst';
import EaeHeadersTableUI from './EaeHeadersTableUI';
import EaeRowTableUI from './EaeRowTableUI';

const fileName = "EaeTableUI";

class EaeTableUI extends React.Component {
    constructor(props) {
        super(props);
        const methodName        = "constructor";
        this.isDebug            = utils.isDebug (props.config.log, fileName);

        
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][props]`, '\n', props);
        }
    }
    
    componentDidMount () {
        const methodName        = "componentDidMount";
    }
    
    render() {
        const methodName        = "render";
        const headersTable  = ( !utils.isEmptyObject(this.props.componentUI) && !utils.isEmptyObject(this.props.componentUI.config) )
        ? 
        <EaeHeadersTableUI
        theaders={this.props.componentUI.config.theader}
        />
        : null;
        
        const bodiesTable  = ( !utils.isEmptyObject(this.props.componentUI.config.tbody) )
        ? 
        <EaeRowTableUI
        trUI={this.props.componentUI.config.tbody}
        />
        : null;
    
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][headersTable]`, '\n', headersTable);
            console.log(`[${fileName}][${methodName}][bodiesTable]`, '\n', bodiesTable);
        }

        return (
            <Table>
                {headersTable}
                {bodiesTable}
            </Table>
        );
    }
}

EaeTableUI.propTypes = {
    componentUI: PropTypes.object
};

const mapStateToProps = (state) => {
    return {
        config: state.configReducer.config
    };
};
const mapDispatchToProps    = (dispatch) => {
    return {};
};
export default connect(mapStateToProps, mapDispatchToProps)(EaeTableUI);