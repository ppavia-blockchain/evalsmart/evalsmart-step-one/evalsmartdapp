import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as utils from '../../../../utils/EvalsmartUtils';
import * as EaeConst from '../../../../model/EaeConst';

const fileName = "EaeHeaderTableUI";

class EaeHeaderTableUI extends React.Component {
    constructor(props) {
        super(props);
        const methodName        = "constructor";
        this.isDebug            = utils.isDebug (this.props.config.log, fileName);
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][props]`, '\n', props);
        }
    }

    render() {

        const theaderData    = ( !utils.isEmptyObject(this.props.theader.config.content) )
        ?
        this.props.theader.config.content
        : "";

        return (
            <th colSpan={this.props.theader.config.sizeCol} >
                {theaderData}
            </th>
        );
    }
}

EaeHeaderTableUI.propTypes = {
    tableUI: PropTypes.object
};

const mapStateToProps = (state) => {
    return {
        config: state.configReducer.config
    };
};
const mapDispatchToProps    = (dispatch) => {
    return {};
};
export default connect(mapStateToProps, mapDispatchToProps)(EaeHeaderTableUI);