/* REACT */
import React from 'react';
import PropTypes from 'prop-types';

/* REDUX */
import { connect } from 'react-redux';

/* BOOTTRAP AND CSS */
import {Button} from 'react-bootstrap';
import { Link } from "react-router-dom";
import classes from './EaeFormInfoComp.module.css';

/* UTILS */
import * as utils from '../../../utils/EvalsmartUtils';
import * as EaeConst from '../../../model/EaeConst';
import { EaeRouter } from '../../../router/EaeRouter';

/* COMPONENTS */
import EaeTextUI from '../comp-formUI/comp-commons-UI/EaeTextUI';

import {Container, Row, Col} from 'react-bootstrap';

const fileName = "EaeFormInfoComp";

class EaeFormInfoComp extends React.Component {
    constructor(props) {
        super(props);
        const methodName        = "constructor";
        this.isDebug            = utils.isDebug (this.props.config.log, fileName);
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][this.props.info]`, '\n', props);
        }
    }
    render () {
        const methodName        = "render";
        return (
            <EaeTextUI
            content={this.props.infoUI.config.content}
            />
        );
    }
}

EaeFormInfoComp.propTypes = {
}

const mapStateToProps = (state) => {
    return {
        eaeUser: state.userReducer.eaeUser,
        config: state.configReducer.config
    };
};
const mapDispatchToProps    = (dispatch) => {
    return {};
};
export default connect(mapStateToProps, mapDispatchToProps)(EaeFormInfoComp);