import React, {Component} from 'react';
import PropTypes from 'prop-types';
import EaeFormInfoComp from './EaeFormInfoComp';
import {Container, Row, Col} from 'react-bootstrap';

class EaeFormListInfosComp extends React.Component {
    constructor(props) {
        super(props);
    }
    render () {
        const infos = ( this.props.infos )
        ? this.props.infos.map((info, index) => {
            return <EaeFormInfoComp key={info.idInfo} info={info}/>;
        })
        : null;

        return (
            <Container>
                {infos}
            </Container>
        );
    }
}
EaeFormListInfosComp.propTypes = {
    infos: PropTypes.array
};
export default EaeFormListInfosComp;