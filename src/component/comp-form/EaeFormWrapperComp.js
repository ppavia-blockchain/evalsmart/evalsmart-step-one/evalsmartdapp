import React from 'react';
import PropTypes from 'prop-types';

/* BOOTTRAP AND CSS */
import {Container} from 'react-bootstrap';
import './comp-form-style/EaeFormComponent.css';

/* REDUX */
import { connect } from 'react-redux';
import * as actionUserForm from '../../redux/actions/action-userform';
import * as actionFormStruct from '../../redux/actions/action-formstruct';
import * as actionResponseType from '../../redux/actions/action-response';

/* UTILS */
import * as utils from '../../utils/EvalsmartUtils';
import * as EaeConst from '../../model/EaeConst';

/* COMPONENTS */
import EaeFormComp from './EaeFormComp';
import EaeHeaderFormComp from './EaeHeaderFormComp';

/* SERVICES */
import EaeServiceComp from '../../service/EaeServiceComp';
import EaeFormStructService from '../../service/formstruct-services/EaeFormStructService';
import EaeUserFormService from '../../service/userform-services/EaeUserFormService';

const fileName = "EaeFormWrapperComp";
/**
 * Form representation built with all form components
 * Here, the form struct and user form are stored into global state
 */
class EaeFormWrapperComp extends React.Component {
    constructor(props) {
        super(props);
        const methodName        = "constructor";
        this.isDebug            = utils.isDebug (this.props.config.log, fileName);
        this.idUser             = this.props.idUser;
        this.errors             = [];
        // TODO - this is a mock props
        this.dateLastActiveFormStruct   = "2019-01-01";
        
        this.idManager              = this.props.idManager;
        this.idOwner                = this.props.idOwner;
        this.eaeServiceComp         = new EaeServiceComp(props.config);
        this.formStructService      = new EaeFormStructService(props.config);
        this.userFormService        = new EaeUserFormService(props.config);

        
        // set up the state
        this.processFormDataLoading (this.idUser, this.idManager, this.idOwner);
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][props]`, this.props);
        }
    }

    processFormDataLoading = async (idUser, idManager, idOwner) => {
        const methodName        = "processFormDataLoading";
        let userFormBlockchainBean  = null;
        let userFormMongoBean       = null;
        let formStructBean          = null;
        idOwner                     = ( this.idUser === this.idOwner )
        ? idOwner
        : null;
        // 01 - search last active user form from blockchain ledger for this user
        // -> user form found -> then search user form by Ids from mongodb
        // -> user form not found -> then search last active user form from mongodb
        // -> error occured -> stop and log
        
        // 02 - search user form from mongodb
        // -> user form found -> then search form struct with user form id 
        // -> user form not found
        // -> if user form from blockchain exists -> stop and log
        // -> if not :
        // -> search last active form struct
        // -> error occured
        
        // 03 - search form struct from mongodb
        // -> form struct found -> create user form in mongodb
        // -> form struct not found -> is an error
        // -> error occured

        console.log(`[${fileName}][${methodName}][this.isDebug]`, this.isDebug);

        try {       
            userFormBlockchainBean  = this.getUserFormFromBlockchainLedger(idUser);

            // user form in blockchain ledger does not exist
            if ( utils.isEmptyObject (userFormBlockchainBean) ) {
                userFormMongoBean  = await this.getLastActiveUserFormFromMongo(idUser, idOwner);
                if ( this.isDebug ) {
                    // console.log(`[${fileName}][${methodName}][userFormMongoBean no ledger]`, '\n', userFormMongoBean);
                }
                if ( userFormMongoBean === null ) { // an error occured
                    throw new Error (`[${fileName}][${methodName}][ERROR RETRIEVING USER FORM]\n`);
                }
            }
            else {
                userFormMongoBean  = await this.getUserFormFromMongoByIds(idUser);
                if ( this.isDebug ) {
                    // console.log(`[${fileName}][${methodName}][userFormMongoBean ledger found]`, '\n', userFormMongoBean);
                }
                if ( userFormMongoBean === null ) { // an error occured
                    throw new Error (`[${fileName}][${methodName}][ERROR RETRIEVING USER FORM]\n`);
                }
            }        
            
            // search form struct from mongodb
            // a user form was found
            if ( !utils.isEmptyObject (userFormMongoBean) ) {
                formStructBean  = await this.getFormStructById(userFormMongoBean.idForm);
                if ( this.isDebug ) {
                    // console.log(`[${fileName}][${methodName}][formStructBean user form exist]`, '\n', formStructBean);
                }
            }
            else {
                formStructBean  = await this.getLastActiveFormStruct(this.dateLastActiveFormStruct);
                if ( this.isDebug ) {
                    // console.log(`[${fileName}][${methodName}][formStructBean user form does not exist]`, '\n', formStructBean);
                }
            
                // then, create user form in mongo
                userFormMongoBean = await this.eaeServiceComp.createUserForm(formStructBean.idForm, idUser, idManager);
                if ( this.isDebug ) {
                    // console.log(`[${fileName}][${methodName}][userFormMongoBean created]`, '\n', userFormMongoBean);
                }
            }
        }
        catch (error) {
            console.error(error.message);
        }
        // store into global state
        this.props.addFormStruct(formStructBean);
        this.props.addUserForm(userFormMongoBean);
        this.props.addResponses(userFormMongoBean.userResponses);
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][formStructBean]`, '\n', formStructBean);
            console.log(`[${fileName}][${methodName}][userFormMongoBean]`, '\n', userFormMongoBean);
        }
    }

    getUserFormFromBlockchainLedger = (idUser) => {
        return null;
    }

    getUserFormFromMongoByIds = (idForm, IdUser) => {
        const methodName        = "getUserFormFromMongoByIds";
        return this.userFormService.getUserForm(idForm, IdUser)
        .then(userFormBean => {
            return userFormBean;
        })
        .catch( (error) => {
            if ( this.isDebug ) {
                console.error(`[${fileName}][${methodName}][error]`, '\n', error.message);
            }
            this.errors     = [...this.errors, error];
            return null;
        });
    }

    /**
     * Get the last active user form with user responses for a specific user.
     * If idOwner defined, retrieve the user responses only owned by the user.
     */
    getLastActiveUserFormFromMongo = (idUser, idOwner) => {
        const methodName        = "getLastActiveUserFormFromMongo";
        return this.userFormService.getActiveUserForm(idUser, idOwner)
        .then(userFormBean => {
            return userFormBean;
        })
        .catch( (error) => {
            if ( this.isDebug ) {
                console.error(`[${fileName}][${methodName}][error]`, '\n', error.message);
            }
            this.errors     = [...this.errors, error];
            return null;
        });
    }

    getFormStructById = (idForm) => {
        const methodName        = "getFormStructById";
        return this.formStructService.getForm(idForm)
        .then (formStruct => {
            return formStruct;
        })
        .catch( (error) => {
            if ( this.isDebug ) {
                console.error(`[${fileName}][${methodName}][error]`, '\n', error.message);
            }
            this.errors     = [...this.errors, error];
            return null;
        });
    }

    getLastActiveFormStruct = (dateLastActiveFormStruct) => {
        const methodName        = "getFormStructById";
        return this.formStructService.getLastActiveForm(dateLastActiveFormStruct)
        .then (formStruct => {
            return formStruct;
        })
        .catch( (error) => {
            if ( this.isDebug ) {
                console.error(`[${fileName}][${methodName}][error]`, '\n', error.message);
            }
            this.errors     = [...this.errors, error];
            return null;
        });
    }

    shouldComponentUpdate(nextProps, nextState) {
        const methodName        = "shouldComponentUpdate";
        if ( this.isDebug ) {
            // console.log(`[${fileName}][${methodName}][props]`, this.props);
            // console.log(`[${fileName}][${methodName}][nextProps]`, nextProps);
        }
        if ( this.props.eaeUserResponses !== nextProps.eaeUserResponses ||
            this.props.eaeUser !== nextProps.eaeUser ||
            this.props.userForm !== nextProps.userForm ) {
            return true;
        }
        return false;
    }

    componentDidMount() {
        const methodName        = "componentDidMount";
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][props]`, this.props);
        }
    }

    // after render
    getSnapshotBeforeUpdate(prevProps, prevState) {
        return null;
    }

    // after render
    componentDidUpdate() {
        const methodName        = "componentDidUpdate";
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][props]`, this.props);
        }
    }

    render () {
        const methodName        = "render";
        if ( this.isDebug ) {
            // console.log(`[${fileName}][${methodName}][props]`, '\n', this.props);
        }
        if ( utils.isEmptyObject(this.props.statusForm) ) {
            if ( this.isDebug ) {
                // console.log(`[${fileName}][${methodName}][state]`, null);
            }
            return null;
        }
        else if ( this.errors.length ) { // display errors
            return <ul>
                    {this.errors.map(error => {
                        return <li className="error">{error.message}</li>;
                    })}
            </ul>

        }
        else {
            const statusLabel   = ( utils.isEmptyObject(this.props.statusForm) ) ? "" : this.props.statusForm.label;
            
            const headerForm = ( utils.isEmptyObject(this.props.formStruct) || utils.isEmptyObject(this.props.userForm) ) 
            ? null 
            : <EaeHeaderFormComp 
            statusLabel={statusLabel} 
            formStruct={this.props.formStruct}
            userForm={this.props.userForm}
            config={this.props.config}
            />;
            
            const formUI = ( 
                utils.isEmptyObject(this.props.formStruct) || 
                utils.isEmptyObject(this.props.userForm)
            ) 
            ? null 
            : <EaeFormComp
            statusLabel={statusLabel}
            eaeServiceComp={this.eaeServiceComp}
            addResponse={this.props.addResponse}
            addResponses={this.props.addResponses}
            initDeletedResponsesStateArray={this.props.initDeletedResponsesStateArray}
            initUserResponsesStateArray={this.props.initUserResponsesStateArray}
            />;
            
            if ( this.isDebug ) {
                console.debug(`[${fileName}][${methodName}][this.props]`, this.props);
            }
            return (
                <Container
                fluid
                className="form-components"
                >
                    {headerForm}
                    {formUI}
                </Container>
            );
        }
    }
}
EaeFormWrapperComp.propTypes = {
    idUser: PropTypes.string.isRequired
};

const mapStateToProps = (state) => {
    return {
        config:state.configReducer.config,
        userConnected:state.userReducer.userConnected,
        eaeUser:state.userReducer.eaeUser,
        userForm: state.userFormReducer.userForm,
        formStruct: state.formStructReducer.formStruct,
        eaeUserResponses: state.responseReducer.eaeUserResponses,
        statusForm:state.enumReducer.statusForm
    };
};
const mapDispatchToProps    = (dispatch) => {
    return {
        addUserForm: (userFormBean) => dispatch({type:actionUserForm.ADD_EAE_USER_FORM, userForm:userFormBean}),
        addFormStruct: (formStructBean) => dispatch({type:actionFormStruct.ADD_FORM_STRUCT, formStruct:formStructBean}),
        addResponses: (eaeUserResponses) => dispatch({
            type:actionResponseType.ADD_RESPONSES, 
            eaeUserResponses:eaeUserResponses
        }),
        addResponse: (eaeUserResponse, isReplacement) => dispatch({
            type:actionResponseType.ADD_RESPONSE, 
            eaeUserResponse:eaeUserResponse,
            isReplacement:isReplacement     // true to delete user responses of the same query
        }),
        initDeletedResponsesStateArray: () => dispatch({type:actionResponseType.INIT_DELETED_RESPONSES_STATE_ARRAY}),
        initUserResponsesStateArray: () => dispatch({type:actionResponseType.INIT_USER_RESPONSES_STATE_ARRAY}),
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(EaeFormWrapperComp);