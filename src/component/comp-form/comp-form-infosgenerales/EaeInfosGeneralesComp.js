import React from 'react';
import {Container, Form, Row, Col} from 'react-bootstrap';
import classes from './EaeInfosGeneralesComp.module.css';


class EaeInfosGeneralesComp extends React.Component {
    constructor(props) {
        super(props);
        this.eaeUser    = null;
    }

    render () {
        const infosGeneralesComp    = 
        <Form className={classes.InfosGenerales}>
            <Form.Group as={Row} className={classes.Inputs} controlId="datesEntretien">
                <Form.Group as={Col} controlId="formGridEmail">
                    <Form.Label>Date de l'entretien</Form.Label>
                    <Form.Control type="input" placeholder="JJ/MM/AAAA" />
                </Form.Group>
                <Form.Group as={Col} controlId="formGridEmail">
                    <Form.Label>Date du dernier entretien</Form.Label>
                    <Form.Control type="input" placeholder="JJ/MM/AAAA" />
                </Form.Group>
            </Form.Group>
            <Form.Group as={Row} className={classes.Inputs} controlId="datesEntretien">
                <Form.Group as={Col} controlId="formGridEmail">
                    <Form.Label>Poste occupé</Form.Label>
                    <Form.Control type="input" placeholder="Poste occupé" />
                </Form.Group>
                <Form.Group as={Col} controlId="formGridEmail">
                    <Form.Label>Niveau de responsabilité</Form.Label>
                    <Form.Control type="input" placeholder="Niveau de responsabilité" />
                </Form.Group>
            </Form.Group>
            <Form.Group as={Row} className={classes.Inputs} controlId="datesEntretien">
                <Form.Group as={Col} controlId="formGridEmail">
                    <Form.Label>Ancienneté Micropole</Form.Label>
                    <Form.Control type="input" placeholder="Ancienneté Micropole" />
                </Form.Group>
                <Form.Group as={Col} controlId="formGridEmail">
                    <Form.Label>Expérience dans le métier</Form.Label>
                    <Form.Control type="input" placeholder="Expérience dans le métier" />
                </Form.Group>
            </Form.Group>
        </Form>;
            return (
                <Container className={classes.InfosGenerales}>
                    {infosGeneralesComp}
                </Container>
        );
    }
}

export default EaeInfosGeneralesComp;