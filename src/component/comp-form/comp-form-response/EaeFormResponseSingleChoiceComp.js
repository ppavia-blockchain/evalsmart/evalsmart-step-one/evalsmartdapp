import React from 'react';
import EaeRadioButtonUI from '../comp-formUI/comp-inputs-UI/EaeRadioButtonUI';
import { connect } from 'react-redux';

import * as utils from '../../../utils/EvalsmartUtils';

/* MODELS */
import EaeUserFormResponseBean from '../../../model/bean/userform-beans/EaeUserFormResponseBean';

/* SERVICES */
import EaeServiceComp from '../../../service/EaeServiceComp';

const fileName   = "EaeFormResponseSingleChoiceComp";

class EaeFormResponseSingleChoiceComp extends React.Component {
    constructor(props) {
        super(props);
        const methodName        = "constructor";
        this.isDebug    = utils.isDebug (this.props.config.log, fileName);
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][this.props]`, '\n', this.props);
        }
        this.eaeServiceCom          = new EaeServiceComp(this.props.config);
        this.saveResponseHandler    = this.saveResponseHandler.bind(this);        
    }

    saveResponseHandler = (event, response, name) => { // insert / update a user response into the user resonses array stored in state
        const methodName        = "saveResponseHandler";
        if ( event.target.name === name ) {
            this.props.inputUI.config.addResponse(response, true);
        }
    }

    render () {
        if ( utils.isEmpty(this.props.inputUI ) ) {
            return null;
        }

        return (
            <EaeRadioButtonUI
            id={this.props.inputUI.id}
            type="radio"
            name={this.props.inputUI.config.name}
            value={this.props.inputUI.config.value}
            isDisabled={this.props.inputUI.config.isDisabled}
            isChecked={this.props.inputUI.config.isChecked}
            isInLine={this.props.inputUI.config.isInLine}
            click={(event) => this.saveResponseHandler(event, this.props.inputUI.linkedData, this.props.inputUI.config.name)}
            >
            </EaeRadioButtonUI>
        );
    }
}
EaeFormResponseSingleChoiceComp.propTypes = {
};
const mapStateToProps = (state) => {
    return {
        config:state.configReducer.config,
        eaeUser:state.userReducer.eaeUser,
        userForm: state.userFormReducer.userForm,
        eaeUserResponses: state.responseReducer.eaeUserResponses,
        eaeDeletedResponses: state.responseReducer.eaeDeletedResponses
    };
}
const mapDispatchToProps    = (dispatch) => {
    return {
    };
};
export default connect(mapStateToProps, null)(EaeFormResponseSingleChoiceComp);