import React, {Component} from 'react';
import '../comp-form-style/EaeLevel.css';
import {Col} from 'react-bootstrap';

class EaeResponseLevelComp extends React.Component {
    constructor(props) {
        super(props);
    }
    render () {
        const mdProp = 3;
        return (
            <Col className="levels" md={mdProp}>
                {this.props.level.label}
            </Col>
        );
    }
}
export default EaeResponseLevelComp;