import React from 'react';
import EaeTextAreaUI from '../comp-formUI/comp-inputs-UI/EaeTextAreaUI';
import * as utils from '../../../utils/EvalsmartUtils';
import { connect } from 'react-redux';
import EaeInputTextUI from '../comp-formUI/comp-inputs-UI/EaeInputTextUI';

const fileName   = "EaeFormResponseSimpleTextComp";

class EaeFormResponseSimpleTextComp extends React.Component {
    constructor(props) {
        super(props);
        const methodName        = "constructor";
        this.isDebug    = utils.isDebug (this.props.config.log, fileName);
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][this.props]`, '\n', this.props);
        }
    }
    render () {
        if ( utils.isEmpty(this.props.inputUI ) ) {
            return null;
        }
        return (
            <EaeInputTextUI
            id={this.props.inputUI.id}
            classes={this.props.inputUI.config.classes}
            content={this.props.inputUI.config.content}
            label={this.props.inputUI.config.label}
            defaultValue={this.props.inputUI.config.defaultValue}
            value={this.props.inputUI.config.value}
            change={this.props.inputUI.config.onChange}
            />
        );
    }
}
EaeFormResponseSimpleTextComp.propTypes = {
};
const mapStateToProps = (state) => {
    return {
        config:state.configReducer.config
    };
}
export default connect(mapStateToProps, null)(EaeFormResponseSimpleTextComp);