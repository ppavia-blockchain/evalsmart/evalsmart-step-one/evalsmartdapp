import React from 'react';
import EaeTextAreaUI from '../comp-formUI/comp-inputs-UI/EaeTextAreaUI';
import * as utils from '../../../utils/EvalsmartUtils';
import { connect } from 'react-redux';

/* MODELS */
import EaeUserFormResponseBean from '../../../model/bean/userform-beans/EaeUserFormResponseBean';

/* SERVICES */
import EaeServiceComp from '../../../service/EaeServiceComp';

const fileName   = "EaeFormResponseFreeTextComp";

class EaeFormResponseFreeTextComp extends React.Component {
    constructor(props) {
        super(props);
        const methodName        = "constructor";
        this.isDebug    = utils.isDebug (this.props.config.log, fileName);
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][this.props]`, '\n', this.props);
        }
        this.eaeServiceCom          = new EaeServiceComp(this.props.config);
        this.saveResponseHandler   = this.saveResponseHandler.bind(this); 
    }

    saveResponseHandler = (event, response, name) => { // insert / update a user response into the user resonses array stored in state
        const methodName        = "saveResponseHandler";
        if ( event.target.name === name ) {
            response.content = event.target.value;
            this.props.inputUI.config.addResponse(response, false);
        }
    }

    render () {
        if ( utils.isEmpty(this.props.inputUI ) ) {
            return null;
        }

        return (
            <EaeTextAreaUI
            key={this.props.inputUI.id}
            id={this.props.inputUI.id}
            name={this.props.inputUI.config.name}
            classes={this.props.inputUI.config.classes}
            readOnly={this.props.inputUI.config.readOnly}
            content={this.props.inputUI.config.content}
            rows={this.props.inputUI.config.rows}
            value={this.props.inputUI.config.content}
            change={(event) => this.saveResponseHandler(event, this.props.inputUI.linkedData, this.props.inputUI.config.name)}
            />
        );
    }
}
EaeFormResponseFreeTextComp.propTypes = {
};
const mapStateToProps = (state) => {
    return {
        config:state.configReducer.config,
        eaeUser:state.userReducer.eaeUser,
        userForm: state.userFormReducer.userForm,
        eaeUserResponses: state.responseReducer.eaeUserResponses,
        eaeDeletedResponses: state.responseReducer.eaeDeletedResponses
    };
}
export default connect(mapStateToProps, null)(EaeFormResponseFreeTextComp);