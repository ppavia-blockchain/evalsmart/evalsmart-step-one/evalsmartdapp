/* REACT */
import React from 'react';
import PropTypes from 'prop-types';

/* REDUX */
import { connect } from 'react-redux';

/* BOOTTRAP AND CSS */
import classes from './EaeFooterDomainComp.module.css';

/* UTILS */
import * as utils from '../../../utils/EvalsmartUtils';
import * as EaeConst from '../../../model/EaeConst';
import { EaeRouter } from '../../../router/EaeRouter';

/* COMPONENTS */
import * as formUI from '../comp-formUI/comp-formUI-bean/oFormUIs';
import EaeRowUI from '../comp-formUI/comp-rows-UI/EaeRowUI';
import EaeButtonUI from '../comp-formUI/comp-commons-UI/EaeButtonUI';
import EaeLinkUI from '../comp-formUI/comp-commons-UI/EaeLinkUI';

/* SERVICES */
import EaeFooterDomainServiceComp from '../../../service/comp-services/EaeFooterDomainServiceComp';

const fileName = "EaeFooterDomainWithStoreResponsesComp";

class EaeFooterDomainWithStoreResponsesComp extends React.Component {
    constructor(props) {
        super(props);
        const methodName       = "constructor";
        this.isDebug           = utils.isDebug (this.props.config.log, fileName);
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][props]`, '\n', props);
        }
        this.eaeFooterDomainServiceComp          = new EaeFooterDomainServiceComp(this.props.config);
        this.eaeServiceComp             = props.eaeServiceComp; 
        this.storeResponseHandler   = this.storeResponseHandler.bind(this);
    }

    componentDidMount() {
    }

    storeResponseHandler = (event, responses, deletedResponses=[]) => {
        const methodName        = "storeResponseHandler";
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][INIT responses]`, responses, deletedResponses);
        }
        switch ( event.target.name ) {
            case "ante":
                this.eaeServiceComp.storeResponsesDomain(responses, deletedResponses);
                if ( this.isDebug ) {
                    console.log(`[${fileName}][${methodName}][ANTE responses]`, responses, deletedResponses);
                }
                break;
            case "post":
                this.eaeServiceComp.storeResponsesDomain(responses, deletedResponses);
                if ( this.isDebug ) {
                    console.log(`[${fileName}][${methodName}][POST this.props]`, this.props.eaeUserResponses, this.props.eaeDeletedResponses);
                }
                break;
        }
    }

    render () {
        const methodName       = "render";
        if ( utils.isEmpty(this.isDebug) ) {
            console.error(`[${fileName}][${methodName}][this.isDebug]`, '\n', this.isDebug);
            return null;
        }
        else {
            return this.eaeFooterDomainServiceComp.buildFooterDomain(
                this.props.currentOrder,
                this.props.orderMax,
                `${classes.Button} ${classes.Buttonante}`,
                `${classes.Button} ${classes.Buttonpost}`,
                (event) => this.storeResponseHandler(event, this.props.eaeUserResponses, this.props.eaeDeletedResponses),
                (event) => this.storeResponseHandler(event, this.props.eaeUserResponses, this.props.eaeDeletedResponses)
            );
        }
    }
}
const mapStateToProps = (state) => {
    return {
        config:state.configReducer.config,
        userConnected:state.userReducer.userConnected,
        eaeUser:state.userReducer.eaeUser,
        userForm: state.userFormReducer.userForm,
        formStruct: state.formStructReducer.formStruct,
        eaeUserResponses: state.responseReducer.eaeUserResponses,
        eaeDeletedResponses: state.responseReducer.eaeDeletedResponses
    };
};
const mapDispatchToProps    = (dispatch) => {
    return {
    };
};
export default connect(mapStateToProps)(EaeFooterDomainWithStoreResponsesComp);