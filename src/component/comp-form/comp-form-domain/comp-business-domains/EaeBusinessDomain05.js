/* REACT */
import React from 'react';
import PropTypes from 'prop-types';

/* REDUX */
import { connect } from 'react-redux';

/* BOOTTRAP AND CSS */
import {Container} from 'react-bootstrap';

/* UTILS */
import * as utils from '../../../../utils/EvalsmartUtils';
import * as EaeConst from '../../../../model/EaeConst';

/* COMPONENTS */
import EaeBlockUI from '../../comp-formUI/comp-block-UI/EaeBlockUI';

/* MODELS */

const fileName          = "EaeBusinessDomain05";

class EaeBusinessDomain05 extends React.Component {
    constructor(props) {
        super(props);
        const methodName        = "constructor";
        this.isDebug            = utils.isDebug (this.props.config.log, fileName);
        this.errors             = [];
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][props]`, props);
        }
    }

    render () {
        const methodName        = "render";

        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][this.props]`, this.props);
        }
        return (
            <Container>

                <EaeBlockUI
                headersUI={[{id:this.props.businessDomainUI.id, title:this.props.businessDomainUI.linkedData.label}]}
                bodiesUI={this.props.businessDomainUI.config.content.domainUI.oComponentsUI}
                footersUI={[]}
                />

                <EaeBlockUI
                headersUI={[{id:this.props.businessDomainUI.id, title:EaeConst.UI_COMMENT_LABEL}]}
                bodiesUI={this.props.businessDomainUI.config.content.domainUI.oCommentsUI}
                footersUI={[this.props.businessDomainUI.config.content.footerUI]}
                />

            </Container>
        );
    }
}

EaeBusinessDomain05.propTypes = {
};

const mapStateToProps = (state) => {
    return {
        config:state.configReducer.config,
        eaeUser:state.userReducer.eaeUser,
        userForm: state.userFormReducer.userForm,
        formStruct: state.formStructReducer.formStruct,
        eaeUserResponses: state.responseReducer.eaeUserResponses
    };
};
const mapDispatchToProps    = (dispatch) => {
    return {
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(EaeBusinessDomain05);


