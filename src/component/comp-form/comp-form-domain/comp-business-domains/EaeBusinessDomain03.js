/* REACT */
import React from 'react';
import PropTypes from 'prop-types';

/* REDUX */
import { connect } from 'react-redux';

/* BOOTTRAP AND CSS */

/* UTILS */
import * as utils from '../../../../utils/EvalsmartUtils';
import * as EaeConst from '../../../../model/EaeConst';

/* COMPONENTS */
import EaeBlockUI from '../../comp-formUI/comp-block-UI/EaeBlockUI';

/* MODELS */

const fileName          = "EaeBusinessDomain03";

class EaeBusinessDomain03 extends React.Component {
    constructor(props) {
        super(props);
        const methodName        = "constructor";
        this.isDebug            = utils.isDebug (this.props.config.log, fileName);
        this.errors             = [];
        this.eaeServiceComp     = props.eaeServiceComp;
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][props]`, props);
        }
    }

    render () {
        const methodName        = "render";

        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][this.props]`, this.props);
        }
        return (
            <EaeBlockUI
            headersUI={[{id:this.props.businessDomainUI.id, title:this.props.businessDomainUI.linkedData.label}]}
            bodiesUI={this.props.businessDomainUI.config.content.domainUI}
            footersUI={[this.props.businessDomainUI.config.content.footerUI]}
            />
        );
    }
}

EaeBusinessDomain03.propTypes = {
};

const mapStateToProps = (state) => {
    return {
        config:state.configReducer.config,
        eaeUser:state.userReducer.eaeUser,
        userForm: state.userFormReducer.userForm,
        formStruct: state.formStructReducer.formStruct,
        eaeUserResponses: state.responseReducer.eaeUserResponses
    };
};
const mapDispatchToProps    = (dispatch) => {
    return {
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(EaeBusinessDomain03);


