/* REACT */
import React from 'react';
import PropTypes from 'prop-types';

/* REDUX */
import { connect } from 'react-redux';

/* BOOTTRAP AND CSS */
import {Container} from 'react-bootstrap';
import '../comp-form-style/EaeDomain.css';

/* UTILS */
import * as utils from '../../../utils/EvalsmartUtils';
import * as EaeConst from '../../../model/EaeConst';
import { EaeRouter } from '../../../router/EaeRouter';

/* COMPONENTS */
import EaeFormListResponseLevelsComp from '../comp-form-response/EaeFormListResponseLevelsComp';

/* SERVICES */
import EaeServiceComp from '../../../service/EaeServiceComp';

/* ROUTE */

const fileName   = "EaeFormDomainComp";

class EaeFormDomainComp extends React.Component {
    constructor(props) {
        super(props);
        const methodName        = "constructor";
        this.isDebug    = utils.isDebug (this.props.config.log, fileName);
        this.router                 = new EaeRouter(this.props.config);
        this.eaeServiceComp         = props.eaeServiceComp;   
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][props]`, '\n', props);
        }
        // inject into EaeServiceComp
        this.eaeServiceComp.idForm                          = this.props.userForm.idForm;
        this.eaeServiceComp.idUser                          = this.props.userForm.idUser;   // should be the same as this.props.eaeUser.idUser
        this.eaeServiceComp.idOwner                         = this.props.userConnected.idUser;
        this.eaeServiceComp.userConnected                   = this.props.userConnected;
        this.eaeServiceComp.eaeUser                         = this.props.eaeUser;
        this.eaeServiceComp.userForm                        = this.props.userForm;
        this.eaeServiceComp.addResponse                     = this.props.addResponse; 
        this.eaeServiceComp.addResponses                    = this.props.addResponses;                      // inject redux props method into EaeServiceComp
        this.eaeServiceComp.initUserResponsesStateArray     = this.props.initUserResponsesStateArray;       // inject redux props method into EaeServiceComp
        this.eaeServiceComp.initDeletedResponsesStateArray  = this.props.initDeletedResponsesStateArray;    // inject redux props method into EaeServiceComp
        this.eaeServiceComp.userResponses                   = this.props.eaeUserResponses;
    }

    componentDidMount () {
        const methodName        = "componentDidMount";
    }

    shouldComponentUpdate (nextProps, nextState) {
        const methodName        = "shouldComponentUpdate";
        if ( this.isDebug ) {
            // console.log(`[${fileName}][${methodName}][this.props.eaeUserResponses]`, '\n', this.props.eaeUserResponses);
        }
        return true;
    }
    
    render () {
        const methodName        = "render";

        this.domain = this.router.getDomain(this.props.formStruct, this.props.match.params.orderId);

        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][this.props]`, '\n', this.props);
            console.log(`[${fileName}][${methodName}][this.eaeServiceComp]`, '\n', this.eaeServiceComp);
        }
        
        const domainComp    = this.eaeServiceComp.getBusinessDomainComp(
            this.domain,
            this.props.formStruct.domains.length,
            this.props.eaeUserResponses,
            this.props.eaeDeletedResponses
        );
        
        return (
            <Container fluid className='domains' id={this.domain.idDomain} >
                {domainComp}
            </Container>
        );
    }
}
EaeFormListResponseLevelsComp.propTypes = {
    domain: PropTypes.object
};
const mapStateToProps = (state) => {
    return {
        config:state.configReducer.config,
        userConnected:state.userReducer.userConnected,
        eaeUser:state.userReducer.eaeUser,
        userForm: state.userFormReducer.userForm,
        formStruct: state.formStructReducer.formStruct,
        eaeUserResponses: state.responseReducer.eaeUserResponses,
        eaeDeletedResponses: state.responseReducer.eaeDeletedResponses
    };
}
const mapDispatchToProps    = (dispatch) => {
    return {
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(EaeFormDomainComp);