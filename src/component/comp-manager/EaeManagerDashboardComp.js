/* REACT */
import React from 'react';
import PropTypes from 'prop-types';

/* REDUX */
import { connect } from 'react-redux';
import * as actionUserType from '../../redux/actions/action-user';

/* SERVICES */
import EaeAuthenticationService from '../../service/EaeAuthenticationService';
import EaeServiceComp from '../../service/EaeServiceComp';

/* UTILS */ 
import * as EaeConst from '../../model/EaeConst';
import * as utils from '../../utils/EvalsmartUtils';

/* BOOTSTRAP - CSS */
import {Container, Col} from 'react-bootstrap';
import {Form, InputGroup, FormControl, Button} from 'react-bootstrap';
import classes from './EaeManagerDashboardComp.module.css';


/* COMPONENT */
import EaeBlockUI from '../comp-form/comp-formUI/comp-block-UI/EaeBlockUI';


const fileName = "EaeManagerDashboardComp";

/**
 * Manage authentification -> statefull, global storage
 */
class EaeManagerDashboardComp extends React.Component {
    state = {};
    constructor(props) {
        super(props);
        const methodName        = "constructor";
        this.isDebug            = utils.isDebug (props.config.log, fileName);
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][props]`, '\n', props);
        }
        this.eaeAuthenticationService       = new EaeAuthenticationService(props.config);
        this.eaeServiceComp                 = new EaeServiceComp(this.props.config);
        this.onChangeInputHandler           = this.onChangeInputHandler.bind(this);
        this.submitAuthentFormHandler       = this.submitAuthentFormHandler.bind(this);

        this._getListOfUserForms();
    }
    componentDidMount() {
    }

    componentDidUpdate() {
    }

    shouldComponentUpdate ( nextProps, nextState ) {
        const methodName        = "shouldComponentUpdate";
        return true;
    }

    onChangeInputHandler = (event) => {
        const methodName            = "onChangeInputHandler";
        switch (event.target.name) {
            case "gotoItem" :
                if ( this.isDebug ) {
                    console.log(`[${fileName}][${methodName}][event.target.value]`, '\n', event.target.value);
                }
                this.props.addEaeUser(JSON.parse(event.target.value));
                sessionStorage.setItem("session-eae-user", event.target.value);
                break;
            default :
                alert("otto");

        }
    }

    submitAuthentFormHandler = (event) => {
        switch (event.target.name) {
            case "submitAuthentForm" :
                break;
            default :
                alert(event.target.value);
        }
    }

    _getListOfUserForms     = async () => {
        const methodName            = "_getListOfUserForms";
        if ( this.isDebug ) {
            console.log(`[${fileName}][${methodName}][this.props.statusForm]`, '\n', this.props.statusForm);
        }
        this.setState({
            oListItw: await this.eaeServiceComp.buildManagerListEaeView(this.props.userConnected, this.props.statusForm, this.onChangeInputHandler)
        })
    }

    render () {
        const methodName            = "render";
        if ( utils.isEmptyObject( this.props.userConnected ) || utils.isEmptyObject(this.state.oListItw) ) {
            return null;
        }
        else {
            if ( this.isDebug ) {
                console.log(`[${fileName}][${methodName}][this.state.oListItw]`, '\n', this.state.oListItw);
            }
            return (
            <Container>
                    <EaeBlockUI
                    headersUI={[]}
                    bodiesUI={this.state.oListItw}
                    footersUI={[]}
                    />
            </Container>
            );
        }
    }
}

EaeManagerDashboardComp.propTypes = {
    config: PropTypes.object
};


const mapStateToProps = (state) => {
    return {
        userConnected:state.userReducer.userConnected,
        eaeUser:state.userReducer.eaeUser,
        userForm: state.userFormReducer.userForm,
        statusForm:state.enumReducer.statusForm
    };
};

const mapDispatchToProps    = (dispatch) => {
    return {
        addEaeUser: (eaeUser) => dispatch({type:actionUserType.ADD_EAE_USER, eaeUser:eaeUser}),
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(EaeManagerDashboardComp);