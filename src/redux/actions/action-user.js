export const ADD_EAE_USER               = 'ADD_EAE_USER';
export const REMOVE_EAE_USER            = 'REMOVE_EAE_USER';
export const ADD_USER_CONNECTED         = 'ADD_USER_CONNECTED';
export const REMOVE_USER_CONNECTED      = 'REMOVE_USER_CONNECTED';
export const ADD_ROLE_USER              = 'ADD_ROLE_USER';
export const REMOVE_ROLE_USER           = 'REMOVE_ROLE_USER';

