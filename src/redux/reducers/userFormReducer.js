import * as actionUserForm from '../actions/action-userform';
import * as utils from '../../utils/EvalsmartUtils';

const fileName          = "userFormReducer";
const initialState      = { userForm: {} };

const userForm     = (state = initialState, action) => {
    let nextState;
    const methodName        = "userForm";
    let isDebug             = false;
    if ( state.config ) {
        isDebug = utils.isDebug (state.config.log, fileName);
    }
    
    switch (action.type) {
        case actionUserForm.ADD_EAE_USER_FORM :
            nextState = {
                ...state,
                userForm: action.userForm
            };
            if ( isDebug ) {
                console.debug(`[${fileName}][${methodName}][nextState]`, '\n', nextState);
            }
            return nextState || state;
        default:
            return state;
    }
};
export default userForm;