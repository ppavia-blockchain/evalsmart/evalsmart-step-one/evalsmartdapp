import * as actionUserType from '../actions/action-user';
import * as utils from '../../utils/EvalsmartUtils';

const fileName          = "userReducer";
const initialState      = { 
    eaeUser: {}, 
    userConnected:{}, 
    eaeUserRole: null
};

const users     = (state = initialState, action) => {
    let nextState;
    const methodName        = "users";
    let isDebug             = false;
    if ( state.config ) {
        isDebug = utils.isDebug (state.config.log, fileName);
    }

    if ( isDebug ) {
        // console.debug(`[${fileName}][${methodName}][state]`, '\n', state);
    }
    
    switch (action.type) {
        case actionUserType.ADD_EAE_USER :
            nextState = {
                ...state,
                eaeUser: action.eaeUser
            };
            if ( isDebug ) {
                console.debug(`[${fileName}][${methodName}][nextState ADD_EAE_USER]`, '\n', nextState);
            }
            return nextState || state;
        case actionUserType.REMOVE_EAE_USER :
            nextState = {
                ...state,
                eaeUser: {}
            };
            if ( isDebug ) {
                console.debug(`[${fileName}][${methodName}][nextState REMOVE_EAE_USER]`, '\n', nextState);
            }
            return nextState || state;
        case actionUserType.ADD_USER_CONNECTED :
            nextState = {
                ...state,
                userConnected: action.userConnected
            };
            if ( isDebug ) {
                console.debug(`[${fileName}][${methodName}][nextState ADD_USER_CONNECTED]`, '\n', nextState);
            }
            return nextState || state;
        case actionUserType.REMOVE_USER_CONNECTED :
            nextState = {
                ...state,
                userConnected: {}
            };
            if ( isDebug ) {
                console.debug(`[${fileName}][${methodName}][nextState REMOVE_USER_CONNECTED]`, '\n', nextState);
            }
            return nextState || state;
        case actionUserType.ADD_ROLE_USER :
            nextState = {
                ...state,
                eaeUserRole: action.userRole
            };
            if ( isDebug ) {
                console.debug(`[${fileName}][${methodName}][nextState ADD_ROLE_USER]`, '\n', nextState);
            }
            return nextState || state;
        case actionUserType.REMOVE_ROLE_USER :
            nextState = {
                ...state,
                eaeUserRole: null
            };
            if ( isDebug ) {
                console.debug(`[${fileName}][${methodName}][nextState REMOVE_ROLE_USER]`, '\n', nextState);
            }
            return nextState || state;
        default:
            return state;
    }
};
export default users;