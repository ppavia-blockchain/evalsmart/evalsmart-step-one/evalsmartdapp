import * as actionEnumType from '../actions/action-enum';
import * as utils from '../../utils/EvalsmartUtils';

const fileName          = "enumReducer";
const initialState      = { statusForm: {} };

const enums     = (state = initialState, action) => {
    let nextState;
    const methodName        = "enums";
    let isDebug             = false;
    if ( state.config ) {
        isDebug = utils.isDebug (state.config.log, fileName);
    }

    if ( isDebug ) {
        console.debug(`[${fileName}][${methodName}][state]`, '\n', state);
    }
    
    switch (action.type) {
        case actionEnumType.ADD_ALL_STATUS_FORM :
            nextState = {
                ...state,
                statusForm: action.statusForm
            };
            if ( isDebug ) {
                console.debug(`[${fileName}][${methodName}][nextState ADD_ALL_STATUS_FORM]`, '\n', nextState);
            }
            return nextState || state;
        default:
            return state;
    }
};
export default enums;