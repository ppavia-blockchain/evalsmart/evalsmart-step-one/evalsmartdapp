import * as actionConfig from '../actions/action-config';
import * as eaeConfig from '../../config/eaeConfig';

const fileName          = "configReducer";
const initialState      = { config: {} };

const configs     = (state = initialState, action) => {
    let nextState;
    
    switch (action.type) {
        case actionConfig.SET_CONFIG :
            let envConfig   = {};
            switch (action.env) {
                case "prod":
                    envConfig   = eaeConfig.config.prod;
                    break;
                case "dev" :
                    envConfig   = eaeConfig.config.dev;
                    break;
                default :
                envConfig   = eaeConfig.config.dev;
            }
            nextState = {
                ...state,
                config: envConfig
            };
            if ( nextState.config.log.isDebug ) {
                //console.log(`[${fileName}][configs]`, '\n', nextState);
            }
            return nextState || state;
        default:
            return state;
    }
};
export default configs;