import * as actionFormStruct from '../actions/action-formstruct';
import * as utils from '../../utils/EvalsmartUtils';

const fileName          = "formStructReducer";
const initialState      = { formStruct : {}};

const formStruct = (state = initialState, action) => {
    let nextState;
    let isDebug             = false;
    if ( state.config ) {
        isDebug = utils.isDebug (state.config.log, fileName);
    }
    
    const methodName        = "formStruct";
    
    switch (action.type) {
        case actionFormStruct.ADD_FORM_STRUCT :
            nextState = {
                ...state,
                formStruct: action.formStruct
            };
            if ( isDebug ) {
                console.debug(`[${fileName}][${methodName}][nextState]`, '\n', nextState);
            }
            return nextState || state;
        default:
            return state;
    }
};

export default formStruct;