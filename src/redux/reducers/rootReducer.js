import { combineReducers } from 'redux';
import configReducer from './configReducer';
import userReducer from './userReducer';
import userFormReducer from './userFormReducer';
import formStructReducer from './formStructReducer';
import responseReducer from './responseReducer';
import enumReducer from './enumReducer';

export default combineReducers({
    configReducer,
    userReducer,
    userFormReducer,
    formStructReducer,
    responseReducer,
    enumReducer
});
