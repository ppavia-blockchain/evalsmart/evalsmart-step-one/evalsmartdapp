import * as actionResponseType from '../actions/action-response';
import * as utils from '../../utils/EvalsmartUtils';

const fileName          = "responseReducer";
const initialState      = { eaeUserResponses: [], eaeDeletedResponses: [] };

const responses     = (state = initialState, action) => {
    let isDebug             = false;
    if ( state.config ) {
        isDebug = utils.isDebug (state.config.log, fileName);
    }
    let nextState;

    // console.log(`[${fileName}][state]`, '\n', state);
    
    switch (action.type) {
        case actionResponseType.ADD_RESPONSE :
            if ( action.isReplacement ) { // delete user responses of the same query, if exists
                const arrDeletedResponse = [];
                let arrResponsesWithReplacement  = []
                state.eaeUserResponses.forEach ( (response) => {
                    if ( response.idQuery === action.eaeUserResponse.idQuery && response.idResponse !== action.eaeUserResponse.idResponse ) {
                        arrDeletedResponse.push(response);
                    }
                    else {
                        arrResponsesWithReplacement.push(response);
                    }
                });
                nextState = {
                    ...state,
                    eaeUserResponses: [...arrResponsesWithReplacement, action.eaeUserResponse],
                    eaeDeletedResponses:[...state.eaeDeletedResponses, ...arrDeletedResponse]
                };
            }
            else {
                // replace existant response
                let arrResponsesWithReplacement  = []
                state.eaeUserResponses.forEach ( (response) => {
                    if ( response.idResponse !== action.eaeUserResponse.idResponse ) {
                        arrResponsesWithReplacement.push(response);
                    }
                });
                nextState = {
                    ...state,
                    eaeUserResponses: [...arrResponsesWithReplacement, action.eaeUserResponse]
                };
            }
            if ( isDebug ) {
                console.log(`[${fileName}][ADD_RESPONSE][nextState]`, '\n', nextState);
            }
            return nextState || state;

        case actionResponseType.ADD_RESPONSES :

            nextState = {
                ...state,
                eaeUserResponses: action.eaeUserResponses.map((eaeUserResponse) => {
                    return eaeUserResponse;
                })
            };

            if ( isDebug ) {
                console.log(`[${fileName}][ADD_RESPONSES][nextState]`, '\n', nextState);
            }
            return nextState || state;
        case actionResponseType.INIT_DELETED_RESPONSES_STATE_ARRAY :
            nextState = {
                ...state,
                eaeDeletedResponses: []
            };
            if ( isDebug ) {
                console.log(`[${fileName}][INIT_DELETED_RESPONSES_STATE_ARRAY][nextState][nextState]`, '\n', nextState);
            }
            return nextState || state;
        case actionResponseType.INIT_USER_RESPONSES_STATE_ARRAY :
                nextState = {
                    ...state,
                    eaeUserResponses: []
                };
                if ( isDebug ) {
                    console.log(`[${fileName}][INIT_USER_RESPONSES_STATE_ARRAY][nextState][nextState]`, '\n', nextState);
                }
                return nextState || state;
        default:
            return state;
    }
};
export default responses;